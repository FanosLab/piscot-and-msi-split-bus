# PISCOT-and-MSI-Split-Bus

This is the source code of our RTSS 2020 paper entitled:
The Best of All Worlds: Improving Predictability at the Performance of Conventional Coherence with No Protocol Modifications

## Table of Contents:

1) [ An overview. ](#an-open-source-project)
2) [ Building Environment. ](#building-PISCOT)
3) [ Running PISCOT. ](#running-PISCOT)
4) [ Configuring PISCOT. ](#configuring-PISCOT)


<a name="an-open-source-project"></a>
## An Open Source project

This code is the implementation of our cache simulation environment used to implement our proposed PISCOT predictable coherence protocol as well as the conventional split-transaction MSI. 
The simulator callback interface between different components (e.g.: cores local buffers, cache controllers, arbiters, shared cache) is inspired by the [ns3 simulator](https://www.nsnam.org/). However, it is important to note that while ns3 is event-based (which is more suitable for networking), our simulator is cycle-accurate. 


<a name="building-PISCOT"></a>
## Building Environment

The simulator can be built using the [WAF building automation tool](https://en.wikipedia.org/wiki/Waf).

To build the PISCOT with a certain benchmark included in this package, 
you need to use the tool 'waf'. The script "do_sim_run.sh" provided can be
used to automate the simulation flow after the initial setup is done.

However, the real quick and dirty way to get started is to
type the command
```shell
./waf configure
```

followed by

```shell
./waf --run "scratch/MultiCoreSimulator --CfgFile=./src/MultiCoreSim/model/test_cfg2.xml --BMsPath=/home/salah/sharedspace/piscot-master/BMs/eembc-traces/a2time01-trace --LogFileGenEnable=1"
```

<a name="running-PISCOT"></a>
## Running PISCOT

On recent Linux systems, once you have built the integrated PISCOT with ns-3, it should be easy to run the sample tests with the
following command, such as:

```shell
./do_sim_run.sh <TestType> <TestCategory> <TestCaseNum> <CopyLogFileFlag> <LogFileVersion>
```
Where, 

            # 1) Script arguments:
            #         ./do_sim_run.sh <TestType> <TestCategory> <TestCaseNum> <CopyLogFileFlag> <LogFileVersion>
            #               <TestType>        : It can be either "Directed" or "Regression".
            #               <TestCategory>    : It can be either "EEMbcTrace" or "SplashTrace".
            #               <TestCaseNum>     : This argument used when running the script in "TestType = Directed" to 
            #                                   select a certain testcase number from "TestCategory".
            #               <CopyLogFileFlag> : A flag used to enable copying test results into a certain
            #                                   version directory
            #               <LogFileVersion>  : Final destination folder of the Log results
            # ------------------------------------------------------
            # 2) Example:
            #    2.1) Directed Mode without log file copying
            #            ./do_sim_run.sh Directed EEMbcTrace 5 0 0
            #            ./do_sim_run.sh Directed SplashTrace 3 0 0
            # ------------------------------------------------------
            #    2.2) Directed Mode with log file copying
            #            ./do_sim_run.sh Directed EEMbcTrace 6 1 0
            #            ./do_sim_run.sh Directed SplashTrace 2 1 1
            # -------------------------------------------------------
            #    2.2) Regression Mode with log file copying
            #            ./do_sim_run.sh Regression EEMbcTrace 0 1 2
            #            ./do_sim_run.sh Regression SplashTrace 0 1 3  

Before running do_run_script, you should setup the following PATHs in the script,

- Benchmark base directory

      BMsBasePath="/home/salah/research/coherence/GitLab/BMs"

- Log files base directory

      LOGBasePath="/home/salah/sharedspace/BMs"


<a name="configuring-PISCOT"></a>
## Configuring PISCOT

The simulator running parameters are configurable. 
Changing the parameters in the configuration file in "./src/MultiCoreSim/model/test_cfg.xml", allows you to configure number of cores, core frequencies, different cache configurations, queue sizes,..etc.

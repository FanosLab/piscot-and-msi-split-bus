/*
 * File  :      BusArbiter.cc
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 21, 2020
 */

#include "BusArbiter.h"

namespace ns3 {

    // override ns3 type
    TypeId BusArbiter::GetTypeId(void) {
        static TypeId tid = TypeId("ns3::BusArbiter")
               .SetParent<Object > ();
        return tid;
    }

    BusArbiter::BusArbiter(std::list<ns3::Ptr<ns3::BusIfFIFO> > associatedPrivCacheBusIfFIFO,
                           ns3::Ptr<ns3::BusIfFIFO>  assoicateLLCBusIfFIFO,
                           ns3::Ptr<ns3::InterConnectFIFO>  interConnectFIFO) {

        // default
        m_dt           = (1.0);
        m_clkSkew      = 0;
        m_cpuCore      = 4;
        m_arbiCycle    = 0;
        m_reqclks      = 4;
        m_respclks     = 50;
        m_workconserv  = false;
        m_reqCycleCnt  = 0;
        m_respCycleCnt = 0;
        m_reqCoreCnt   = 0;
        m_respCoreCnt  = 0;
        m_logFileGenEnable = 0;
        m_sharedCacheBusIfFIFO = assoicateLLCBusIfFIFO;
        m_busIfFIFO    = associatedPrivCacheBusIfFIFO;
        m_interConnectFIFO = interConnectFIFO;
        m_PndReq       = false;
        m_PndResp      = false;
        m_cach2Cache   = false;
        m_PndWb        = false;
        m_pndMemResp   = false;
        m_UpdateFlg    = false;
        m_PndReq2      = false;
        m_TimeOut      = 0;
        m_FcFsPndRespCoreId = -1;
        m_FcFsPndMemResp = false;
        m_PndPutMChk = false;
        m_PrevReqWasWbFromCPU = false;

    }

    // We don't do any dynamic allocations
    BusArbiter::~BusArbiter() {
    }

    void BusArbiter::SetDt (double dt) {
      m_dt = dt;
    }

    int BusArbiter::GetDt () {
      return m_dt;
    }

    void BusArbiter::SetClkSkew (double clkSkew) {
       m_clkSkew = clkSkew;
    }

    void BusArbiter::SetIsWorkConserv (bool workConservFlag) {
       m_workconserv = workConservFlag;
    }

    void BusArbiter::SetNumPrivCore (int nPrivCores) {
      m_cpuCore = nPrivCores;
    }

    void BusArbiter::SetNumReqCycles (int ncycle) {
      m_reqclks = ncycle;
    }

    void BusArbiter::SetNumRespCycles (int ncycle) {
      m_respclks = ncycle;
    }

    void BusArbiter::SetCache2Cache (bool cach2Cache) {
       m_cach2Cache = cach2Cache;
    }

    void BusArbiter::SetLogFileGenEnable (bool logFileGenEnable ) {
      m_logFileGenEnable = logFileGenEnable;
    }

   bool BusArbiter::FcFsWriteBackCheckInsert (uint16_t core_idx, uint64_t addr, bool CheckOnly = true) {
     bool PendingReq = false;
     // get the BusIf FIFO of the desired core
     std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin();
     std::advance(it1, core_idx);

     // get current size of the RxResp queue
     int pendingQueueSize = (*it1)->m_txRespFIFO.GetQueueSize();
     BusIfFIFO::BusRespMsg pendingWbMsg, txResp;

     // iterate over buffer to check if the WB message is there
     for (int i = 0; i < pendingQueueSize ;i++) {
       pendingWbMsg = (*it1)->m_txRespFIFO.GetFrontElement();
       // Remove message from the busResp buffer
       (*it1)->m_txRespFIFO.PopElement();
       if (pendingWbMsg.addr == addr ) {
         if (CheckOnly == true) {
           (*it1)->m_txRespFIFO.InsertElement(pendingWbMsg);
         }
         PendingReq = true;
         txResp = pendingWbMsg;
       }
       else {
         (*it1)->m_txRespFIFO.InsertElement(pendingWbMsg);
       }
     }

     // process pending response
     if (PendingReq == true && CheckOnly == false) {
         if (m_logFileGenEnable) {
           std::cout << "\nBusArbiter: Cpu " << (*it1)->m_fifo_id << " granted TDM response slot =============================================================== " << m_arbiCycle << "\n\n";
         }
       for(std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin(); it2 != m_busIfFIFO.end(); it2++) {
         if (txResp.reqCoreId == (*it2)->m_fifo_id){
           if (!(*it2)->m_rxRespFIFO.IsFull()) {
             (*it2)->m_rxRespFIFO.InsertElement(txResp);
           }
           else {
             if (m_logFileGenEnable) {
               std::cout << "BusArbiter cannot insert new messages into the buffers" << std::endl;
             }
             exit(0);
           }
         }
       }

       if (txResp.reqCoreId == m_sharedCacheBusIfFIFO->m_fifo_id || txResp.dualTrans == true) {
         txResp.reqCoreId = m_sharedCacheBusIfFIFO->m_fifo_id;
         // send message to LLC controller as well
         if (!m_sharedCacheBusIfFIFO->m_rxRespFIFO.IsFull()) {
             m_sharedCacheBusIfFIFO->m_rxRespFIFO.InsertElement(txResp);
         }
         else {
            if (m_logFileGenEnable) {
              std::cout << "BusArbiter cannot insert new messages into LLC RxResp buffers" << std::endl;
            }
            exit(0);
         }
       }

       // send message to Interconnect FIFO
       if (!m_interConnectFIFO->m_RespMsgFIFO.IsEmpty()) {
         m_interConnectFIFO->m_RespMsgFIFO.PopElement();
       }
       txResp.cycle = m_arbiCycle;
       m_interConnectFIFO->m_RespMsgFIFO.InsertElement(txResp);

       return true; 
     }

     return PendingReq;
   }

   bool BusArbiter::FcFsMemCheckInsert (uint16_t coreId, uint64_t addr, bool CheckOnly = true) {
     bool PendingReq = false;
     int pendingQueueSize = m_sharedCacheBusIfFIFO->m_txRespFIFO.GetQueueSize();
     BusIfFIFO::BusRespMsg pendingWbMsg, txResp;

     for (int i = 0; i < pendingQueueSize ;i++) {
       pendingWbMsg = m_sharedCacheBusIfFIFO->m_txRespFIFO.GetFrontElement();
       // Remove message from the busResp buffer
       m_sharedCacheBusIfFIFO->m_txRespFIFO.PopElement();
       if (pendingWbMsg.reqCoreId == coreId && pendingWbMsg.addr == addr) {
         if (CheckOnly == true) {
           m_sharedCacheBusIfFIFO->m_txRespFIFO.InsertElement(pendingWbMsg);
         }
         PendingReq = true;
         txResp = pendingWbMsg;
       }
       else {
         m_sharedCacheBusIfFIFO->m_txRespFIFO.InsertElement(pendingWbMsg);
       }
     }


     // process pending response
     if (PendingReq == true && CheckOnly == false) {
       if (m_logFileGenEnable) {
         std::cout << "BusArbiter: SharedMem granted TDM response slot ============================================================ " << m_arbiCycle << "\n\n";
       }
/*
       for(std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin(); it2 != m_busIfFIFO.end(); it2++) {
         if (!(*it2)->m_rxRespFIFO.IsFull()) {
           (*it2)->m_rxRespFIFO.InsertElement(txResp);
         }
         else {
           if (m_logFileGenEnable) {
             std::cout << "BusArbiter cannot insert new messages into the buffers" << std::endl;
           }
           exit(0);
         }
       }
*/
       for(std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin(); it2 != m_busIfFIFO.end(); it2++) {
         if (txResp.reqCoreId == (*it2)->m_fifo_id){
           if (!(*it2)->m_rxRespFIFO.IsFull()) {
             (*it2)->m_rxRespFIFO.InsertElement(txResp);
           }
           else {
             if (m_logFileGenEnable) {
               std::cout << "BusArbiter cannot insert new messages into the buffers" << std::endl;
             }
             exit(0);
           }
         }
       }

/*
       // send message to LLC controller as well
       if (!m_sharedCacheBusIfFIFO->m_rxRespFIFO.IsFull()) {
           m_sharedCacheBusIfFIFO->m_rxRespFIFO.InsertElement(txResp);
       }
       else {
          if (m_logFileGenEnable) {
            std::cout << "BusArbiter cannot insert new messages into LLC RxResp buffers" << std::endl;
          }
          exit(0);
       }
*/
       // send message to Interconnect FIFO
       if (!m_interConnectFIFO->m_RespMsgFIFO.IsEmpty()) {
         m_interConnectFIFO->m_RespMsgFIFO.PopElement();
       }
       txResp.cycle = m_arbiCycle;
       m_interConnectFIFO->m_RespMsgFIFO.InsertElement(txResp);

       return true; 
     }

     return PendingReq;
   }

   bool BusArbiter::CheckPendingFCFSReq (BusIfFIFO::BusReqMsg & txMsg) {
     double arrivalTime  = 0;
     uint16_t core_idx   = 0;
     uint16_t TargetCore = 0;
     bool PendingTxReq   = false;

     for(std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin(); it1 != m_busIfFIFO.end(); it1++) {
       if ((*it1)->m_txMsgFIFO.IsEmpty()==false) {
         txMsg = (*it1)->m_txMsgFIFO.GetFrontElement();
         //std::cout << "Request TS of Core " << core_idx << " = " << txMsg.timestamp << std::endl;
         if ((PendingTxReq == false) || arrivalTime > txMsg.timestamp){
           arrivalTime = txMsg.timestamp;
           TargetCore  = core_idx;
         }
         PendingTxReq = true;
       }
       core_idx++;
     }

     if (PendingTxReq) {
       //std::cout << "TargetCoreId " << TargetCore << " , timestamp = " << arrivalTime << std::endl;
       std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin();
       std::advance(it2, TargetCore);
       txMsg = (*it2)->m_txMsgFIFO.GetFrontElement();
       // Remove message from the busResp buffer
       (*it2)->m_txMsgFIFO.PopElement();
     }
     return PendingTxReq;

   }

   bool BusArbiter::CheckPendingReq (uint16_t core_idx, BusIfFIFO::BusReqMsg & txMsg, bool CheckOnly = false) {
     std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin();
     std::advance(it1, core_idx);
     bool PendingTxReq = false;

     if (!(*it1)->m_txMsgFIFO.IsEmpty()) {

       PendingTxReq = true;
       txMsg = (*it1)->m_txMsgFIFO.GetFrontElement();
       //std::cout << "CheckPendingReq core Id = " << core_idx << std::endl;

       if (PendingTxReq && CheckOnly == false) {
         // Remove message from the busResp buffer
         (*it1)->m_txMsgFIFO.PopElement();
       }
     }
     return PendingTxReq;
   }

   bool BusArbiter::CheckPendingPutM (BusIfFIFO::BusReqMsg reqMsg, BusIfFIFO::BusReqMsg & putmReq) {
            //std::cout << "CheckPendingPutM = " << std::endl;
     BusIfFIFO::BusReqMsg txreq;
     int pendingQueueSize;
     bool PendingPutmReq = false;

     for(std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin(); it1 != m_busIfFIFO.end() && (PendingPutmReq == false); it1++) {
       // check if core has pending requests
       pendingQueueSize = (*it1)->m_txMsgFIFO.GetQueueSize();
       for (int i = 0; i < pendingQueueSize ;i++) {
         txreq = (*it1)->m_txMsgFIFO.GetFrontElement();
         // Remove message from the busReq buffer
         (*it1)->m_txMsgFIFO.PopElement();
            //std::cout << "txreq.addr = " << txreq.addr << " addr = " << addr << std::endl;
         if (txreq.cohrMsgId == SNOOPPrivCohTrans::PutMTrans && (txreq.addr == reqMsg.addr) && (txreq.reqCoreId == reqMsg.reqCoreId)) {
           putmReq = txreq;
           PendingPutmReq = true;
         }
         // extra check if cache to cache is enabled
         else if (txreq.cohrMsgId == SNOOPPrivCohTrans::PutMTrans && m_cach2Cache &&
            ((txreq.addr >> (int) log2(64)) == (reqMsg.addr >> (int) log2(64))) &&
            (txreq.msgId == 0)) {
           putmReq = txreq;
           putmReq.addr      = reqMsg.addr;
           putmReq.msgId     = reqMsg.msgId;
           putmReq.reqCoreId = reqMsg.reqCoreId;
           PendingPutmReq    = true;
         }

         else {
           (*it1)->m_txMsgFIFO.InsertElement(txreq);
         }
       }
     }
     return PendingPutmReq;  
   }

   bool BusArbiter::InsertOnReqBus (BusIfFIFO::BusReqMsg txMsg) {
     if (m_logFileGenEnable) {
       if ( txMsg.cohrMsgId == SNOOPPrivCohTrans::PutMTrans){
         std::cout << "\nBusArbiter: Cpu " << txMsg.wbCoreId << " granted TDM request slot =============================================================== " << m_arbiCycle <<
         std::endl;
       }
       else {
         std::cout << "\nBusArbiter: Cpu " << txMsg.reqCoreId << " granted TDM request slot =============================================================== " << m_arbiCycle <<
         std::endl;
       }
     }
     // broadcast requests to all cores (snooping based)
     for(std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin(); it2 != m_busIfFIFO.end(); it2++) {
       if (!(*it2)->m_rxMsgFIFO.IsFull()) {
         (*it2)->m_rxMsgFIFO.InsertElement(txMsg);
       }
       else {
         if (m_logFileGenEnable) {
           std::cout << "BusArbiter: cannot insert new messages into PrivCache Ctrl buffers" << std::endl;
         }
         exit(0);
       }
     }
     // send request to Shared Mem controller as well
     if (!m_sharedCacheBusIfFIFO->m_rxMsgFIFO.IsFull()) {
       m_sharedCacheBusIfFIFO->m_rxMsgFIFO.InsertElement(txMsg);
     }
     else {
       if (m_logFileGenEnable) {
         std::cout << "BusArbiter: cannot insert new messages into SharedMem RxMsg buffers" << std::endl;
       }
       exit(0);
     }

     // send message to Interconnect FIFO
     if (!m_interConnectFIFO->m_ReqMsgFIFO.IsEmpty()) {
       m_interConnectFIFO->m_ReqMsgFIFO.PopElement();
     }
     txMsg.cycle = m_arbiCycle;
     m_interConnectFIFO->m_ReqMsgFIFO.InsertElement(txMsg);
     return true;
   }

   bool BusArbiter::CheckPendingMsg (uint16_t core_idx, ArbiterMsgType msg, double & arrivalTime) {
     bool EmptyTxResp = true;
     std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin();
     if (msg == InsertCoreRequest) {
       // check if core has pending requests
       std::advance(it1, core_idx);
       EmptyTxResp = (*it1)->m_txMsgFIFO.IsEmpty();
     }
     else if (msg == InsertCoreResponse) {
       std::advance(it1, core_idx);
       EmptyTxResp = (*it1)->m_txRespFIFO.IsEmpty();
     }
     else if (msg == InsertMemResponse) {
       if (!m_sharedCacheBusIfFIFO->m_txRespFIFO.IsEmpty()) {
         BusIfFIFO::BusRespMsg topRespMsg = m_sharedCacheBusIfFIFO->m_txRespFIFO.GetFrontElement();
         arrivalTime = topRespMsg.timestamp;
         EmptyTxResp = false;
       }
     }
     else { // InsertMemResponseCoreId
       std::advance(it1, core_idx);
       uint16_t coreId = (*it1)->m_fifo_id;
       if (!m_sharedCacheBusIfFIFO->m_txRespFIFO.IsEmpty()) {
         BusIfFIFO::BusRespMsg topRespMsg = m_sharedCacheBusIfFIFO->m_txRespFIFO.GetFrontElement();
         if (topRespMsg.reqCoreId == coreId) {
           EmptyTxResp = false;
         }
       }
     }
     return !EmptyTxResp;
   }

   // get first response message
  // implement first come first serve on response bus:
  // return -1 if there is no pending WB in the response core buffers
   int BusArbiter::CheckFcFsMsg (double & arrivalTime) {
     BusIfFIFO::BusRespMsg txResp;
     int core_idx = -1;
     int core_cnt = 0;
     double leastTs = 10000000000;
     for(std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin(); it1 != m_busIfFIFO.end(); it1++) {
       // check if core has pending requests
       if (!(*it1)->m_txRespFIFO.IsEmpty()) {
         txResp = (*it1)->m_txRespFIFO.GetFrontElement();
         std::cout << "\nBusArbiter: Cpu " << (*it1)->m_fifo_id << " has pending WB Inserted at Time " << txResp.timestamp<< " ============================================================\n\n";
         if (leastTs > txResp.timestamp) {
           core_idx = core_cnt;
           leastTs = txResp.timestamp;
         }
       }
       core_cnt++;
     }
     arrivalTime = leastTs;
     return core_idx;  
   }

   bool BusArbiter::InsertOnBus (uint16_t core_idx, ArbiterMsgType msg, BusIfFIFO::BusReqMsg & txMsg, bool InsertFromBack = false) {
     if (msg == InsertCoreRequest) {
       std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin();
       std::advance(it1, core_idx);

       // check if core has pending requests
       if (!(*it1)->m_txMsgFIFO.IsEmpty()) {
         std::cout << "\nBusArbiter: Cpu " << (*it1)->m_fifo_id << " granted TDM request slot =============================================================== " << m_arbiCycle << "\n\n";
         if (InsertFromBack == true) {
           txMsg = (*it1)->m_txMsgFIFO.GetBackElement();
           (*it1)->m_txMsgFIFO.PopBackElement();
         }
         else {
           txMsg = (*it1)->m_txMsgFIFO.GetFrontElement();
           (*it1)->m_txMsgFIFO.PopElement();
         }
         // broadcast requests to all cores (snooping based)
         for(std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin(); it2 != m_busIfFIFO.end(); it2++) {
           if (!(*it2)->m_rxMsgFIFO.IsFull()) {
             (*it2)->m_rxMsgFIFO.InsertElement(txMsg);
           }
           else {
             std::cout << "BusArbiter: cannot insert new messages into PrivCache Ctrl buffers" << std::endl;
             exit(0);
           }
         }
         // send request to Shared Mem controller as well
         if (!m_sharedCacheBusIfFIFO->m_rxMsgFIFO.IsFull()) {
           m_sharedCacheBusIfFIFO->m_rxMsgFIFO.InsertElement(txMsg);
         }
         else {
           std::cout << "BusArbiter: cannot insert new messages into SharedMem RxMsg buffers" << std::endl;
           exit(0);
         }

         // send message to Interconnect FIFO
         if (!m_interConnectFIFO->m_ReqMsgFIFO.IsEmpty()) {
           m_interConnectFIFO->m_ReqMsgFIFO.PopElement();
         }
         txMsg.cycle = m_arbiCycle;
         m_interConnectFIFO->m_ReqMsgFIFO.InsertElement(txMsg);

         return true; 
       }
       else {
         return false;
       }
     } // if (msg == InsertNewRequest) {
     else {
       bool EmptyTxResp;
       BusIfFIFO::BusRespMsg txResp;

       // check if response agent has pending message
       if (msg == InsertCoreResponse) {
         std::list<Ptr<BusIfFIFO>>::iterator it1 = m_busIfFIFO.begin();
         std::advance(it1, core_idx);
         EmptyTxResp = (*it1)->m_txRespFIFO.IsEmpty();
         if (!EmptyTxResp) {
           std::cout << "BusArbiter: Cpu " << (*it1)->m_fifo_id  << " granted TDM response slot ================================================================ " << m_arbiCycle << "\n\n";
           txResp = (*it1)->m_txRespFIFO.GetFrontElement();
           (*it1)->m_txRespFIFO.PopElement();
         }
       }
       else {
         EmptyTxResp = m_sharedCacheBusIfFIFO->m_txRespFIFO.IsEmpty();
         if (!EmptyTxResp) {
           std::cout << "BusArbiter: SharedMem granted TDM response slot ============================================================ " << m_arbiCycle << "\n\n";
           txResp = m_sharedCacheBusIfFIFO->m_txRespFIFO.GetFrontElement();
           m_sharedCacheBusIfFIFO->m_txRespFIFO.PopElement();
         }
       }
       // process pending response
       if (!EmptyTxResp) {

         for(std::list<Ptr<BusIfFIFO>>::iterator it2 = m_busIfFIFO.begin(); it2 != m_busIfFIFO.end(); it2++) {
           if (!(*it2)->m_rxRespFIFO.IsFull()) {
             (*it2)->m_rxRespFIFO.InsertElement(txResp);
           }
           else {
             std::cout << "BusArbiter cannot insert new messages into the buffers" << std::endl;
             exit(0);
           }
         }
         // send message to LLC controller as well
         if (!m_sharedCacheBusIfFIFO->m_rxRespFIFO.IsFull()) {
             m_sharedCacheBusIfFIFO->m_rxRespFIFO.InsertElement(txResp);
         }
         else {
            std::cout << "BusArbiter cannot insert new messages into LLC RxResp buffers" << std::endl;
            exit(0);
         }

         // send message to Interconnect FIFO
         if (!m_interConnectFIFO->m_RespMsgFIFO.IsEmpty()) {
           m_interConnectFIFO->m_RespMsgFIFO.PopElement();
         }
         txResp.cycle = m_arbiCycle;
         m_interConnectFIFO->m_RespMsgFIFO.InsertElement(txResp);

         return true; 
       }
       else {
         return false; 
       }
     } 
   }


    void BusArbiter::PMSI_TDM() {      
      double arrivalTime;
      BusIfFIFO::BusReqMsg txMsg;
      // Schedule the next run 
      // work conservative TDM
      if (m_PndReq) {
        m_PndReq = InsertOnBus (m_reqCoreCnt, InsertCoreRequest, txMsg);
        m_PndReq = false;
        m_PndReq2 = true;
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::PMSI_TDM, Ptr<BusArbiter > (this));
        return;
      }
 
      if (m_PndReq2) {
        if(CheckPendingMsg (m_reqCoreCnt, InsertCoreRequest , arrivalTime)) {
          m_PndReq = InsertOnBus (m_reqCoreCnt, InsertCoreRequest, txMsg);
        }
        Simulator::Schedule(NanoSeconds(m_dt*(m_respclks-2*m_reqclks)), &BusArbiter::PMSI_TDM, Ptr<BusArbiter > (this));
        m_UpdateFlg = true;
        m_PndReq2   = false;
        return;
      }

      if (m_UpdateFlg) {
        std::cout << "Debug Check Update Flag" << std::endl;
        m_PndWb  = CheckPendingMsg (m_reqCoreCnt, InsertCoreResponse, arrivalTime);
        m_pndMemResp = CheckPendingMsg (m_reqCoreCnt, InsertMemResponseCoreId, arrivalTime);
      }
      
      if (m_PndWb) {
        std::cout << "Debug Check m_PndWb" << std::endl;
        m_PndWb = InsertOnBus (m_reqCoreCnt, InsertCoreResponse, txMsg);
      }
      else if (m_pndMemResp) {
        std::cout << "Debug Check m_pndMemResp" << std::endl;
        m_pndMemResp = InsertOnBus (m_reqCoreCnt, InsertMemResponse, txMsg);
      }

      if (m_UpdateFlg || m_PndWb || m_pndMemResp) {
        m_UpdateFlg  = false;
        m_pndMemResp = false;
        m_PndWb      = false;
        m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      for (int i = 0; i < m_cpuCore && (m_PndReq || m_PndWb || m_pndMemResp) == false; i++) {
        // check for pending events
        m_PndReq = CheckPendingMsg (m_reqCoreCnt, InsertCoreRequest , arrivalTime);
        m_PndWb  = CheckPendingMsg (m_reqCoreCnt, InsertCoreResponse, arrivalTime);
        m_pndMemResp = CheckPendingMsg (m_reqCoreCnt, InsertMemResponseCoreId, arrivalTime);



        if (!(m_PndReq || m_PndWb || m_pndMemResp))
          m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::PMSI_TDM, Ptr<BusArbiter > (this));
      }
      else if (m_PndWb || m_pndMemResp ) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks), &BusArbiter::PMSI_TDM, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::PMSI_TDM, Ptr<BusArbiter > (this));
      }
    }

    void BusArbiter::ReqCycleProcess() {   
      double arrivalTime; 
      BusIfFIFO::BusReqMsg txMsg;
  
      // Schedule the next run 
      // work conservative TDM
      if (m_PndReq) {
        m_PndReq = InsertOnBus (m_reqCoreCnt, InsertCoreRequest, txMsg);
        m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
        m_PndReq = false;
      }

      for (int i = 0; i < m_cpuCore && m_PndReq == false; i++) {
        m_PndReq = CheckPendingMsg (m_reqCoreCnt, InsertCoreRequest, arrivalTime);
        if (!m_PndReq)
          m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
    }


    void BusArbiter::RespCycleProcess() {
      double arrivalTime;    
      BusIfFIFO::BusReqMsg txMsg;   
      // Schedule the next run 
      // work conservative TDM
      if (m_PndResp) {
        m_PndResp = (m_respCoreCnt == m_cpuCore) ? InsertOnBus (m_respCoreCnt, InsertMemResponse, txMsg) :
                                                   InsertOnBus (m_respCoreCnt, InsertCoreResponse, txMsg);
        m_respCoreCnt = (m_respCoreCnt == m_cpuCore) ? 0 : (m_respCoreCnt + 1); 
        m_PndResp = false;
      }

      for (int i = 0; i <= m_cpuCore && m_PndResp == false; i++) {
        m_PndResp = (m_respCoreCnt == m_cpuCore) ? CheckPendingMsg (m_respCoreCnt, InsertMemResponse, arrivalTime) :
                                                   CheckPendingMsg (m_respCoreCnt, InsertCoreResponse, arrivalTime);
        if (!m_PndResp)
          m_respCoreCnt = (m_respCoreCnt == m_cpuCore) ? 0 : (m_respCoreCnt + 1); 
      }

      if (m_PndResp) {
        // wait one TDM response slot
        std::cout << "Pending Resp Core Id " << m_respCoreCnt << std::endl;
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
    }

    void BusArbiter::FcFsReqCycleProcess() {  

      BusIfFIFO::BusReqMsg tempPutmMsg;     
      if (m_PndReq) {
        InsertOnReqBus (m_ReqBusMsg);
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks/2), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
        m_PndPutMChk = true;
        m_PndReq = false;
        return;
      }

      if (m_PndPutMChk) {
        // check if there is a pending PutM issued from other cores
        // This PutM has to broatcasted independed of the Bus 
        // Arbiteration Policy.
        if (CheckPendingPutM(m_ReqBusMsg, tempPutmMsg)){
          InsertOnReqBus (tempPutmMsg);
          m_GlobalReqFIFO.InsertElement(tempPutmMsg);
          if (m_logFileGenEnable) {
            std::cout << "BusArbiter: Insert Put(M) on the Bus from Core " << tempPutmMsg.wbCoreId <<"============================================================\n\n";
          }
        }

        // sort Resquest Messages
        if (m_ReqBusMsg.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          int pendingQueueSize = m_GlobalReqFIFO.GetQueueSize();
          bool InsertionDone = false;
          BusIfFIFO::BusReqMsg txResp;
          for (int i = 0; i < pendingQueueSize ;i++) {
            txResp = m_GlobalReqFIFO.GetFrontElement();
            // Remove message from the busResp buffer
            m_GlobalReqFIFO.PopElement();

            if (InsertionDone == false &&(txResp.cohrMsgId == SNOOPPrivCohTrans::GetSTrans || SNOOPPrivCohTrans::GetMTrans) && 
               ((txResp.addr >> (int) log2(64)) ==  (m_ReqBusMsg.addr >> (int) log2(64))) ) {
              m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
              m_GlobalReqFIFO.InsertElement(txResp);
              InsertionDone = true;
            }
            else {
              m_GlobalReqFIFO.InsertElement(txResp);
            }
          }

          if (InsertionDone == false) {
            m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
          }
        }
        else if (m_ReqBusMsg.cohrMsgId != SNOOPPrivCohTrans::UpgTrans) {
          m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
        }

        // advance TDM slot pointer
        m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
        m_PndPutMChk = false;
      }


      for (int i = 0; i < m_cpuCore && m_PndReq == false; i++) {
        m_PndReq = CheckPendingReq (m_reqCoreCnt, m_ReqBusMsg);
        if (!m_PndReq)
          m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks/2), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
    }

    void BusArbiter::FcFsRespCycleProcess2() {

      if (m_PndResp || m_FcFsPndMemResp) {

        //if (m_PndResp) 
        //  std::cout << " m_PndResp  = " << m_PndResp << " reqCoreId = " << pendingReq.reqCoreId << " pendingReq.addr = " << pendingReq.addr << std::endl;
        m_FcFsPndMemResp = (m_FcFsPndMemResp == true) ? FcFsMemCheckInsert (m_respCoreCnt, pendingReq.addr, false) :
                                                        FcFsWriteBackCheckInsert (m_respCoreCnt, pendingReq.addr, false);

        m_GlobalReqFIFO.PopElement();
        if (m_cach2Cache && m_PndResp && pendingReq.msgId != 0) { // remove GetS/M if cache to cache, except if the PutM was a relpacement one
          m_GlobalReqFIFO.PopElement();
        }

      }

      m_PndResp        = false;
      m_FcFsPndMemResp = false;
      if (!m_GlobalReqFIFO.IsEmpty()) {
        pendingReq = m_GlobalReqFIFO.GetFrontElement();
        if (pendingReq.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          // this is a write back slot 
          m_respCoreCnt = pendingReq.wbCoreId;
          m_PndResp = FcFsWriteBackCheckInsert (m_respCoreCnt, pendingReq.addr);
          //std::cout << " m_PndResp  = " << m_PndResp << " m_respCoreCnt = " << m_respCoreCnt << " pendingReq.addr = " << pendingReq.addr << std::endl;
        }
        else {
          m_respCoreCnt = pendingReq.reqCoreId;
          m_FcFsPndMemResp = FcFsMemCheckInsert (m_respCoreCnt, pendingReq.addr);
          //std::cout << " m_FcFsPndMemResp  = " << m_FcFsPndMemResp << " m_respCoreCnt = " << m_respCoreCnt << " pendingReq.addr = " << pendingReq.addr << std::endl;
        }
      }

      if (m_PndResp == true || m_FcFsPndMemResp == true) {
        // wait complete TDM response slot
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
    }

    void BusArbiter::FcFsRespCycleProcess() {
       double CoreRespArrivalTime,
              SharedMemArrivalTime;
      BusIfFIFO::BusReqMsg txMsg;
      // implement First come First serve on response bus
      if (m_FcFsPndMemResp) {
        InsertOnBus (m_cpuCore, InsertMemResponse, txMsg);
        m_PrevReqWasWbFromCPU = false;
        m_FcFsPndMemResp      = false;
        //return;
      }
      else if (m_FcFsPndRespCoreId!=-1) {
        InsertOnBus (m_FcFsPndRespCoreId, InsertCoreResponse, txMsg);
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks/2.0), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
        m_PrevReqWasWbFromCPU = true;
        m_FcFsPndRespCoreId   = -1;
        return;
      }

      // check for pending requests
      m_FcFsPndRespCoreId = CheckFcFsMsg(CoreRespArrivalTime);
      m_FcFsPndMemResp    = CheckPendingMsg (m_cpuCore, InsertMemResponse, SharedMemArrivalTime);

      if (m_FcFsPndMemResp == true && m_FcFsPndRespCoreId !=-1 && m_PrevReqWasWbFromCPU == false) {
        if (CoreRespArrivalTime <  SharedMemArrivalTime) {
          std::cout << "Arbiter CoreResp WB is priortized over SharedMem" << std::endl;
          m_FcFsPndMemResp = false;
        }
        else {
          std::cout << "Arbiter SharedMem Resp is priortized over CoreResp WB" << std::endl;
          m_FcFsPndRespCoreId = -1;
        }
      }
      

      if (m_FcFsPndRespCoreId !=-1 && m_FcFsPndMemResp == false) {
        // wait half TDM response slot
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks/2.0), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
      else if (m_FcFsPndMemResp == true) {
        // wait complete TDM response slot
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
    }

    void BusArbiter::FcFsReqCycleProcess2() {  

      if (m_PndReq) {
        InsertOnReqBus (m_ReqBusMsg);
        m_PndPutMChk = true;
        m_PndReq = false;
      }

      if (m_PndPutMChk) {

        // sort Resquest Messages
        if (m_ReqBusMsg.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          int pendingQueueSize = m_GlobalReqFIFO.GetQueueSize();
          bool InsertionDone = false;
          BusIfFIFO::BusReqMsg txResp;
          for (int i = 0; i < pendingQueueSize ;i++) {
            txResp = m_GlobalReqFIFO.GetFrontElement();
            // Remove message from the busResp buffer
            m_GlobalReqFIFO.PopElement();

            if (InsertionDone == false &&(txResp.cohrMsgId == SNOOPPrivCohTrans::GetSTrans || SNOOPPrivCohTrans::GetMTrans) && 
               ((txResp.addr >> (int) log2(64)) ==  (m_ReqBusMsg.addr >> (int) log2(64))) ) {
              //std::cout << "Don't insert PutM into global buffer" << txResp.reqCoreId << std::endl;
              //m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
              m_GlobalReqFIFO.InsertElement(txResp);
              InsertionDone = true;
            }
            else {
              m_GlobalReqFIFO.InsertElement(txResp);
            }
          }

          if (InsertionDone == false) {
            m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
          }
        }
        else if (m_ReqBusMsg.cohrMsgId != SNOOPPrivCohTrans::UpgTrans) {
          //std::cout << "Globle Buffer Msg inserted from Core " << m_ReqBusMsg.reqCoreId << std::endl;
          m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
        }

        // advance TDM slot pointer
        m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
        m_PndPutMChk = false;
      }


      for (int i = 0; i < m_cpuCore && m_PndReq == false; i++) {
        m_PndReq = CheckPendingReq (m_reqCoreCnt, m_ReqBusMsg);
        if (!m_PndReq)
          m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
    }

    // PISCOT OOO execution
    void BusArbiter::FcFsReqCycleProcess4() {  

      if (m_PndReq) {
        InsertOnReqBus (m_ReqBusMsg);
        m_PndPutMChk = true;
        m_PndReq = false;
      }

      if (m_PndPutMChk) {
        bool InsertionDone = false;

        int pendingQueueSize = m_GlobalReqFIFO.GetQueueSize();
        BusIfFIFO::BusReqMsg txResp;
        int pending_cnt [4];
        uint16_t reqCoreId = m_ReqBusMsg.reqCoreId;
        for (int i = 0; i < 4; i++) pending_cnt[i] = 0;

        // Check if PutM need to be inserted
        if (m_ReqBusMsg.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          for (int i = 0; i < pendingQueueSize ;i++) {
            txResp = m_GlobalReqFIFO.GetFrontElement();
            // Remove message from the busResp buffer
            m_GlobalReqFIFO.PopElement();

            if (InsertionDone == false &&(txResp.cohrMsgId == SNOOPPrivCohTrans::GetSTrans || SNOOPPrivCohTrans::GetMTrans) && 
               ((txResp.addr >> (int) log2(64)) ==  (m_ReqBusMsg.addr >> (int) log2(64))) ) {
              //std::cout << "Don't insert PutM into global buffer" << txResp.reqCoreId << std::endl;
              InsertionDone = true;
            }
            m_GlobalReqFIFO.InsertElement(txResp);
          }
        }

        if (InsertionDone == false) {
          pending_cnt[reqCoreId] = 1;
          for (int i = 0; i < pendingQueueSize ;i++) {
            txResp = m_GlobalReqFIFO.GetFrontElement();

            // Remove message from the busResp buffer
            m_GlobalReqFIFO.PopElement();

            pending_cnt[txResp.reqCoreId] = pending_cnt[txResp.reqCoreId] + 1;

            std::cout << "Arbiter Global Buffer entry i = " << i << " coreId " << txResp.reqCoreId << " addr " << txResp.addr << " Msg-2-insert from CoreId " << reqCoreId << std::endl;

            if (txResp.reqCoreId != reqCoreId && (pending_cnt[txResp.reqCoreId] > pending_cnt[reqCoreId]) && InsertionDone == false) {
              std::cout << " New Msg inserted before above entry" << reqCoreId << std::endl;
              m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
              m_GlobalReqFIFO.InsertElement(txResp);
              InsertionDone = true;
            }
            else {
              m_GlobalReqFIFO.InsertElement(txResp);
              std::cout << " Dequeue FIFO, New Msg Not inserted" << reqCoreId << std::endl;
            }
          }

          if (InsertionDone == false) {
            m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
            std::cout << " New Msg inserted at the end of the Queue" << reqCoreId << std::endl;
          }
        }

        // advance TDM slot pointer
        m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
        m_PndPutMChk = false;
      }


      for (int i = 0; i < m_cpuCore && m_PndReq == false; i++) {
        m_PndReq = CheckPendingReq (m_reqCoreCnt, m_ReqBusMsg);
        if (!m_PndReq)
          m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
    }

    // OOO execution ver2
    void BusArbiter::FcFsReqCycleProcess5() {  

      int pendingQueueSize = m_GlobalReqFIFO.GetQueueSize();
      bool InsertionDone = false;
      BusIfFIFO::BusReqMsg txResp;

      // broadcast Msg
      if (m_PndReq) {
        m_PndReq = false;

        // insert Msg On But
        InsertOnReqBus (m_ReqBusMsg);

        // Check if PutM need to be inserted
        if (m_ReqBusMsg.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          //std::cout << "PutM from core Id" << m_ReqBusMsg.wbCoreId << " addr " << m_ReqBusMsg.addr << std::endl;
          for (int i = 0; i < pendingQueueSize ;i++) {
            txResp = m_GlobalReqFIFO.GetFrontElement();
            // Remove message from the busResp buffer
            m_GlobalReqFIFO.PopElement();

            if (InsertionDone == false &&(txResp.cohrMsgId == SNOOPPrivCohTrans::GetSTrans || SNOOPPrivCohTrans::GetMTrans) && 
               ((txResp.addr >> (int) log2(64)) ==  (m_ReqBusMsg.addr >> (int) log2(64))) ) {
              //InsertionDone = true;
              //std::cout << "PutM from core Id not inserted " << m_ReqBusMsg.wbCoreId << " addr " << m_ReqBusMsg.addr << std::endl;
            }
            m_GlobalReqFIFO.InsertElement(txResp);
          }
        }

        if (InsertionDone == false) {
          m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
        }

        // advance TDM slot pointer
        m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
      }

      // check pending request
      bool temp_PndReq = false;
      pendingQueueSize = m_GlobalReqFIFO.GetQueueSize();
      for (int i = 0; i < m_cpuCore && m_PndReq == false; i++) {
        // check if the core has pending request in its buffer
        temp_PndReq = CheckPendingReq (m_reqCoreCnt, m_ReqBusMsg, true);
        m_PndReq    = temp_PndReq;

        //std::cout << "Checking TDM request for core " << m_reqCoreCnt << " m_PndReq " << temp_PndReq << " pendingQueueSize = " << pendingQueueSize << std::endl;

        // check if that core has pending request in the global buffer
        //for (int j = 0; (j < pendingQueueSize && temp_PndReq == true && m_ReqBusMsg.cohrMsgId != SNOOPPrivCohTrans::PutMTrans) ;j++) {
        for (int j = 0; (j < pendingQueueSize && temp_PndReq == true) ;j++) {
          txResp = m_GlobalReqFIFO.GetFrontElement();
          // Remove message from the busResp buffer
          m_GlobalReqFIFO.PopElement();
          if (m_reqCoreCnt == txResp.reqCoreId) {
            m_PndReq = false;
          }
          m_GlobalReqFIFO.InsertElement(txResp);
            //std::cout << "Arbiter Global Buffer entry j = " << j << " coreId " << txResp.reqCoreId << " addr " << txResp.addr << " Msg-2-insert from CoreId " << m_reqCoreCnt <<  " m_PndReq " << m_PndReq << std::endl;
        }
                 
        if (!m_PndReq) {
          m_reqCoreCnt = (m_reqCoreCnt == m_cpuCore - 1) ? 0 : (m_reqCoreCnt + 1); 
        }
        else {
          m_PndReq     = CheckPendingReq (m_reqCoreCnt, m_ReqBusMsg, false); // fetch
        }
      }

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
    }


    void BusArbiter::FcFsReqCycleProcess3() {  

      if (m_PndReq) {
        InsertOnReqBus (m_ReqBusMsg);
        m_PndPutMChk = true;
        m_PndReq = false;
      }

      if (m_PndPutMChk) {

        int pendingQueueSize = m_GlobalReqFIFO.GetQueueSize();
        // sort Resquest Messages
        if (m_ReqBusMsg.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          bool InsertionDone = false;
          BusIfFIFO::BusReqMsg txResp;
          for (int i = 0; i < pendingQueueSize ;i++) {
            txResp = m_GlobalReqFIFO.GetFrontElement();
            // Remove message from the busResp buffer
            m_GlobalReqFIFO.PopElement();

            if (InsertionDone == false &&(txResp.cohrMsgId == SNOOPPrivCohTrans::GetSTrans || SNOOPPrivCohTrans::GetMTrans) && 
               ((txResp.addr >> (int) log2(64)) ==  (m_ReqBusMsg.addr >> (int) log2(64))) ) {
              //std::cout << "Don't insert PutM into global buffer" << txResp.reqCoreId << std::endl;
              //m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
              m_GlobalReqFIFO.InsertElement(txResp);
              InsertionDone = true;
            }
            else {
              m_GlobalReqFIFO.InsertElement(txResp);
            }
          }

          if (InsertionDone == false) {
            m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
          }
        }
        else if (m_ReqBusMsg.cohrMsgId != SNOOPPrivCohTrans::UpgTrans) {
          //std::cout << "Globle Buffer Msg inserted from Core " << m_ReqBusMsg.reqCoreId << " addr = " << m_ReqBusMsg.addr << " pendingQueueSize = " << pendingQueueSize << std::endl;
          m_GlobalReqFIFO.InsertElement(m_ReqBusMsg);
        }

        m_PndPutMChk = false;
      }

      // FCFS request
      m_PndReq = CheckPendingFCFSReq (m_ReqBusMsg);

      if (m_PndReq) {
        // wait one TDM Request slot
        Simulator::Schedule(NanoSeconds(m_dt*m_reqclks), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
        Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
      }
    }

    void BusArbiter::FcFsRespCycleProcess3() {

      if (m_PndResp || m_FcFsPndMemResp) {

        //if (m_PndResp) 
         //std::cout << " m_PndResp  = " << m_PndResp << " reqCoreId = " << pendingReq.reqCoreId << " pendingReq.addr = " << pendingReq.addr << std::endl;
        m_FcFsPndMemResp = (m_FcFsPndMemResp == true) ? FcFsMemCheckInsert (m_respCoreCnt, pendingReq.addr, false) :
                                             FcFsWriteBackCheckInsert (m_respCoreCnt, pendingReq.addr, false);

        if ((m_cach2Cache == false &&  ((m_PndResp == false) || (pendingReq.cohrMsgId == SNOOPPrivCohTrans::PutMTrans))) || m_cach2Cache == true) {
          m_GlobalReqFIFO.PopElement();
          //std::cout << "remove Request from global buffer" << std::endl;
        }
        //if (m_cach2Cache && m_PndResp && pendingReq.msgId != 0) { // remove GetS/M if cache to cache, except if the PutM was a relpacement one
        //  m_GlobalReqFIFO.PopElement();
        //}

      }

      m_PndResp        = false;
      m_FcFsPndMemResp = false;
      if (!m_GlobalReqFIFO.IsEmpty()) {
        pendingReq = m_GlobalReqFIFO.GetFrontElement();
        if (pendingReq.cohrMsgId == SNOOPPrivCohTrans::PutMTrans) {
          // this is a write back slot 
          m_respCoreCnt = pendingReq.wbCoreId;
          m_PndResp = FcFsWriteBackCheckInsert (m_respCoreCnt, pendingReq.addr);

          // remove PUTM!!
          if (m_PndResp == false) {
            m_TimeOut = m_TimeOut + 1;
            if (m_TimeOut == 5) {
              m_TimeOut = 0;
              m_GlobalReqFIFO.PopElement();
              //std::cout << "Pending PutM() is removed m_PndResp  = " << m_PndResp << " m_respCoreCnt = " << m_respCoreCnt << " pendingReq.addr = " << pendingReq.addr << std::endl;
            }
          }
          else {
              m_TimeOut = 0;
          }

          //std::cout << " m_PndResp  = " << m_PndResp << " m_respCoreCnt = " << m_respCoreCnt << " pendingReq.addr = " << pendingReq.addr << std::endl;
        }
        else {
          m_respCoreCnt = pendingReq.reqCoreId;
          m_FcFsPndMemResp = FcFsMemCheckInsert (m_respCoreCnt, pendingReq.addr);

          //std::cout << " m_FcFsPndMemResp  = " << m_FcFsPndMemResp << " m_respCoreCnt = " << m_respCoreCnt << " pendingReq.addr = " << pendingReq.addr << std::endl;

          if (m_FcFsPndMemResp == false) {
            for (int i = 0; i < m_cpuCore && m_PndResp == false; i++) {
              m_respCoreCnt = i;
              m_PndResp = FcFsWriteBackCheckInsert (m_respCoreCnt, pendingReq.addr);
              //std::cout << " m_PndResp  = " << m_PndResp << " m_respCoreCnt = " << m_respCoreCnt << " pendingReq.addr = " << pendingReq.addr << std::endl;
            }
          }

        }
      }

      if (m_PndResp == true || m_FcFsPndMemResp == true) {
        // wait complete TDM response slot
        Simulator::Schedule(NanoSeconds(m_dt*m_respclks), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
      else {
        // wait one clock cycle and check again
          Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
      }
    }

    // The init function starts the controller at the beginning 
    void BusArbiter::init() {
        Simulator::Schedule(NanoSeconds(m_clkSkew), &BusArbiter::ReqStep, Ptr<BusArbiter > (this));
        Simulator::Schedule(NanoSeconds(m_clkSkew), &BusArbiter::RespStep, Ptr<BusArbiter > (this));
        Simulator::Schedule(NanoSeconds(m_clkSkew), &BusArbiter::Step, Ptr<BusArbiter > (this));
        //Simulator::Schedule(NanoSeconds(m_clkSkew), &BusArbiter::PMSI_TDM, Ptr<BusArbiter > (this));
    }

    /**
     * Runs one mobility Step for the given vehicle generator.
     * This function is called each interval dt
     */

    void BusArbiter::ReqStep(Ptr<BusArbiter> busArbiter) {
       // busArbiter->FcFsReqCycleProcess3 (); // MSI FCFS
        busArbiter->FcFsReqCycleProcess5 (); // latest PISCOT + OOO ver2
    }

    void BusArbiter::RespStep(Ptr<BusArbiter> busArbiter) {
        busArbiter->FcFsRespCycleProcess3(); // latest PISCOT + MSI
    }

    void BusArbiter::Step(Ptr<BusArbiter> busArbiter) {
      // Schedule the next run
      busArbiter->CycleAdvance();
    }

    void BusArbiter::CycleAdvance () {
      Simulator::Schedule(NanoSeconds(m_dt), &BusArbiter::Step, Ptr<BusArbiter > (this));
      m_arbiCycle++;
    }

}

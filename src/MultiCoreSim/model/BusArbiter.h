/*
 * File  :      BusArbiter.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 21, 2020
 */

#ifndef _BusArbiter_H
#define _BusArbiter_H

#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"
#include "MemTemplate.h"
#include "PMSI.h"

namespace ns3 { 
  /**
   * brief BusArbiter manage the scheduling of the different core
   * message on the shared interconnect
  */
  
  class BusArbiter : public ns3::Object {
  private:
    enum ArbiterMsgType {
      InsertCoreRequest = 0,
      InsertCoreResponse,
      InsertMemResponse,
      InsertMemResponseCoreId,
    };

    double   m_dt; 
    double   m_clkSkew; 
    uint16_t m_cpuCore;
    uint16_t m_reqclks;
    uint16_t m_respclks;
    bool     m_workconserv;
    uint16_t m_reqCycleCnt;
    uint16_t m_respCycleCnt;
    uint16_t m_reqCoreCnt;
    uint16_t m_respCoreCnt;
    uint64_t m_arbiCycle;
    bool     m_logFileGenEnable;
    bool     m_cach2Cache;
    bool     m_PndReq,
             m_PndResp,
             m_PndWb,
             m_pndMemResp,
             m_UpdateFlg,
             m_PndReq2;

    int      m_FcFsPndRespCoreId;

    int      m_TimeOut;

    bool     m_FcFsPndMemResp,
             m_PndPutMChk,
             m_PrevReqWasWbFromCPU;

    BusIfFIFO::BusReqMsg m_ReqBusMsg;
    BusIfFIFO::BusReqMsg pendingReq;

    GenericDeque <BusIfFIFO::BusReqMsg> m_GlobalReqFIFO;
    // A list of Cache Ctrl Bus interface buffers
    std::list<ns3::Ptr<ns3::BusIfFIFO> > m_busIfFIFO;

    // A pointer to shared cache Bus IF buffers
    ns3::Ptr<ns3::BusIfFIFO> m_sharedCacheBusIfFIFO;

     // A pointer to Inteconnect FIFOs
    ns3::Ptr<ns3::InterConnectFIFO>  m_interConnectFIFO;

    // insert new message on the bus
    bool InsertOnBus (uint16_t core_idx,ArbiterMsgType msg, BusIfFIFO::BusReqMsg & txMsg, bool InsertFromBack);
    bool CheckPendingMsg (uint16_t core_idx, ArbiterMsgType msg, double & arrivalTime);

    void CycleAdvance ();
    void ReqCycleProcess();
    void RespCycleProcess();
    void PMSI_TDM();
    int CheckFcFsMsg (double & arrivalTime);
    bool CheckPendingPutM (BusIfFIFO::BusReqMsg reqMsg, BusIfFIFO::BusReqMsg & putmReq);
    bool CheckPendingReq  (uint16_t core_idx, BusIfFIFO::BusReqMsg & txMsg, bool CheckOnly);
    bool InsertOnReqBus (BusIfFIFO::BusReqMsg txMsg);
    bool FcFsMemCheckInsert (uint16_t coreId,uint64_t addr, bool CheckOnly);
    bool FcFsWriteBackCheckInsert (uint16_t core_idx, uint64_t addr, bool CheckOnly);
    void FcFsRespCycleProcess();
    void FcFsReqCycleProcess();
    void FcFsRespCycleProcess2();

    void FcFsReqCycleProcess2();
    void FcFsRespCycleProcess3();

    void FcFsReqCycleProcess3();
    bool CheckPendingFCFSReq (BusIfFIFO::BusReqMsg & txMsg);

    void FcFsReqCycleProcess4();
    void FcFsReqCycleProcess5();

  public:
    // Override TypeId.
    static TypeId GetTypeId(void);

    BusArbiter(std::list<ns3::Ptr<ns3::BusIfFIFO> > associatedPrivCacheBusIfFIFO,
                         ns3::Ptr<ns3::BusIfFIFO>   assoicateLLCBusIfFIFO,
                         ns3::Ptr<ns3::InterConnectFIFO>  interConnectFIFO);


    ~BusArbiter();

    void SetDt (double dt);

    int GetDt ();

    void SetClkSkew (double clkSkew);

    void SetNumPrivCore (int nPrivCores);

    void SetLogFileGenEnable (bool logFileGenEnable);

    void SetNumReqCycles (int ncycle);

    void SetNumRespCycles (int ncycle);
 
    void SetIsWorkConserv (bool workConservFlag);

    void SetCache2Cache (bool cach2Cache);
   
    void init();

    /**
     * Run PrivateCacheCtrl every clock cycle to
     * update cache line states and generate coherence 
     * messages. This function does the scheduling
     */
     static void ReqStep(Ptr<BusArbiter> busArbiter);
     static void RespStep(Ptr<BusArbiter> busArbiter);
     static void Step(Ptr<BusArbiter> busArbiter);
  };


}

#endif /* _BusArbiter_H */

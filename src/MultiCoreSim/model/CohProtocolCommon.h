/*
 * File  :      CohProtocolCommon.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 29, 2020
 */


#ifndef _CohProtocolCommon_H
#define _CohProtocolCommon_H


#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

#include "SNOOPProtocolCommon.h"
#include "IFCohProtocol.h"

#include "MSI.h"
#include "PMSI.h"

// Add new protocol here ...

namespace ns3 { 


  struct PrivCacheState {
    SNOOP_PMSIPrivCacheState SNOOP_PMSIState;
    SNOOP_MSIPrivCacheState  SNOOP_MSIState;
    // Add new protocol here ...
  };



}

#endif /* _CohProtocolCommon_H */

/*
 * File  :      GenericCache.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 30, 2020
 */

#ifndef _GenericCache_H
#define _GenericCache_H


#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

namespace ns3 { 


  // cache line defination
  class GenericCacheFrmt {
  public:
    bool     valid; // used with associated map only
    uint64_t tag;
    uint8_t data[8];
    int     state;
    GenericCacheFrmt () {
    tag   = 0;
    valid = false;
    for(int i = 0; i < 8; i++)
      data[i] = 0;
    }
    ~GenericCacheFrmt() {
    }
  };

  // address map format
  class GenericCacheMapFrmt {
  public:
    uint32_t idx_set;
    uint64_t tag;
    uint32_t ofst;
    GenericCacheMapFrmt () {
      idx_set   = 0;
      tag       = 0;
      ofst      = 0;
    }
    ~GenericCacheMapFrmt() {
    }
  };

  enum class ReplcPolicy {
    RANDOM,
    LRU
  };

  // generic cache template
  class GenericCache : public ns3::Object {
  private:
    GenericCacheFrmt * m_cache;
     uint16_t m_cacheType;
     uint32_t m_cacheSize;
     uint32_t m_cacheBlkSize;
     uint32_t m_nways;
     uint32_t m_nsets;
     Ptr<UniformRandomVariable> uRnd1;
  public:

    struct CacheLineInfo {
      bool IsExist;
      bool IsValid;
      bool IsSetFull;
      int  state;
      uint32_t cl_idx;
      uint32_t setOfst;
    };

    GenericCache(uint32_t size) {
        m_cache  = new GenericCacheFrmt[size];
        uRnd1 = CreateObject<UniformRandomVariable> ();
    }

    ~GenericCache() {
      delete [] m_cache;
    }

    void SetCacheSize (uint32_t cacheSize) {
      m_cacheSize = cacheSize;
    }

    uint32_t GetCacheSize () {
      return m_cacheSize;
    }

    void SetCacheBlkSize (uint32_t cacheBlkSize) {
      m_cacheBlkSize = cacheBlkSize;
    }

    uint32_t GetCacheBlkSize () {
      return m_cacheBlkSize;
    }

    void SetCacheNways (uint32_t nways) {
      m_nways = nways;
    }

    uint32_t GetCacheNways () {
      return m_nways;
    }

    void SetCacheNsets (uint32_t nsets) {
      m_nsets = nsets;
    }

    uint32_t GetCacheNsets () {
      return m_nsets;
    }

    void SetCacheType (uint16_t cacheType) {
      m_cacheType = cacheType;
    }

    uint16_t GetCacheType () {
      return m_cacheType;
    }

    void InitalizeCacheStates (int initialState) {
      for(uint32_t i = 0; i < m_nsets*m_nways; i++) {
        m_cache[i].state = initialState;
        m_cache[i].valid = false;
      }
    }

    void WriteCacheLine (GenericCacheFrmt line, uint32_t cl_idx) {
      m_cache[cl_idx] = line;
    }

    GenericCacheFrmt ReadCacheLine (uint32_t cl_idx) {
      return m_cache[cl_idx];
    }

    void SetCacheLineState(uint32_t cl_idx, int state) {
      m_cache[cl_idx].state = state;
    }

    int GetCacheLineState(uint32_t cl_idx) {
      return m_cache[cl_idx].state;
    }

    int GetCacheLineState(uint64_t phy_addr) {
      return m_cache[GetCacheLineIdx(phy_addr)].state;
    }

    // Map physical address to private cache format
    GenericCacheMapFrmt CpuAddrMap (uint64_t phy_addr) {
       /* Map physical address to private cache format 
       * PA  is shared memory physical address. For 2MB, PA is 24-bit byte address
       * BO  is the block offset bits = PA & BO_BIT_MASK (i.e. 0000_0000_0000_0000_0011_1111)
       * CI  is cache index bits = (PA >> BO) & CI_BIT_MASK (i.e. 0000_0000_0000_0000_1111_1111)
       * TAG is cache tage bits = PA >> (CI + BO) 
       */
       // FixMe: address width in the BMs are higher than 24-bits, 
       // so the tag values is not correct. It needs to be trunchated. 
      GenericCacheMapFrmt cacheLine;

      cacheLine.idx_set = (phy_addr >> (int) log2(m_cacheBlkSize)) & (m_nsets-1);  
      cacheLine.ofst    = phy_addr & (m_cacheBlkSize-1);  
      cacheLine.tag     = phy_addr >> ((int) log2(m_cacheBlkSize)+(int) log2(m_nsets));
      return cacheLine;
    }

    uint64_t CpuPhyAddr (GenericCacheMapFrmt cacheLine) {
      return (cacheLine.ofst + (cacheLine.idx_set << (int) log2(m_cacheBlkSize)) + (cacheLine.tag << ((int) log2(m_cacheBlkSize)+(int) log2(m_nsets))));
    }

    uint64_t CpuPhyAddr (uint32_t cl_idx) {
      GenericCacheFrmt cacheLine = m_cache[cl_idx];
      uint32_t setIdx = cl_idx/m_nways;
      uint32_t ofst   = 0;
      return (ofst + (setIdx << (int) log2(m_cacheBlkSize)) + (cacheLine.tag << ((int) log2(m_cacheBlkSize)+(int) log2(m_nsets))));
    }

    uint32_t GetCacheLineIdx (uint64_t phy_addr) {
      GenericCacheMapFrmt cacheLine;
      cacheLine = CpuAddrMap (phy_addr);
      return cacheLine.idx_set;
    }

    uint32_t GetReplacementLine (uint32_t set_idx, ReplcPolicy replcPolicy) {
      if (replcPolicy == ReplcPolicy::RANDOM) {
        uRnd1-> SetAttribute ("Min", DoubleValue (0));
        uRnd1-> SetAttribute ("Max", DoubleValue (m_nways-1));
        return (set_idx + uint32_t(uRnd1->GetValue()));
      }
      return set_idx;     
    }

    int GetEmptyCacheLine (uint64_t addr) {
       GenericCacheMapFrmt addrMap   = CpuAddrMap (addr);
       GenericCacheFrmt cacheLine;
       uint32_t setOfst = addrMap.idx_set * m_nways;
       for (uint32_t wayIdx = setOfst; wayIdx < setOfst+m_nways;wayIdx++) {
         cacheLine    = ReadCacheLine(wayIdx);
         if (cacheLine.valid == false) {
           return wayIdx;
         }
       }
       return -1;
    }

    CacheLineInfo GetCacheLineInfo (uint64_t addr) {
       CacheLineInfo lineInfo;
       lineInfo.IsExist   = false;
       lineInfo.IsValid   = false;
       lineInfo.IsSetFull = true;

       GenericCacheFrmt cacheLine;

       GenericCacheMapFrmt addrMap   = CpuAddrMap (addr);

       uint32_t setOfst = addrMap.idx_set * m_nways;

       lineInfo.setOfst   = setOfst;              
       lineInfo.cl_idx    = setOfst;

       for (uint32_t wayIdx = setOfst; wayIdx < setOfst+m_nways;wayIdx++) {
         cacheLine    = ReadCacheLine(wayIdx);

         // check address tag
         if (cacheLine.tag == addrMap.tag) {
           lineInfo.IsExist   = true;
           lineInfo.IsValid   = cacheLine.valid;
           lineInfo.state     = cacheLine.state;
           lineInfo.cl_idx    = wayIdx;
         }

         // Check if the current Set is full
         if (cacheLine.valid == false) {
           lineInfo.IsSetFull = false;
         }
       }

       return lineInfo;
    }

  }; // class GenericCache



}

#endif /* _GenericCache_H */



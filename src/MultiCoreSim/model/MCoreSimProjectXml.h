/*
 * File  :      MCoreSimProjectXml.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 15, 2020
 */

#ifndef _MCoreSimProjectXml_H
#define _MCoreSimProjectXml_H

#include <list>
#include <stdlib.h>
#include <string.h>
#include "tinyxml.h"
#include "CacheXml.h"

using namespace std;

class MCoreSimProjectXml {
private:
    int  m_numberOfRuns;
    int  m_totalTimeInSeconds;
    int  m_runTillSimEnd;
    int  m_busClkNanoSec;
    int  m_nCores;
    int  m_cpuFIFOSize;
    int  m_busFIFOSize;
    int  m_cach2Cache;

    list<CacheXml> m_privateCaches;
    CacheXml m_sharedCache;
    //SharedBusXml m_sharedBus;

    // The name of the path used for Benchmark trace files
    string m_bmsPath;

    // trace file names
    string m_cpuTraceFile;
    string m_cohCtrlsTraceFile;

    // enable flag for LogFile
    bool m_logFileGenEnable;

public:

    list<CacheXml> GetPrivateCaches() {
        return m_privateCaches;
    }

    void SetPrivateCaches(list<CacheXml> privateCaches) {
        m_privateCaches = privateCaches;
    }

    CacheXml GetSharedCache() {
       return m_sharedCache;
    }

    void SetSharedCache(CacheXml sharedCache) {
      m_sharedCache = sharedCache;
    }
  
    void SetBMsPath (string fileName) {
      m_bmsPath = fileName;
    }

    string GetBMsPath () {
      return m_bmsPath;
    }

    void SetCohCtrlsTraceFile (string fileName) {
      m_cohCtrlsTraceFile = fileName;
    }

    string GetCohCtrlsTraceFile () {
      return m_cohCtrlsTraceFile;
    }

    void SetCpuTraceFile (string fileName) {
      m_cpuTraceFile = fileName;
    }

    void SetLogFileGenEnable (bool logFileGenEnable ) {
      m_logFileGenEnable = logFileGenEnable;
    }

    bool GetLogFileGenEnable () {
      return m_logFileGenEnable;
    }

    string GetCpuTraceFile () {
      return m_cpuTraceFile;
    }

    int GetCpuFIFOSize () {
      return m_cpuFIFOSize;
    }

    int GetBusFIFOSize () {
      return m_busFIFOSize;
    }

/*  
    SharedBusXml GetSharedBus() {
       return m_sharedBus;
    }

    SetSharedBus(SharedBusXml sharedBus) {
       m_sharedBus = sharedBus;
    }
*/
    int GetNumberOfRuns() {
      return m_numberOfRuns;
    }

    void SetNumberOfRuns (int numberOfRuns) {
       m_numberOfRuns = numberOfRuns;
    }

    int GetBusClkInNanoSec() {
      return m_busClkNanoSec;
    }

    void SetBusClkInNanoSec(int busClkNanoSec) {
       m_busClkNanoSec = busClkNanoSec;
    }

    int GetTotalTimeInSeconds() {
      return m_totalTimeInSeconds;
    }

    void SetTotalTimeInSeconds(int totalTimeInSeconds) {
       m_totalTimeInSeconds = totalTimeInSeconds;
    }

    int GetRunTillSimEnd() {
      return m_runTillSimEnd;
    }

    int GetNumPrivCore () {
      return m_nCores;
    }

    int GetCache2Cache () {
      return m_cach2Cache;
    }

    // load input configurations
    void LoadFromXml (TiXmlHandle root) {
       m_numberOfRuns       = 1;
       m_totalTimeInSeconds = 5;
       m_runTillSimEnd      = 0;
       m_busClkNanoSec      = 100;
       m_nCores             = 4;
       m_cpuFIFOSize        = 6;
       m_busFIFOSize        = 6;
       m_cach2Cache         = true;
       m_privateCaches      = list<CacheXml> ();
       m_sharedCache        = CacheXml ();
       //m_sharedBus          = SharedBusXml ();



       TiXmlElement* rootPtr = root.Element();

       if (rootPtr != NULL) {
          rootPtr->QueryIntAttribute("numberOfRuns", &m_numberOfRuns);
          rootPtr->QueryIntAttribute("totalTimeInSeconds", &m_totalTimeInSeconds);
          rootPtr->QueryIntAttribute("RunTillEnd", &m_runTillSimEnd);
          rootPtr->QueryIntAttribute("busClkNanoSec", &m_busClkNanoSec);
          rootPtr->QueryIntAttribute("nCores", &m_nCores);
          rootPtr->QueryIntAttribute("cpuFIFOSize", &m_cpuFIFOSize);
          rootPtr->QueryIntAttribute("busFIFOSize", &m_busFIFOSize);
          rootPtr->QueryIntAttribute("Cache2Cache", &m_cach2Cache );     
          TiXmlHandle privateCachesRoot = root.FirstChildElement("privateCaches");
          TiXmlElement* privateCachesRootPtr = privateCachesRoot.Element();

          if (privateCachesRootPtr) {
             TiXmlElement* privateCachePtr = privateCachesRootPtr->FirstChildElement("privateCache");
             for (; privateCachePtr; privateCachePtr = privateCachePtr->NextSiblingElement()) {
               CacheXml newPrivateCache;
               TiXmlHandle privateCacheHandle = TiXmlHandle(privateCachePtr);
               newPrivateCache.LoadFromXml(privateCacheHandle);
               m_privateCaches.push_back(newPrivateCache);
             }
          }

          TiXmlHandle sharedCachesRoot = root.FirstChildElement("sharedCaches");
          TiXmlElement* sharedCachesRootPtr = sharedCachesRoot.Element();

          // FixMe: If multiple shared caches are possible
          if (sharedCachesRootPtr) {
             TiXmlElement* sharedCachePtr = sharedCachesRootPtr->FirstChildElement("sharedCache");
             TiXmlHandle sharedCacheHandle = TiXmlHandle(sharedCachePtr);
             m_sharedCache.LoadFromXml(sharedCacheHandle);
          }       
       }
    } // void LoadFromXml

};

#endif /* _MCoreSimProjectXml_H */

/*
 * File  :      MSI.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On June 18, 2020
 */

#include "MSI.h"

namespace ns3 { 

    // override ns3 type
    TypeId MSI::GetTypeId(void) {
        static TypeId tid = TypeId("ns3::MSI")
               .SetParent<Object > ();
        return tid;
    }

    MSI::MSI() {
        // default
    }

    // We don't do any dynamic allocations
    MSI::~MSI() {
    }


    // MSI Private Cache FSM
    void  MSI::CacheEventProcessing (SNOOPPrivEventType         eventType, 
                                     SNOOPPrivEventList         eventList,
                                     int                        &cacheState,     
                                     SNOOPPrivCohTrans          &trans2Issue,
                                     PMSI::PMSIEventPriority    &eventsPriority,
                                     SNOOPPrivCtrlAction        &ctrlAction,
                                     bool                       Cache2Cache) 
    {
       // current and next state variable
       SNOOP_MSIPrivCacheState  cacheState_c, 
                                cacheState_n; 

       // type case MSI State variable
       cacheState_c = static_cast<SNOOP_MSIPrivCacheState>(cacheState);

       // default assignments
       trans2Issue    = NullTrans;
       ctrlAction     = SNOOPPrivCtrlAction::NoAck;
       cacheState_n   = cacheState_c;
       eventsPriority = PMSI::ParallelEvents;

       // process CoreEvent
       if (eventType == SNOOPPrivEventType::Core) {
         switch (eventList.cpuReqEvent) {
           case SNOOPPrivCoreEvent::Load:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I: 
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_ad;
                 trans2Issue  = SNOOPPrivCohTrans::GetSTrans;
                 ctrlAction   = SNOOPPrivCtrlAction::issueTrans;
                 eventsPriority = PMSI::CoreReqBusEventsWorkConserv;
                 break;
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_ad:
                 ctrlAction     = SNOOPPrivCtrlAction::Hit;
                 eventsPriority = PMSI::RequestBusEvent;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::MI_a:
                 ctrlAction     = SNOOPPrivCtrlAction::Hit;
                 eventsPriority = PMSI::CoreReqBusEventsWorkConserv;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 ctrlAction   = SNOOPPrivCtrlAction::Stall;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivCoreEvent::Store:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_ad;
                 trans2Issue  = SNOOPPrivCohTrans::GetMTrans;
                 ctrlAction   = SNOOPPrivCtrlAction::issueTrans;
                 eventsPriority = PMSI::CoreReqBusEventsWorkConserv;
                 break;
               case SNOOP_MSIPrivCacheState::S:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_ad;
                 trans2Issue  = SNOOPPrivCohTrans::GetMTrans;
                 ctrlAction   = SNOOPPrivCtrlAction::issueTrans;
                 eventsPriority = PMSI::RequestBusEvent;
                 break;
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::MI_a:
                 ctrlAction  = SNOOPPrivCtrlAction::Hit;
                 eventsPriority = PMSI::CoreReqBusEventsWorkConserv;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 ctrlAction  = SNOOPPrivCtrlAction::Stall;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivCoreEvent::Replacement:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               case SNOOP_MSIPrivCacheState::S: // self invalidate
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 break;
               case SNOOP_MSIPrivCacheState::M:
                 cacheState_n   = SNOOP_MSIPrivCacheState::MI_a;
                 trans2Issue    = SNOOPPrivCohTrans::PutMTrans;
                 ctrlAction     = SNOOPPrivCtrlAction::issueTrans;
                 eventsPriority = PMSI::CoreReqBusEventsWorkConserv;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::MI_a:
               case SNOOP_MSIPrivCacheState::II_a:  
                 ctrlAction   = SNOOPPrivCtrlAction::Stall;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           default: // CoreNullEvent
             cacheState_n = cacheState_c;
             ctrlAction   = SNOOPPrivCtrlAction::NullAck;
         }
       }
       // process BusEvent
       else if (eventType == SNOOPPrivEventType::ReqBus) {
         switch (eventList.busReqEvent) {
           case SNOOPPrivReqBusEvent::OwnGetS:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::IS_ad: 
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_d;
                 break;  
               case SNOOP_MSIPrivCacheState::IS_a: 
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;  
               default:     
                 ctrlAction  = SNOOPPrivCtrlAction::Fault;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OwnGetM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::IM_ad: 
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d;
                 break;  
               case SNOOP_MSIPrivCacheState::IM_a: 
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break; 
               case SNOOP_MSIPrivCacheState::SM_ad: 
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d;
                 break;  
                case SNOOP_MSIPrivCacheState::SM_a: 
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;  
               default:     
                 ctrlAction  = SNOOPPrivCtrlAction::Fault;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OwnPutM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::M:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               case SNOOP_MSIPrivCacheState::MI_a:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::II_a:  
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OtherGetS:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 break;
               case SNOOP_MSIPrivCacheState::IM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d_S;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d_S;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::M:
                 cacheState_n   = SNOOP_MSIPrivCacheState::S;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreMem : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::MI_a:
                 cacheState_n   = SNOOP_MSIPrivCacheState::II_a;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreMem : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OtherGetM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 break;
               case SNOOP_MSIPrivCacheState::IS_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_d_I;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d_I;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d_SI;
                 break;
               case SNOOP_MSIPrivCacheState::S: 
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 break;
               case SNOOP_MSIPrivCacheState::SM_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_ad;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d_I;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::SM_a:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_a;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d_SI;
                 break;
               case SNOOP_MSIPrivCacheState::M:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreOnly : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::MI_a:
                 cacheState_n   = SNOOP_MSIPrivCacheState::II_a;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreOnly : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OtherPutM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::II_a:  
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::MI_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
                 break;
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::SM_a:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           default: // BusNullEvent
             cacheState_n = cacheState_c;
             ctrlAction   = SNOOPPrivCtrlAction::NullAck;
         } // switch (reqbusEvent) {
       } // else if (eventType == SNOOPPrivEventType::ReqBus)
       else if(eventType == SNOOPPrivEventType::RespBus) {
         switch (eventList.busRespEvent) {
           case SNOOPPrivRespBusEvent::OwnDataResp:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::MI_a:
               case SNOOP_MSIPrivCacheState::II_a:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_a;
                 break;
               case SNOOP_MSIPrivCacheState::IS_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::IS_d_I:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::IM_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_a;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_I:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_SI:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::SM_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_a;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_I:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_SI:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivRespBusEvent::OtherDataResp:{
             break;
           }
           default: // BusOtherDataRespEvent
             cacheState_n = cacheState_c;
             ctrlAction   = SNOOPPrivCtrlAction::NullAck;
         }
       }// else if (eventType == RespBusEvent)
       cacheState = static_cast<int>(cacheState_n);
    }


    void MSI::SharedCacheEventProcessing (SNOOPSharedEventType         eventType, 
                                          bool                         Cache2Cache,
                                          SNOOPSharedReqBusEvent       reqbusEvent, 
                                          SNOOPSharedRespBusEvent      respbusEvent, 
                                          int                         &cacheState, 
                                          PMSI::PMSIOwnerState              &ownerState,
                                          PMSI::PMSISharedAction            &ctrlAction) {

       // current and next state variable
       SNOOP_MSISharedCacheState cacheState_c, 
                                 cacheState_n; 
       PMSI::PMSIOwnerState      ownerState_n; 

       cacheState_c = static_cast<SNOOP_MSISharedCacheState>(cacheState);

       // default assignments
       ctrlAction   = PMSI::SharedNoAck;
       cacheState_n = cacheState_c;
       ownerState_n = PMSI::SkipSet;

       // process ReqBusEvent
       if (eventType == SNOOPSharedEventType::ReqBus) {
         // process ReqBusEvent
         switch (reqbusEvent) {
           case SNOOPSharedReqBusEvent::GetS:{
             switch (cacheState_c) {
               case SNOOP_MSISharedCacheState::IorS: 
                 ctrlAction   = PMSI::SendData;
                 ownerState_n = PMSI::SharedMem;
                 break;
               case SNOOP_MSISharedCacheState::M:
                 ctrlAction   = Cache2Cache ? PMSI::SharedNoAck : PMSI::SaveReqCoreId;
                 ownerState_n = PMSI::SharedMem;
                 cacheState_n = SNOOP_MSISharedCacheState::M_d_IorS;
                 break;
               case SNOOP_MSISharedCacheState::M_d_IorS:
               case SNOOP_MSISharedCacheState::M_d_M:
                 ctrlAction   = PMSI::SharedStall;
                 break;
               case SNOOP_MSISharedCacheState::IorSorM_a:
                 cacheState_n = SNOOP_MSISharedCacheState::IorS;
                 ctrlAction   = Cache2Cache ? PMSI::SharedNoAck : PMSI::SendData;
                 ownerState_n = PMSI::SharedMem;
                 break;
               default:
                 std::cout << "MSI-Shared::Error, uncovered condition detected" << std::endl;

             }
             break;
           }
           case SNOOPSharedReqBusEvent::GetM:{
             switch (cacheState_c) {
               case SNOOP_MSISharedCacheState::IorS: 
                 ctrlAction   = PMSI::SendData;
                 ownerState_n = PMSI::OtherCore;
                 cacheState_n = SNOOP_MSISharedCacheState::M;
                 break;
               case SNOOP_MSISharedCacheState::M:
                 ctrlAction   = Cache2Cache ? PMSI::SharedNoAck : PMSI::SaveReqCoreId;
                 ownerState_n = PMSI::OtherCore;
                 cacheState_n = Cache2Cache ? SNOOP_MSISharedCacheState::M :
                                              SNOOP_MSISharedCacheState::M_d_M;
                 break;
               case SNOOP_MSISharedCacheState::M_d_IorS:
               case SNOOP_MSISharedCacheState::M_d_M:
                 ctrlAction   = PMSI::SharedStall;
                 break;
               case SNOOP_MSISharedCacheState::IorSorM_a:
                 cacheState_n = SNOOP_MSISharedCacheState::M;
                 ctrlAction   = Cache2Cache ? PMSI::SharedNoAck : PMSI::SendData;
                 ownerState_n = PMSI::OtherCore;
                 break;
               default:
                 std::cout << "MSI-Shared::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPSharedReqBusEvent::OwnerPutM:{
             switch (cacheState_c) {
               case SNOOP_MSISharedCacheState::IorS: 
                 ctrlAction  = PMSI::SharedFault;
                 break;
               case SNOOP_MSISharedCacheState::M:
                 ownerState_n = PMSI::SharedMem;
                 ctrlAction   = PMSI::SharedNoAck;
                 cacheState_n = SNOOP_MSISharedCacheState::M_d_IorS;
                 break;
               case SNOOP_MSISharedCacheState::M_d_IorS:
               case SNOOP_MSISharedCacheState::M_d_M:
                 ctrlAction   = PMSI::SharedStall;
                 break;
               case SNOOP_MSISharedCacheState::IorSorM_a:
                 cacheState_n = SNOOP_MSISharedCacheState::IorS;
                 ctrlAction   = PMSI::SharedNoAck;
                 ownerState_n = PMSI::SharedMem;
                 break;
               default:
                 std::cout << "MSI-Shared::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPSharedReqBusEvent::OTherPutM: {
             switch (cacheState_c) { 
               case SNOOP_MSISharedCacheState::IorS: 
               case SNOOP_MSISharedCacheState::M: 
               case SNOOP_MSISharedCacheState::M_d_IorS: 
               case SNOOP_MSISharedCacheState::M_d_M: 
               case SNOOP_MSISharedCacheState::IorSorM_a:
                 ctrlAction  = PMSI::SharedNoAck;
                 break;
               default:
                 std::cout << "MSI-Shared::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           default: // BusNullEvent
             cacheState_n = cacheState_c;
             ctrlAction   = PMSI::SharedNullAck;
         }
       }
       else if (eventType == SNOOPSharedEventType::RespBus) {
         switch (respbusEvent) {
           case SNOOPSharedRespBusEvent::OWnDataResp:{
             ctrlAction   = PMSI::SharedNoAck;
             break;
           }
           case SNOOPSharedRespBusEvent::OTherDataResp:{
             switch (cacheState_c) {
               case SNOOP_MSISharedCacheState::IorS: 
               case SNOOP_MSISharedCacheState::M:
                 //ctrlAction  = PMSI::SharedFault;
                 ctrlAction   = PMSI::StoreDataOnly;
                 cacheState_n = SNOOP_MSISharedCacheState::IorSorM_a;
                 break;
               case SNOOP_MSISharedCacheState::M_d_IorS:
                 ctrlAction   = Cache2Cache ? PMSI::StoreDataOnly : PMSI::StoreData;
                 cacheState_n = SNOOP_MSISharedCacheState::IorS;
                 break;
               case SNOOP_MSISharedCacheState::M_d_M:
                 ctrlAction   = Cache2Cache ? PMSI::StoreDataOnly : PMSI::StoreData;
                 cacheState_n = SNOOP_MSISharedCacheState::M;
                 break;
               default:
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           default: // RespNullEvent
             cacheState_n = cacheState_c;
             ctrlAction   = PMSI::SharedNullAck;
         }
       } // else if (eventType == SNOOPSharedEventType::RespBus)
       cacheState = static_cast<int>(cacheState_n);
       ownerState = ownerState_n;
    } // PMSI::SharedCacheEventProcessing


    // MSI Private Cache FSM
    void  MSI::SNOOPPrivEventProcessing 
             (SNOOPPrivEventType  eventType, 
              SNOOPPrivEventList  eventList,
              int                 &cacheState,     
              SNOOPPrivCohTrans   &trans2Issue,
              SNOOPPrivCtrlAction &ctrlAction,
              bool                Cache2Cache) 
    {
       // current and next state variable
       SNOOP_MSIPrivCacheState  cacheState_c, 
                                cacheState_n; 

       // type case MSI State variable
       cacheState_c = static_cast<SNOOP_MSIPrivCacheState>(cacheState);

       // default assignments
       trans2Issue    = NullTrans;
       ctrlAction     = SNOOPPrivCtrlAction::NoAck;
       cacheState_n   = cacheState_c;

       // process CoreEvent
       if (eventType == SNOOPPrivEventType::Core) {
         switch (eventList.cpuReqEvent) {
           case SNOOPPrivCoreEvent::Load:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I: 
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_ad;
                 trans2Issue  = SNOOPPrivCohTrans::GetSTrans;
                 ctrlAction   = SNOOPPrivCtrlAction::issueTrans;
                 break;
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_ad:
                 ctrlAction     = SNOOPPrivCtrlAction::Hit;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::MI_a:
                 ctrlAction     = SNOOPPrivCtrlAction::Hit;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 ctrlAction  = SNOOPPrivCtrlAction::Stall;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivCoreEvent::Store:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_ad;
                 trans2Issue  = SNOOPPrivCohTrans::GetMTrans;
                 ctrlAction   = SNOOPPrivCtrlAction::issueTrans;
                 break;
               case SNOOP_MSIPrivCacheState::S:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_ad;
                 trans2Issue  = SNOOPPrivCohTrans::GetMTrans;
                 ctrlAction   = SNOOPPrivCtrlAction::issueTrans;
                 break;
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::MI_a:
                 ctrlAction  = SNOOPPrivCtrlAction::Hit;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 ctrlAction  = SNOOPPrivCtrlAction::Stall;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivCoreEvent::Replacement:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               case SNOOP_MSIPrivCacheState::S: // self invalidate
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 break;
               case SNOOP_MSIPrivCacheState::M:
                 cacheState_n   = SNOOP_MSIPrivCacheState::MI_a;
                 trans2Issue    = SNOOPPrivCohTrans::PutMTrans;
                 ctrlAction     = SNOOPPrivCtrlAction::issueTrans;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::MI_a:
               case SNOOP_MSIPrivCacheState::II_a:  
                 ctrlAction   = SNOOPPrivCtrlAction::Stall;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           default: // CoreNullEvent
             cacheState_n = cacheState_c;
             ctrlAction   = SNOOPPrivCtrlAction::NullAck;
         }
       }
       // process BusEvent
       else if (eventType == SNOOPPrivEventType::ReqBus) {
         switch (eventList.busReqEvent) {
           case SNOOPPrivReqBusEvent::OwnGetS:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::IS_ad: 
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_d;
                 break;  
               case SNOOP_MSIPrivCacheState::IS_a: 
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;  
               default:     
                 ctrlAction  = SNOOPPrivCtrlAction::Fault;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OwnGetM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::IM_ad: 
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d;
                 break;  
               case SNOOP_MSIPrivCacheState::IM_a: 
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break; 
               case SNOOP_MSIPrivCacheState::SM_ad: 
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d;
                 break;  
                case SNOOP_MSIPrivCacheState::SM_a: 
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;  
               default:     
                 ctrlAction  = SNOOPPrivCtrlAction::Fault;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OwnPutM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::M:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               case SNOOP_MSIPrivCacheState::MI_a:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::II_a:  
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OtherGetS:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 break;
               case SNOOP_MSIPrivCacheState::IM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d_S;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d_S;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::M:
                 cacheState_n   = SNOOP_MSIPrivCacheState::S;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreMem : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::MI_a:
                 cacheState_n   = SNOOP_MSIPrivCacheState::II_a;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreMem : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OtherGetM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
               case SNOOP_MSIPrivCacheState::II_a:  
                 break;
               case SNOOP_MSIPrivCacheState::IS_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_d_I;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d_I;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_d_SI;
                 break;
               case SNOOP_MSIPrivCacheState::S: 
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 break;
               case SNOOP_MSIPrivCacheState::SM_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_ad;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d_I;
                 ctrlAction   = SNOOPPrivCtrlAction::SaveWbCoreId;
                 break;
               case SNOOP_MSIPrivCacheState::SM_a:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_a;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_d_SI;
                 break;
               case SNOOP_MSIPrivCacheState::M:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreOnly : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::MI_a:
                 cacheState_n   = SNOOP_MSIPrivCacheState::II_a;
                 ctrlAction     = Cache2Cache ? SNOOPPrivCtrlAction::SendCoreOnly : SNOOPPrivCtrlAction::SendMemOnly;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivReqBusEvent::OtherPutM:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_ad:
               case SNOOP_MSIPrivCacheState::IM_ad:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::II_a:  
               case SNOOP_MSIPrivCacheState::IM_d:
               case SNOOP_MSIPrivCacheState::IS_d:
               case SNOOP_MSIPrivCacheState::IS_d_I:
               case SNOOP_MSIPrivCacheState::SM_ad:
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_d:
               case SNOOP_MSIPrivCacheState::MI_a:
               case SNOOP_MSIPrivCacheState::IM_d_I:
               case SNOOP_MSIPrivCacheState::IM_d_S:
               case SNOOP_MSIPrivCacheState::IM_d_SI:
               case SNOOP_MSIPrivCacheState::SM_d_I:
               case SNOOP_MSIPrivCacheState::SM_d_S:
               case SNOOP_MSIPrivCacheState::SM_d_SI:
                 break;
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::SM_a:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           default: // BusNullEvent
             cacheState_n = cacheState_c;
             ctrlAction   = SNOOPPrivCtrlAction::NullAck;
         } // switch (reqbusEvent) {
       } // else if (eventType == SNOOPPrivEventType::ReqBus)
       else if(eventType == SNOOPPrivEventType::RespBus) {
         switch (eventList.busRespEvent) {
           case SNOOPPrivRespBusEvent::OwnDataResp:{
             switch (cacheState_c) {
               case SNOOP_MSIPrivCacheState::I:
               case SNOOP_MSIPrivCacheState::IS_a:
               case SNOOP_MSIPrivCacheState::IM_a:
               case SNOOP_MSIPrivCacheState::S:
               case SNOOP_MSIPrivCacheState::SM_a:
               case SNOOP_MSIPrivCacheState::M:
               case SNOOP_MSIPrivCacheState::MI_a:
               case SNOOP_MSIPrivCacheState::II_a:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 break;
               case SNOOP_MSIPrivCacheState::IS_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::IS_a;
                 break;
               case SNOOP_MSIPrivCacheState::IS_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::IS_d_I:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::IM_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::IM_a;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_I:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::IM_d_SI:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::SM_ad:
                 cacheState_n = SNOOP_MSIPrivCacheState::SM_a;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d:
                 cacheState_n = SNOOP_MSIPrivCacheState::M;
                 ctrlAction   = SNOOPPrivCtrlAction::CopyThenHit;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_I:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_S:
                 cacheState_n = SNOOP_MSIPrivCacheState::S;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               case SNOOP_MSIPrivCacheState::SM_d_SI:
                 cacheState_n = SNOOP_MSIPrivCacheState::I;
                 ctrlAction   = Cache2Cache ? SNOOPPrivCtrlAction::CopyThenHitSendCoreMem : SNOOPPrivCtrlAction::CopyThenHitSendMemOnly;
                 break;
               default:
                 ctrlAction   = SNOOPPrivCtrlAction::Fault;
                 std::cout << "MSI::Error, uncovered condition detected" << std::endl;
             }
             break;
           }
           case SNOOPPrivRespBusEvent::OtherDataResp:{
             break;
           }
           default: // BusOtherDataRespEvent
             cacheState_n = cacheState_c;
             ctrlAction   = SNOOPPrivCtrlAction::NullAck;
         }
       }// else if (eventType == RespBusEvent)
       cacheState = static_cast<int>(cacheState_n);
    }

    SNOOPPrivEventPriority MSI::PrivCacheEventPriority (SNOOPPrivEventList eventList,SNOOPPrivEventsCacheInfo eventCacheInfoList ) {
       SNOOPPrivEventPriority eventPriority = SNOOPPrivEventPriority::WorkConserv;
       SNOOP_MSIPrivCacheState cacheState_c;

       if (eventList.busRespEvent == SNOOPPrivRespBusEvent::OwnDataResp) {
         eventPriority = SNOOPPrivEventPriority::RespBus;
       }
       else if (eventList.cpuReqEvent != SNOOPPrivCoreEvent::Null && 
                eventList.busReqEvent != SNOOPPrivReqBusEvent::Null ) {
         cacheState_c = static_cast<SNOOP_MSIPrivCacheState>(eventCacheInfoList.cpuReqCacheLineInfo.state);
         if (eventCacheInfoList.cpuReqCacheLineInfo.cl_idx == eventCacheInfoList.busReqCacheLineInfo.cl_idx) {
           if (eventCacheInfoList.cpuReqCacheLineInfo.IsValid == true && cacheState_c == SNOOP_MSIPrivCacheState::S) {
             eventPriority = SNOOPPrivEventPriority::ReqBus;
           }
         }
       }
       return eventPriority;
    }


}


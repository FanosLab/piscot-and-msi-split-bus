/*
 * File  :      MSI.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 28, 2020
 */

#ifndef _MSI_H
#define _MSI_H


#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

#include "PMSI.h"

#include "SNOOPProtocolCommon.h"
#include "IFCohProtocol.h"

namespace ns3 { 

  // MSI Protocal states enconding (Private Cache Side)
  enum class SNOOP_MSIPrivCacheState {
    I = CohProtType::SNOOP_MSI,   
    IS_ad,
    IS_d,
    IS_a,
    IS_d_I,
    IM_ad,
    IM_d,
    IM_a,
    IM_d_I,
    IM_d_S,
    IM_d_SI,
    S,
    SM_ad,
    SM_d,
    SM_a,
    SM_d_I,
    SM_d_S,
    SM_d_SI,
    M,
    MI_a,
    II_a
  };

  // Protocal states enconding  (Memory Controller Side)
  enum class SNOOP_MSISharedCacheState {
    IorS = CohProtType::SNOOP_MSI, 
    M,	    
    M_d_M,	    
    M_d_IorS,
    IorSorM_a
  };

  class MSI : public ns3::Object, public ns3::IFCohProtocol {
  public:

      MSI ();

     ~MSI ();

     // Override TypeId.
     static TypeId GetTypeId(void);

     // Protocol FSM at PrivCache Control Side
     void SNOOPPrivEventProcessing (SNOOPPrivEventType         eventType, 
                                    SNOOPPrivEventList         eventList,
                                    int                        &cacheState,     
                                    SNOOPPrivCohTrans          &trans2Issue,
                                    SNOOPPrivCtrlAction        &ctrlAction,
                                    bool                       cache2Cache);

     SNOOPPrivEventPriority PrivCacheEventPriority (SNOOPPrivEventList       eventList,
                                                    SNOOPPrivEventsCacheInfo eventCacheInfoList );


     // Protocol FSM at SharedCache Control Side
     void SNOOPSharedEventProcessing (SNOOPSharedEventType                    eventType, 
                                      SNOOPSharedEventList                    eventList, 
                                      int                                     &cacheState, 
                                      SNOOPSharedOwnerState                   &ownerState,
                                      Ptr<GenericFIFO<SNOOPSharedCtrlAction>> &actionQueue,
                                      bool                                    cache2Cache);


     static void CacheEventProcessing (SNOOPPrivEventType         eventType, 
                                       SNOOPPrivEventList         eventList,
                                       int                        &cacheState,     
                                       SNOOPPrivCohTrans          &trans2Issue,
                                       PMSI::PMSIEventPriority    &eventsPriority,
                                       SNOOPPrivCtrlAction        &ctrlAction,
                                       bool                       cache2Cache);


     static void SharedCacheEventProcessing (SNOOPSharedEventType      eventType, 
                                          bool                         Cache2Cache,
                                          SNOOPSharedReqBusEvent       reqbusEvent, 
                                          SNOOPSharedRespBusEvent      respbusEvent, 
                                          int                         &cacheState, 
                                          PMSI::PMSIOwnerState              &ownerState,
                                          PMSI::PMSISharedAction            &ctrlAction);

    static bool IsValidBlk (int s) {
       SNOOP_MSIPrivCacheState state = static_cast<SNOOP_MSIPrivCacheState>(s);
       switch (state) {
         case SNOOP_MSIPrivCacheState::S:
         case SNOOP_MSIPrivCacheState::SM_ad:
         case SNOOP_MSIPrivCacheState::SM_d:
         case SNOOP_MSIPrivCacheState::SM_a:
         case SNOOP_MSIPrivCacheState::SM_d_I:
         case SNOOP_MSIPrivCacheState::SM_d_S:
         case SNOOP_MSIPrivCacheState::SM_d_SI:
         case SNOOP_MSIPrivCacheState::M:
         case SNOOP_MSIPrivCacheState::MI_a:
           return true;
         default:
           return false;
       }
    }

     std::string PrivStateName (int s)
     {

       SNOOP_MSIPrivCacheState state = static_cast<SNOOP_MSIPrivCacheState>(s);

       switch (state) {
         case SNOOP_MSIPrivCacheState::I:
           return " I "; 
         case SNOOP_MSIPrivCacheState::IS_ad:
           return " I_S_ad (waiting for GET(S) command to be issued on bus) ";
         case SNOOP_MSIPrivCacheState::IS_a:
           return " I_S_a (waiting for GET(S) command to be issued on bus, data has been received) ";
         case SNOOP_MSIPrivCacheState::IS_d:
           return " I_S_d (waiting data resp) ";
         case SNOOP_MSIPrivCacheState::IS_d_I:
           return " IS_d_I (waiting for data, then do Hit, then move to I)";
         case SNOOP_MSIPrivCacheState::IM_ad:
           return " IM_ad (waiting for GET(M) command to be issued on bus) ";
         case SNOOP_MSIPrivCacheState::IM_a:
           return " IM_a (waiting for GET(M) command to be issued on bus, data has been received) ";
         case SNOOP_MSIPrivCacheState::IM_d:
           return " IM_d (waiting data resp) ";
         case SNOOP_MSIPrivCacheState::IM_d_I:
           return " IM_d_I (waiting data resp), Then Hit, then move to I ";
         case SNOOP_MSIPrivCacheState::IM_d_S:
           return " IM_d_S (waiting data resp), Then Hit, then move to S";
         case SNOOP_MSIPrivCacheState::IM_d_SI:
           return " IM_d_SI (waiting data resp), Then Hit, then move to I";
         case SNOOP_MSIPrivCacheState::S:
           return " S ";
         case SNOOP_MSIPrivCacheState::SM_ad:
           return " SM_ad (Waiting for GET(M) upgrade to be issued on the bus) " ;
         case SNOOP_MSIPrivCacheState::SM_d:
           return " SM_d (Waiting for Data Response) " ;
         case SNOOP_MSIPrivCacheState::SM_a:
           return " SM_a (Waiting for GET(M), data has been received) " ;
         case SNOOP_MSIPrivCacheState::SM_d_I:
           return " SM_d_I (Waiting for Data Response, Then do Hit, then move to I) " ;
         case SNOOP_MSIPrivCacheState::SM_d_S:
           return " SM_d_S (Waiting for Data Response, Then do Hit, then move to S) " ;
         case SNOOP_MSIPrivCacheState::SM_d_SI:
           return " SM_d_SI (Waiting for Data Response, Then do Hit, then move to I) " ;
         case SNOOP_MSIPrivCacheState::M:
           return " M ";
         case SNOOP_MSIPrivCacheState::MI_a:
           return " MI_a (Waiting for Put(M) then move to I)";
         case SNOOP_MSIPrivCacheState::II_a:
           return " II_a (Waiting for Put(M) then move to I)";
         default: 
           return " MSI: Error Undefine State";
       }
     }

     static std::string SharedStateName (int s)
     {
       SNOOP_MSISharedCacheState state = static_cast<SNOOP_MSISharedCacheState>(s);
       switch (state) {
         case SNOOP_MSISharedCacheState::IorS:
           return " IorS (Shared Mem is the Owner) "; 
         case SNOOP_MSISharedCacheState::M:
           return " M (Shared Mem content is dirty) ";
         case SNOOP_MSISharedCacheState::M_d_IorS:
           return " M_d_IorS (Waiting for Data then Move to IorS) ";
         case SNOOP_MSISharedCacheState::M_d_M:
           return " M_d_M (Waiting for Data then Move to M) ";
         case SNOOP_MSISharedCacheState::IorSorM_a:
           return " IorSorM_a (Waiting for Data then Move to M or IorS) ";
         default:
           return " MSI: Error Undefine State";
       }
     }

  }; // class MSI


}

#endif /* _MSI_H */

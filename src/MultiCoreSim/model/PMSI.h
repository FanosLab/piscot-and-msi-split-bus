/*
 * File  :      PMSI.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 18, 2020
 */

#ifndef _PMSI_H
#define _PMSI_H


#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

#include "SNOOPProtocolCommon.h"
#include "IFCohProtocol.h"

namespace ns3 { 


  // Protocal states enconding
  enum class SNOOP_PMSIPrivCacheState {
   I = CohProtType::SNOOP_PMSI,   
   S,       
   M,       
   IS_a_d,  
   IM_a_d,  
   IS_a,    
   IM_a,    
   IS_d,    
   IM_d,   
   SM_w,   
   MI_wb,   
   MS_wb,   
   IM_d_I,  
   IS_d_I,  
   IM_d_S   
  };

  // Protocal states enconding
  enum class SNOOP_PMSISharedCacheState {
    IorS = CohProtType::SNOOP_PMSI,   
    M,	       
    M_d_IorS,   
    M_d_M ,     
    IorS_A,
    IorS_d
  };

  class PMSI : public ns3::Object, public ns3::IFCohProtocol {
  public:

     // this is the list of local actions 
     // need to be taken by controller
     enum PMSICtrlAction {
       Stall = 0,          // In-order cores has to wait for its data responses before issuing a new command.
       Hit,                // When read/write requests hit can be done given that the tag are matched at the controller level.
       issueTrans,         // When coherence transaction is needed to be sent on the bus.
       ReissueTrans,       // When controller need to cancel the previous transaction for this cache line and replace the TxReq FIFO with a new one.
       issueTransSaveWbId, // When coherence transaction is needed to be sent on the bus, and Other coreId need to be saved in case of cache-2-cahce feature is enabled.
       WritBack,           // When controller needs to do write back of the cache line on the response bus.
       CopyThenHit,        // When controller need to copy response data into its own cache then, then do hit load/store for the request core command. 
       CopyThenHitWB,      // Same as "CopyThenHit", but the core needs also to write back the response data into its local TxResponse buffer.
       CopyThenHitSendCoreOnly,
       CopyThenHitSendMemOnly,
       CopyThenHitSendCoreMem,
       SaveWbCoreId,
       SendMemOnly,
       SendCoreOnly,
       SendCoreMem,
       Fault,              // When controller enter unexpected state, then program should stop and debug the issue.
       NoAck,              // When controller doesn't need to take any action except that removing the message from the received FIFO and update cache coherence state.
       NullAck,            // When controller input events for core, RxResp, or RxReq bus are Null, then the output action is Null too. 
       ProcessedAck        // When controller processed the action, then it marked as prossesed once its done. 
     };
     
     enum PMSIEventPriority {
       ParallelEvents = 0,
       CoreReqBusEventsWorkConserv,
       RequestBusEvent
     };

     enum PMSIOwnerState{
       SharedMem = 0,
       OtherCore,
       SkipSet
     };

     enum PMSISharedAction {
       SharedNoAck = 0,
       SharedStall,
       SendData,
       StoreData,
       StoreDataOnly,
       SaveReqCoreId,
       SendPendingData,
       SharedFault,
       SharedNullAck,
       SharedProcessedAck
     };

      PMSI ();

     ~PMSI ();

      // Override TypeId.
      static TypeId GetTypeId(void);


     // Process Received Events, update Cache state if needed,
     // and issue needed transaction if any.
     static void CacheEventProcessing (SNOOPPrivEventType         eventType, 
                                       SNOOPPrivEventList         eventList,
                                       int  & cacheState,       // bidirectional variable to implement current, and next state 
                                       SNOOPPrivCohTrans &        trans2Issue,
                                       PMSIEventPriority &        eventsPriority, 
                                       SNOOPPrivCtrlAction &      ctrlAction,
                                       bool                       cache2Cache = false);   

     void SNOOPPrivEventProcessing 
          (SNOOPPrivEventType  eventType, 
           SNOOPPrivEventList  eventList,
           int                 &cacheState,       
           SNOOPPrivCohTrans   &trans2Issue,
           SNOOPPrivCtrlAction &ctrlAction,
           bool                cache2Cache
           );

    static void SharedCacheEventProcessing (SNOOPSharedEventType         eventType, 
                                            bool                         Cache2Cache,
                                            SNOOPSharedReqBusEvent       reqbusEvent, 
                                            SNOOPSharedRespBusEvent      respbusEvent, 
                                            int                       & cacheState, 
                                            PMSIOwnerState   &      ownerState,
                                            PMSISharedAction &      ctrlAction);

    static void FillCpuActionQueue (Ptr<GenericFIFO<SNOOPPrivCtrlAction>> actionQueue, SNOOPPrivCtrlAction actionType);
    static void FillReqBusActionQueue (Ptr<GenericFIFO<SNOOPPrivCtrlAction>> actionQueue, SNOOPPrivCtrlAction actionType);
    static void FillRespBusActionQueue (Ptr<GenericFIFO<SNOOPPrivCtrlAction>> actionQueue, SNOOPPrivCtrlAction actionType);

    static bool IsValidBlk (int s) {
       SNOOP_PMSIPrivCacheState state = static_cast<SNOOP_PMSIPrivCacheState>(s);
       switch (state) {
         case SNOOP_PMSIPrivCacheState::S:
         case SNOOP_PMSIPrivCacheState::M:
         case SNOOP_PMSIPrivCacheState::MI_wb:
         case SNOOP_PMSIPrivCacheState::MS_wb:
           return true;
         default:
           return false;
       }
    }

    SNOOPPrivEventPriority PrivCacheEventPriority (SNOOPPrivEventList eventList,SNOOPPrivEventsCacheInfo eventCacheInfoList );


     static std::string PrivActionName (PMSICtrlAction action)
     {
       switch (action) {
         case Stall:
           return " Stall"; 
         case Hit:
           return " Hit";
         case issueTrans:
           return " issueTrans";
         case ReissueTrans:
           return " ReissueTrans";
         case issueTransSaveWbId:
           return " issueTransSaveWbId";
         case WritBack:
           return " WritBack";
         case CopyThenHit:
           return " CopyThenHit" ;
         case CopyThenHitWB:
           return " CopyThenHitWB" ;
         case CopyThenHitSendCoreOnly:
           return " CopyThenHitSendCoreOnly" ;
         case CopyThenHitSendMemOnly:
           return " CopyThenHitSendMemOnly" ;
         case CopyThenHitSendCoreMem:
           return " CopyThenHitSendCoreMem" ;
         case SaveWbCoreId:
           return " SaveWbCoreId" ;
         case SendMemOnly:
           return " SendMemOnly" ;
         case SendCoreOnly:
           return " SendCoreOnly" ;
         case SendCoreMem:
           return " SendCoreMem" ;
         case Fault:
           return " Fault" ;
         case NoAck:
           return " NoAck";
         case NullAck:
           return " NullAck";
         default: 
           return " ProcessedAck";
       }
     }


     static std::string SharedActionName (PMSISharedAction action)
     {
       switch (action) {
         case SharedNoAck:
           return " No-Action"; 
         case SharedStall:
           return " Stall (Waiting For Data Resp)";
         case SendData:
           return " SendData to core";
         case StoreData:
           return " StoreData into Memory and Check Pending WB Buffer";
         case StoreDataOnly:
           return " StoreData Only into Memory";
         case SaveReqCoreId:
           return " SaveReqCoreId Msg in Pending WB buffer";
         case  SendPendingData:
           return " SendPending Data to core";
         case SharedFault:
           return " Fault" ;
         case SharedNullAck:
           return " No Pending Request";
         case SharedProcessedAck:
           return " Actioned is Processsed";
         default:
           return " Unknown !!!!";
       }
     }

     std::string PrivStateName (int s)
     {

       SNOOP_PMSIPrivCacheState state = static_cast<SNOOP_PMSIPrivCacheState>(s);

       switch (state) {
         case SNOOP_PMSIPrivCacheState::I:
           return " I "; 
         case SNOOP_PMSIPrivCacheState::S:
           return " S ";
         case SNOOP_PMSIPrivCacheState::M:
           return " M ";
         case SNOOP_PMSIPrivCacheState::IS_a_d:
           return " I_S_a_d (waiting for GET(S) command to be issued on bus) ";
         case SNOOP_PMSIPrivCacheState::IS_a:
           return " I_S_a (waiting for GET(S) command to be issued on bus, data has been received) ";
         case SNOOP_PMSIPrivCacheState::IS_d:
           return " I_S_d (waiting data resp) ";
         case SNOOP_PMSIPrivCacheState::IM_a_d:
           return " I_M_a_d (waiting for GET(M) command to be issued on bus) ";
         case SNOOP_PMSIPrivCacheState::IM_a:
           return " I_M_a (waiting for GET(M) command to be issued on bus, data has been received) ";
         case SNOOP_PMSIPrivCacheState::IM_d:
           return " I_M_d (waiting data resp) ";
         case SNOOP_PMSIPrivCacheState::SM_w:
           return " S_M_w (waiting core slot) " ;
         case SNOOP_PMSIPrivCacheState::MI_wb:
           return " M_wb_I (waiting to WB) ";
         case SNOOP_PMSIPrivCacheState::MS_wb:
           return " M_wb_S (waiting to WB, then move to S) ";
         case SNOOP_PMSIPrivCacheState::IM_d_I:
           return " IM_d_I (waiting for data, then do Hit, then move to I";
         case SNOOP_PMSIPrivCacheState::IS_d_I:
           return " IS_d_I (waiting for data, then do Hit, then move to I)";
         default: //SNOOP_PMSIPrivCacheState::IM_d_S:
           return " IM_d_S (waiting for data, then do Hit, then move to S)";
       }
     }

     std::string SharedStateName (int s)
     {
       SNOOP_PMSISharedCacheState state = static_cast<SNOOP_PMSISharedCacheState>(s);
       switch (state) {
         case SNOOP_PMSISharedCacheState::IorS:
           return " IorS (Shared Mem is the Owner) "; 
         case SNOOP_PMSISharedCacheState::M:
           return " M (Shared Mem content is dirty) ";
         case SNOOP_PMSISharedCacheState::M_d_IorS:
           return " M_d_IorS (Waiting for Data then Move to IorS) ";
         case SNOOP_PMSISharedCacheState::M_d_M:
           return " M_d_M (Waiting for Data then Move to M) ";
         case SNOOP_PMSISharedCacheState::IorS_A:
           return " IorS_A (Waiting for transaction then Move to IorS) ";
         case SNOOP_PMSISharedCacheState::IorS_d:
           return " IorS_d (Waiting for DataResp then Move to IorS) ";
         default:
           return "Unknown !!!!";
       }
     }

     static std::string PrivReqBusEventName (SNOOPPrivReqBusEvent event)
     {
       switch (event) {
         case SNOOPPrivReqBusEvent::OwnGetS:
           return " OwnGetSEvent "; 
         case SNOOPPrivReqBusEvent::OwnGetM:
           return " OwnGetMEvent ";
         case SNOOPPrivReqBusEvent::OwnPutM:
           return " OwnPutMEvent ";
         case SNOOPPrivReqBusEvent::OwnPutS:
           return " OwnPutSEvent ";
         case SNOOPPrivReqBusEvent::OwnUpg:
           return " OwnUpgEvent ";
         case SNOOPPrivReqBusEvent::OtherGetS:
           return " OtherGetSEvent " ;
         case SNOOPPrivReqBusEvent::OtherGetM:
           return " OtherGetMEvent ";
         case SNOOPPrivReqBusEvent::OtherPutS:
           return " OtherPutSEvent ";
         case SNOOPPrivReqBusEvent::OtherPutM:
           return " OtherPutMEvent ";
         case SNOOPPrivReqBusEvent::OtherUpg:
           return " OtherUpgEvent )";
         case SNOOPPrivReqBusEvent::Null:
           return " NullEvent ";
         default:
           return "Unknown !!!!";
       }
     }

     static std::string PrivRespBusEventName (SNOOPPrivRespBusEvent event)
     {
       switch (event) {
         case SNOOPPrivRespBusEvent::OwnDataResp:
           return " OwnDataRespEvent "; 
         case SNOOPPrivRespBusEvent::OtherDataResp:
           return " OtherDataRespEvent ";
         case SNOOPPrivRespBusEvent::Null:
           return " NullRespEvent ";
         default:
           return "Unknown !!!!";

       }
     }

     static std::string SharedRespBusEventName (SNOOPSharedRespBusEvent event)
     {
       switch (event) {
         case SNOOPSharedRespBusEvent::OWnDataResp:
           return " OwnDataRespEvent "; 
         case SNOOPSharedRespBusEvent::OTherDataResp:
           return " OtherDataRespEvent "; 
         case SNOOPSharedRespBusEvent::NUll:
           return " NullRespEvent ";
         default:
           return "Unknown !!!!";
       }
     }

     static std::string SharedReqBusEventName (SNOOPSharedReqBusEvent event)
     {
       switch (event) {
         case SNOOPSharedReqBusEvent::GetS:
           return " GetSEvent "; 
         case SNOOPSharedReqBusEvent::GetM:
           return " GetMEvent ";
         case SNOOPSharedReqBusEvent::Upg:
           return " UpgEvent ";
         case SNOOPSharedReqBusEvent::OwnerPutM:
           return " OwnerPutMEvent ";
         case SNOOPSharedReqBusEvent::OTherPutM:
           return " OTherPutMEvent " ;
         case SNOOPSharedReqBusEvent::PutS:
           return " PutSEvent ";
         case SNOOPSharedReqBusEvent::Null:
           return " NullReqEvent ";
         default:
           return "Unknown !!!!";
       }
     }

     static std::string TransName (SNOOPPrivCohTrans trans)
     {
       switch (trans) {
         case GetSTrans:
           return " GetSTrans "; 
         case GetMTrans:
           return " GetMTrans ";
         case UpgTrans:
           return " UpgTrans ";
         case PutMTrans:
           return " PutMTrans ";
         case PutSTrans:
           return " PutSTrans " ;
         default: // NullTrans:
           return " NullTrans ";

       }
     }

  };

}

#endif /* _PMSI_H */



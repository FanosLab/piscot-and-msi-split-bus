/*
 * File  :      MCoreSimProjectXml.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 17, 2020
 */

#include "PrivateCacheCtrl.h"

namespace ns3 {

    // override ns3 type
    TypeId PrivateCacheCtrl::GetTypeId(void) {
        static TypeId tid = TypeId("ns3::PrivateCacheCtrl")
               .SetParent<Object > ();
        return tid;
    }

    PrivateCacheCtrl::PrivateCacheCtrl(uint32_t cachLines, 
                     Ptr<BusIfFIFO> assoicateBusIfFIFO, 
                     Ptr<CpuFIFO  > associatedCpuFIFO) {
        // default
        m_cacheType    = 0;
        m_cacheSize    = 64*256;
        m_cacheBlkSize = 64;
        m_nways        = 1;
        m_nsets        = 256;
        m_coreId       = 1;
        m_sharedMemId  = 10;
        m_cacheCycle   = 1;
        m_dt           = (1.0/1000000);
        m_clkSkew      = 0;
        m_prllActionCnt= 0;
        m_reqWbRatio   = 0;
        m_cach2Cache   = false;
        m_logFileGenEnable = false;
        m_cache        = CreateObject<GenericCache> (cachLines);
        m_busIfFIFO    = assoicateBusIfFIFO;
        m_cpuFIFO      = associatedCpuFIFO;
        m_PendingWbFIFO.SetFifoDepth(assoicateBusIfFIFO->m_txMsgFIFO.GetFifoDepth());

        m_cohProtocol  = CreateObject<SNOOPPrivCohProtocol>();

        m_maxPendingReq    = 0;
        m_pendingCpuReq    = 0;
        m_cpuPendingFIFO   = CreateObject<GenericFIFO <PendingMsg >> ();

    }

    // We don't do any dynamic allocations
    PrivateCacheCtrl::~PrivateCacheCtrl() {
    }

    // Set Maximum number of Pending CPU Request (OOO)
    void PrivateCacheCtrl::SetMaxPendingReq (int maxPendingReq) {
      m_maxPendingReq = maxPendingReq;
    }

    // Set Pending CPU FIFI depth
    void PrivateCacheCtrl::SetPendingCpuFIFODepth (int size) {
      m_cpuPendingFIFO->SetFifoDepth(size);
    }

    void PrivateCacheCtrl::SetCacheSize (uint32_t cacheSize) {
      m_cacheSize = cacheSize;
      m_cache->SetCacheSize(cacheSize);
    }

    uint32_t PrivateCacheCtrl::GetCacheSize () {
      return m_cacheSize;
    }

    void PrivateCacheCtrl::SetCacheBlkSize (uint32_t cacheBlkSize) {
      m_cacheBlkSize = cacheBlkSize;
      m_cache->SetCacheBlkSize(cacheBlkSize);
    }

    uint32_t PrivateCacheCtrl::GetCacheBlkSize () {
      return m_cacheBlkSize;
    }

    void PrivateCacheCtrl::SetCacheNways (uint32_t nways) {
      m_nways = nways;
      m_cache->SetCacheNways(nways);
    }

    uint32_t PrivateCacheCtrl::GetCacheNways () {
      return m_nways;
    }

    void PrivateCacheCtrl::SetCacheNsets (uint32_t nsets) {
      m_nsets = nsets;
      m_cache->SetCacheNsets(nsets);
    }

    uint32_t PrivateCacheCtrl::GetCacheNsets () {
      return m_nsets;
    }

    void PrivateCacheCtrl::SetCacheType (uint16_t cacheType) {
      m_cacheType = cacheType;
      m_cache->SetCacheType(cacheType);
    }

    uint16_t PrivateCacheCtrl::GetCacheType () {
      return m_cacheType;
    }

    void PrivateCacheCtrl::SetCoreId (int coreId) {
      m_coreId = coreId;
    }

    void PrivateCacheCtrl::SetSharedMemId (int sharedMemId) {
      m_sharedMemId = sharedMemId;
    }

    int PrivateCacheCtrl::GetCoreId () {
      return m_coreId;
    }

    void PrivateCacheCtrl::SetDt (double dt) {
      m_dt = dt;
    }

    int PrivateCacheCtrl::GetDt () {
      return m_dt;
    }

    void PrivateCacheCtrl::SetClkSkew (double clkSkew) {
       m_clkSkew = clkSkew;
    }

    void PrivateCacheCtrl::SetReqWbRatio (int reqWbRatio) {
      m_reqWbRatio = reqWbRatio;
    }

    void PrivateCacheCtrl::SetCache2Cache (bool cach2Cache) {
       m_cach2Cache = cach2Cache;
    }

    void PrivateCacheCtrl::SetLogFileGenEnable (bool logFileGenEnable ) {
      m_logFileGenEnable = logFileGenEnable;
    }

     // remove message from TxMsg FIFO
     bool PrivateCacheCtrl::ReplcMsgInBusTxFIFO (uint16_t       transId,
                                                 uint64_t       addr,
                                                 uint16_t       newTransId)
     {
        bool ReqRemovedFlag = false;
       if (!m_busIfFIFO->m_txMsgFIFO.IsEmpty()) {

          BusIfFIFO::BusReqMsg busReqMsg ;
          GenericCacheMapFrmt  ReqAddrMap, ReplcAddrMap;
          ReplcAddrMap = m_cache->CpuAddrMap (addr);
          int QueueSize = m_busIfFIFO->m_txMsgFIFO.GetQueueSize();

          for (int i = 0; i < QueueSize ;i++) {
            busReqMsg  = m_busIfFIFO->m_txMsgFIFO.GetFrontElement();
            ReqAddrMap = m_cache->CpuAddrMap (busReqMsg.addr);
            // Remove message from the busReq buffer
            m_busIfFIFO->m_txMsgFIFO.PopElement();
            if (ReplcAddrMap.idx_set == ReqAddrMap.idx_set &&
                ReplcAddrMap.tag == ReqAddrMap.tag &&
                busReqMsg.cohrMsgId == transId) {
              // replace transaction Id with the new one
              busReqMsg.cohrMsgId    = newTransId;
              ReqRemovedFlag = true;
            }
            // Dequeue the data again into pending buffer
            m_busIfFIFO->m_txMsgFIFO.InsertElement(busReqMsg);
          }
        } 
        return ReqRemovedFlag;
     }

     // insert new Transaction into BusTxMsg FIFO 
     bool PrivateCacheCtrl::PushMsgInBusTxFIFO (uint64_t       msgId, 
                                                uint16_t       reqCoreId, 
                                                uint16_t       wbCoreId, 
                                                uint16_t       transId, 
                                                uint64_t       addr,
                                                bool PendingWbBuf = false) {

       BusIfFIFO::BusReqMsg tempBusReqMsg;
       tempBusReqMsg.msgId        = msgId;
       tempBusReqMsg.reqCoreId    = reqCoreId;
       tempBusReqMsg.wbCoreId     = wbCoreId;
       tempBusReqMsg.cohrMsgId    = transId;
       tempBusReqMsg.addr         = addr;
       tempBusReqMsg.timestamp    = m_cacheCycle*m_dt;
       tempBusReqMsg.cycle        = m_cacheCycle;

       if (!m_busIfFIFO->m_txMsgFIFO.IsFull() && PendingWbBuf == false) {
         // push message into BusTxMsg FIFO
         m_busIfFIFO->m_txMsgFIFO.InsertElement(tempBusReqMsg);
         return true;
       }
       else if (!m_PendingWbFIFO.IsFull() && PendingWbBuf == true) {
         // push message into BusTxMsg FIFO
         m_PendingWbFIFO.InsertElement(tempBusReqMsg);
         return true;
       }
       else {
         if (m_logFileGenEnable){
           std::cout << "Info: Cannot insert the Msg into BusTxMsg FIFO, FIFO is Full" << std::endl;
         }
         return false;
       }

     }


     // update private cache state or complete line
     void PrivateCacheCtrl::UpdatePrivateCache (CacheField field, 
                                                uint64_t addr, 
                                                int state, 
                                                uint8_t * data) {
       GenericCacheMapFrmt addrMap   = m_cache->CpuAddrMap (addr);
       uint32_t cache_idx            = addrMap.idx_set;
       if (field == State) {
         m_cache->SetCacheLineState (cache_idx, state);
       }
       else if (field == Line) {
         GenericCacheFrmt cacheLine;
         cacheLine.state  = state;
         cacheLine.tag    = addrMap.tag;
         for (int i = 0; i < 8; i++)
           cacheLine.data[i] = data[i];

         m_cache->WriteCacheLine(cacheLine, cache_idx);
       }
     }

     // execute write back command
     bool PrivateCacheCtrl::DoWriteBack (uint64_t addr, uint16_t wbCoreId, uint64_t msgId, bool dualTrans=false) {
       if (!m_busIfFIFO->m_txRespFIFO.IsFull()) {
         GenericCacheMapFrmt addrMap   = m_cache->CpuAddrMap (addr);
         GenericCacheFrmt wbLine = m_cache->ReadCacheLine (addrMap.idx_set);
         BusIfFIFO::BusRespMsg  wbMsg;
         wbMsg.reqCoreId    = wbCoreId;
         wbMsg.respCoreId   = m_coreId;
         wbMsg.addr         = addr;
         wbMsg.timestamp    = m_cacheCycle*m_dt;
         wbMsg.cycle        = m_cacheCycle;
         wbMsg.msgId        = msgId;
         wbMsg.dualTrans    = dualTrans;

         if (m_logFileGenEnable) {
           std::cout << "DoWriteBack:: coreId = " << m_coreId << " requested Core = " << wbCoreId << std::endl;
         }

         for (int i = 0; i < 8; i++)
           wbMsg.data[i] = wbLine.data[i];

         // push message into BusTxMsg FIFO
         m_busIfFIFO->m_txRespFIFO.InsertElement(wbMsg);
         return true;
       }
       else {
         if (m_logFileGenEnable) {
           std::cout << "Info: Cannot insert the Msg into BusTxResp FIFO, FIFO is Full" << std::endl;
           std::cout << "TxResp Buffer Size = "<< m_busIfFIFO->m_txRespFIFO.GetQueueSize() << std::endl;
         }
         return false;
       }
     }

     // process pending buffer
     bool PrivateCacheCtrl::SendPendingWB  (GenericCacheMapFrmt recvTrans, TransType type = MemOnly ) {
        bool ReqSentFlag = false;
       // check if there is a pending write-back to this line in the pending buffer
       if (!m_PendingWbFIFO.IsEmpty()) {
          BusIfFIFO::BusReqMsg pendingWbMsg ;
          GenericCacheMapFrmt  pendingWbAddrMap;
          int pendingQueueSize = m_PendingWbFIFO.GetQueueSize();
          //std::cout << "PrivCache: pendingWb Queue size = " << pendingQueueSize << std::endl;
          for (int i = 0; i < pendingQueueSize ;i++) {
            pendingWbMsg = m_PendingWbFIFO.GetFrontElement();
            pendingWbAddrMap = m_cache->CpuAddrMap (pendingWbMsg.addr);
            //std::cout << "PrivCache: pendingWb Entery = " << i << ", core Id = " << pendingWbMsg.reqCoreId << ", addr = " << pendingWbMsg.addr << std::endl;
            // Remove message from the busReq buffer
            m_PendingWbFIFO.PopElement();
            if (recvTrans.idx_set == pendingWbAddrMap.idx_set &&
                recvTrans.tag == pendingWbAddrMap.tag) {
              // b.3.2)send data to requestors
              //std::cout << "PrivCache: PendingWb Entery = " << i << ", Core Id = " << pendingWbMsg.reqCoreId <<  " get inserted into Tx ResqBuf" << std::endl;
              //uint16_t reqCoreId = (m_cach2Cache == true) ? pendingWbMsg.reqCoreId : m_sharedMemId;
              uint16_t reqCoreId = (type == CoreOnly || type == CorePlsMem) ? pendingWbMsg.reqCoreId : m_sharedMemId;
              bool dualTrans = (type == CorePlsMem);
              if (!DoWriteBack (pendingWbMsg.addr, reqCoreId, pendingWbMsg.msgId, dualTrans)) {
                if (m_logFileGenEnable) {
                  std::cout << "PrivCache " << m_coreId << " TxResp FIFO is Full!" << std::endl;
                }
                exit(0);
              }
              ReqSentFlag = true;
            }
            else {
              // b.3.1) Dequeue the data again into pending buffer
              m_PendingWbFIFO.InsertElement(pendingWbMsg);
            }
          }
        } 
        return ReqSentFlag;
     }


     // This function does most of the functionality.
     void PrivateCacheCtrl::CacheCtrlMain () {

       // Generate Cache Controller requested messages
       // and its corresponding Events if exists
       SNOOPPrivEventList eventList;

       SNOOPPrivCoreEvent    cpuReqEvent;
       SNOOPPrivReqBusEvent  busReqEvent;
       SNOOPPrivRespBusEvent busRespEvent;

       CpuFIFO  ::ReqMsg      cpuReqMsg = {};
       BusIfFIFO::BusReqMsg   busReqMsg = {};
       BusIfFIFO::BusRespMsg  busRespMsg= {};

       PendingMsg cpuPendingMsg;
       bool IssueFromPendingBuf;

       //m_cohProtocol->UpdateCohActionList ();

       m_cohProtocol->ChkCohEvents ();

       cpuReqMsg    = m_cohProtocol->GetCpuReqMsg ();
       cpuReqEvent  = m_cohProtocol->GetCpuReqEvent ();
       IssueFromPendingBuf = false;

       if (m_pendingCpuReq < m_maxPendingReq) {
         if (!m_cpuPendingFIFO->IsEmpty()) {
           cpuPendingMsg = m_cpuPendingFIFO->GetFrontElement();
           if (cpuPendingMsg.IsIssued == false) {
             cpuReqEvent  = cpuPendingMsg.cpuReqEvent;
             cpuReqMsg    = cpuPendingMsg.cpuMsg;
             IssueFromPendingBuf = true;
             if (m_logFileGenEnable) {
               std::cout << "Core Id = " << m_coreId << " Pending Cpu Event Processed from the Pending Buffer, Pending Cnt " << m_pendingCpuReq << std::endl;
             }
           }
         }
       }
       else {
         cpuReqEvent = SNOOPPrivCoreEvent::Null;
       }

       busReqMsg    = m_cohProtocol->GetBusReqMsg ();
       busReqEvent  = m_cohProtocol->GetBusReqEvent ();

       busRespMsg   = m_cohProtocol->GetBusRespMsg ();
       busRespEvent = m_cohProtocol->GetBusRespEvent ();

       eventList.cpuReqEvent  = cpuReqEvent;
       eventList.busReqEvent  = busReqEvent;
       eventList.busRespEvent = busRespEvent;



       //cpuReqEvent  = ChkCoreEvent      (m_cpuFIFO  , cpuReqMsg );
       //busReqEvent  = ChkBusRxReqEvent  (m_busIfFIFO, busReqMsg );
       //busRespEvent = ChkBusRxRespEvent (m_busIfFIFO, busRespMsg);

       // Next, Generate PMSI next action/state for each type of Events
       SNOOPPrivCtrlAction cpuReqAction; 
       SNOOPPrivCtrlAction busReqAction;  
       SNOOPPrivCtrlAction busRespAction;

       SNOOPPrivCohTrans  cputrans2Issue,
                          busReqtrans2Issue,
                          busResptrans2Issue;

       PMSI::PMSIEventPriority cpuReqBusPriority,
                               busPriority;

       int                         cpuReqCacheNextState,
                                   cpuReqCacheCurrState,
                                   busReqCacheNextState,
                                   busReqCacheCurrState,
                                   busRespCacheNextState,
                                   busRespCacheCurrState;

       // get current state first
       cpuReqCacheCurrState  = m_cache->GetCacheLineState(cpuReqMsg.addr );
       busReqCacheCurrState  = m_cache->GetCacheLineState(busReqMsg.addr );
       busRespCacheCurrState = m_cache->GetCacheLineState(busRespMsg.addr);

       // set next state to current state before PMSI call, this is due
       // the bidirection design of the CacheEventProcessing fucntion
       cpuReqCacheNextState  = cpuReqCacheCurrState;
       busReqCacheNextState  = busReqCacheCurrState;
       busRespCacheNextState = busRespCacheCurrState;

       // Call PMSI protocol to get next actions/states
    #ifdef PMSI_PROT
       SNOOPPrivEventType eventType = SNOOPPrivEventType::Core;
       PMSI::CacheEventProcessing (eventType, eventList, cpuReqCacheNextState, cputrans2Issue, 
                                   cpuReqBusPriority, cpuReqAction,m_cach2Cache);

       eventType = SNOOPPrivEventType::ReqBus;
       PMSI::CacheEventProcessing (eventType, eventList, busReqCacheNextState, busReqtrans2Issue, 
                                   busPriority,  busReqAction,m_cach2Cache);

       eventType = SNOOPPrivEventType::RespBus;
       PMSI::CacheEventProcessing (eventType, eventList, busRespCacheNextState, busResptrans2Issue, 
                                   busPriority,  busRespAction,m_cach2Cache);
    #else
       SNOOPPrivEventType eventType = SNOOPPrivEventType::Core;
       MSI::CacheEventProcessing (eventType, eventList, cpuReqCacheNextState, cputrans2Issue, 
                                   cpuReqBusPriority, cpuReqAction,m_cach2Cache);

       eventType = SNOOPPrivEventType::ReqBus;
       MSI::CacheEventProcessing (eventType, eventList, busReqCacheNextState, busReqtrans2Issue, 
                                   busPriority,  busReqAction,m_cach2Cache);

       eventType = SNOOPPrivEventType::RespBus;
       MSI::CacheEventProcessing (eventType, eventList, busRespCacheNextState, busResptrans2Issue, 
                                   busPriority,  busRespAction,m_cach2Cache);
    #endif

      // Map Event's addresses into cache line format (i.e tag, index or set)
      GenericCacheMapFrmt cpuAddrMap   = m_cache->CpuAddrMap (cpuReqMsg.addr);
      uint32_t cpuReqCacheLineIdx      = cpuAddrMap.idx_set; 
      GenericCacheFrmt cpuReqCacheLine = m_cache->ReadCacheLine (cpuReqCacheLineIdx);

      GenericCacheMapFrmt busAddrMap   = m_cache->CpuAddrMap (busReqMsg.addr);
      uint32_t busReqCacheLineIdx      = busAddrMap.idx_set; 
      GenericCacheFrmt busReqCacheLine = m_cache->ReadCacheLine (busReqCacheLineIdx);

      GenericCacheMapFrmt respAddrMap  = m_cache->CpuAddrMap (busRespMsg.addr);
      uint32_t busRespCacheLineIdx      = respAddrMap.idx_set; 
      GenericCacheFrmt busRespCacheLine = m_cache->ReadCacheLine (busRespCacheLineIdx);

      // cpu request cache line fields
      GenericCacheMapFrmt cacheLineContents;
      cacheLineContents.tag     = cpuReqCacheLine.tag;
      cacheLineContents.idx_set = cpuReqCacheLineIdx;
      cacheLineContents.ofst    = 0;
        
      // cpu RxResp buffer message
      CpuFIFO::RespMsg CpuResp;

      // check if core Event needs a replacement to
      // be issued first before executed it.
      bool ReplacementEventFlag = false;
      if (cpuReqAction != SNOOPPrivCtrlAction::NullAck && MSI::IsValidBlk(cpuReqCacheCurrState  ) 
                                        && cacheLineContents.tag != cpuAddrMap.tag) 
      {
          ReplacementEventFlag = true;

          if (m_logFileGenEnable) {
            std::cout << "PrivCache " << m_coreId << "Cpu Req to addr       = " << cpuReqMsg.addr << " has tag mismatch, Cpu requested Tag = " << cpuAddrMap.tag << ", internal Cache Tag = " << cacheLineContents.tag <<  std::endl;
          }

          // a.1) generate a replacement event for the current cache line
          SNOOPPrivEventType eventType = SNOOPPrivEventType::Core;
          //cpuReqEvent = PMSI::PMSICoreEvent::CoreReplacemetEvent;
          cpuReqEvent = SNOOPPrivCoreEvent::Replacement;
          eventList.cpuReqEvent = cpuReqEvent;
          cpuReqCacheNextState = cpuReqCacheCurrState;

    #ifdef PMSI_PROT
          PMSI::CacheEventProcessing (eventType,    eventList, cpuReqCacheNextState, cputrans2Issue, 
                                      cpuReqBusPriority, cpuReqAction,m_cach2Cache);
    #else
          MSI::CacheEventProcessing (eventType,    eventList, cpuReqCacheNextState, cputrans2Issue, 
                                      cpuReqBusPriority, cpuReqAction,m_cach2Cache);
    #endif
         // a.2) set cpu request address to the internal cache line address   
         //      until the replacement done (exactly till the write back is done, and 
         //                                  cache line state goes to "I" state. At that
         //                                  time, PMSI::IsValidBlk function will return
         //                                  false and then the new Cpu request can go on)
         cpuReqMsg.addr = m_cache->CpuPhyAddr(cacheLineContents);
         cpuAddrMap     = m_cache->CpuAddrMap (cpuReqMsg.addr);
      }


/*
      // Skip Stall action as it doesn't
      // need to do anything and to reduce
      // log file size too.
      if (cpuReqAction == PMSI::Stall) {

        m_pendingCpuReq++;

        cpuReqAction = PMSI::NullAck;
      } 
*/
      if (m_logFileGenEnable) {
        // Print debug messages here
        if (cpuReqAction != SNOOPPrivCtrlAction::NullAck) {
          if (ReplacementEventFlag == true) {
            std::cout << "PrivCache " << m_coreId << " one-going Replacement Action" << " at ClkTic ================================================================== " 
                    << m_cacheCycle << std::endl;
            std::cout << "\t\t ReplcCacheLine      = " << cpuAddrMap.idx_set << " Cache Tag = " << cpuReqCacheLine.tag << std::endl;
          }
          else {
            std::cout << "\nPrivCache " << m_coreId << " has Cpu ReqAction" << " at ClkTic ================================================================== " 
                    << m_cacheCycle << std::endl;
            std::cout << "\t\t Cpu Req Addr        = " << cpuReqMsg.addr << " Type(0:Read) = " << cpuReqMsg.type << " CacheLine = " << cpuAddrMap.idx_set << " Cache Tag = " << cpuReqCacheLine.tag << std::endl;
          }
          std::cout << "\t\t CacheLine CurrState = " << m_cohProtocol->PrintPrivStateName(cpuReqCacheCurrState) << std::endl;
          std::cout << "\t\t CacheLine NextState = " << m_cohProtocol->PrintPrivStateName(cpuReqCacheNextState) << std::endl;
          std::cout << "\t\t Ctrl ReqAction      = " << m_cohProtocol->PrintPrivActionName(cpuReqAction) << std::endl;
          std::cout << "\t\t Ctrl ReqTrans       = " << PMSI::TransName(cputrans2Issue) << std::endl;
        } 

        if (busReqAction != SNOOPPrivCtrlAction::NullAck) {
          std::cout << "\nPrivCache " << m_coreId << " has Msg in the RxReq Bus" << " at ClkTic =========================================================== " 
                    << m_cacheCycle << std::endl;
          std::cout << "\t\t BusEventName        = " << PMSI::PrivReqBusEventName(busReqEvent) << std::endl;
          std::cout << "\t\t ReqCoreId           = " << busReqMsg.reqCoreId << ", RespCoreId = " << busReqMsg.wbCoreId << ", ReqMsgId = " << busReqMsg.msgId << ", Req Addr  = " << busReqMsg.addr << " CacheLine = " << busAddrMap.idx_set << " Cache Tag = " << busReqCacheLine.tag << " busAddrMap.tag " << busAddrMap.tag << std::endl;
          if (busReqCacheLine.tag != busAddrMap.tag) {
            std::cout << "\t\t Ignored BusReq to different Memory Location " << std::endl; 
          }
          else {
            std::cout << "\t\t CacheLine CurrState = " << m_cohProtocol->PrintPrivStateName(busReqCacheCurrState) << std::endl;
            std::cout << "\t\t CacheLine NextState = " << m_cohProtocol->PrintPrivStateName(busReqCacheNextState) << std::endl;
            std::cout << "\t\t Ctrl ReqAction      = " << m_cohProtocol->PrintPrivActionName(busReqAction) << std::endl;
            std::cout << "\t\t Ctrl ReqTrans       = " << PMSI::TransName(busReqtrans2Issue) << std::endl;
          }

        }

        if (busRespAction != SNOOPPrivCtrlAction::NullAck) {
          std::cout << "\nPrivCache " << m_coreId << " has Msg in the RxResp Bus" << " at ClkTic ============================================================ " 
                    << m_cacheCycle << std::endl;
          std::cout << "\t\t BusEventName        = " << PMSI::PrivRespBusEventName(busRespEvent) << std::endl;
          std::cout << "\t\t ReqCoreId           = " << busRespMsg.reqCoreId << " RespCoreId = " << busRespMsg.respCoreId << ", RespMsgId = " << busRespMsg.msgId << " Resp Addr  = " << busRespMsg.addr << " CacheLine = " << respAddrMap.idx_set << " Cache Tag = " << busRespCacheLine.tag << std::endl;
          std::cout << "\t\t CacheLine CurrState = " << m_cohProtocol->PrintPrivStateName(busRespCacheCurrState) << std::endl;
          std::cout << "\t\t CacheLine NextState = " << m_cohProtocol->PrintPrivStateName(busRespCacheNextState) << std::endl;
          std::cout << "\t\t Ctrl ReqAction      = " << m_cohProtocol->PrintPrivActionName(busRespAction) << std::endl;
          std::cout << "\t\t Ctrl ReqTrans       = " << PMSI::TransName(busResptrans2Issue) << std::endl;
        }
      } // if (m_logFileGenEnable) {

       /******************************
        * First Step is to process any 
        * pending response message.
        ******************************/

       /* a) CopyThenHit ACK Processing
        */
       if (busRespAction == SNOOPPrivCtrlAction::CopyThenHit             ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitWB           ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendMemOnly  ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendCoreMem ){
         // a.1) Remove message from the busResp buffer
         m_busIfFIFO->m_rxRespFIFO.PopElement();

         // a.2) Copy response data into private cache 
         UpdatePrivateCache (Line, busRespMsg.addr, busRespCacheNextState, busRespMsg.data);

         // remove message from pending buffer
         int pendingQueueSize = m_cpuPendingFIFO->GetQueueSize();
         GenericCacheMapFrmt  pendingcpuAddrMap;

         SNOOPPrivCtrlAction     cpuReqAction_temp; 
         SNOOPPrivCohTrans       cputrans2Issue_temp;
         int                     cpuReqCacheNextState_temp;
         PMSI::PMSIEventPriority cpuReqBusPriority_temp;
         SNOOPPrivEventType eventType_temp = SNOOPPrivEventType::Core;

         for (int i = 0; i < pendingQueueSize ;i++) {
           cpuPendingMsg = m_cpuPendingFIFO->GetFrontElement();
           pendingcpuAddrMap = m_cache->CpuAddrMap (cpuPendingMsg.cpuMsg.addr);

           m_cpuPendingFIFO->PopElement();

           // a.4) Push a new response msg into cpuRx FIFO
           CpuResp.reqcycle = cpuPendingMsg.cpuMsg.cycle;
           CpuResp.cycle    = m_cacheCycle;
           CpuResp.msgId    = cpuPendingMsg.cpuMsg.msgId;
           CpuResp.addr     = cpuPendingMsg.cpuMsg.addr;

           if (m_logFileGenEnable) {
             std::cout << "PendingCpuBuffer: Entery = " << i << ", core Id = " << m_coreId << " requestId = " << cpuPendingMsg.cpuMsg.msgId << " busRespMsg.msgId = " << busRespMsg.msgId << ", addr = " << cpuPendingMsg.cpuMsg.addr << " IssueFlag = " << cpuPendingMsg.IsIssued << std::endl;
           }

           if (cpuPendingMsg.cpuMsg.msgId == busRespMsg.msgId) {
             m_cpuFIFO->m_rxFIFO.InsertElement(CpuResp);
             m_pendingCpuReq--;
             if (m_logFileGenEnable) {
               std::cout << "Core Id = " << m_coreId << " New message get removed from the Pending Buffer, Pending Cnt " << m_pendingCpuReq << std::endl;
             }
           }
           else if (respAddrMap.idx_set == pendingcpuAddrMap.idx_set &&
                    respAddrMap.tag     == pendingcpuAddrMap.tag && 
                    cpuPendingMsg.IsIssued == false) {
   

             eventList.cpuReqEvent = cpuPendingMsg.cpuReqEvent;
             cpuReqCacheNextState_temp  = busRespCacheNextState;

      #ifdef PMSI_PROT
             PMSI::CacheEventProcessing (eventType_temp,    eventList, cpuReqCacheNextState_temp, cputrans2Issue_temp, 
                                        cpuReqBusPriority_temp, cpuReqAction_temp,m_cach2Cache);
      #else
             MSI::CacheEventProcessing (eventType_temp,    eventList, cpuReqCacheNextState_temp, cputrans2Issue_temp, 
                                        cpuReqBusPriority_temp, cpuReqAction_temp,m_cach2Cache);
      #endif

             if (cpuReqAction_temp == SNOOPPrivCtrlAction::Hit) {
               m_cpuFIFO->m_rxFIFO.InsertElement(CpuResp);
               m_pendingCpuReq--;
               if (m_logFileGenEnable) {
                 std::cout << "Core Id = " << m_coreId << " New message get removed from the Pending Buffer, Pending Cnt " << m_pendingCpuReq << std::endl;
               }
             }
             else {
               // dequeue
               m_cpuPendingFIFO->InsertElement(cpuPendingMsg);
             }
           }
           else {
               // dequeue
               m_cpuPendingFIFO->InsertElement(cpuPendingMsg);
           }
         }

         // a.5) check if there is any assoicated writeback
         //      need to be generated on the bus (i.e Pending write back)
         if (busRespAction == SNOOPPrivCtrlAction::CopyThenHitWB ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendMemOnly  ||
           busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendCoreMem ) {
           TransType type = (busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly) ? CoreOnly :
                            (busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendMemOnly ) ? MemOnly  :
                            (busRespAction == SNOOPPrivCtrlAction::CopyThenHitSendCoreMem ) ? CorePlsMem : CorePlsMem ;
           SendPendingWB (respAddrMap,type);
         }
         // a.6) skip processing cpu, bus request actions
         cpuReqAction = SNOOPPrivCtrlAction::NullAck;
         busReqAction = SNOOPPrivCtrlAction::NullAck;
       }
       /*
        * b) No Action Processing
        */
       else if (busRespAction == SNOOPPrivCtrlAction::NoAck){
         // b.1) Remove "other" DataResp Msgs
         m_busIfFIFO->m_rxRespFIFO.PopElement();
       }
       /*
        * c) Fault Action Processing
        */
       else if (busRespAction == SNOOPPrivCtrlAction::Fault) {
         if (m_logFileGenEnable) {
           std::cout << "DataResp occur in illegal state!" << std::endl;
         }
         exit(0);
       }
       /*
        * c) Check For Uncovered scenario
        */
       else if (busRespAction != SNOOPPrivCtrlAction::NullAck){
         if (m_logFileGenEnable) {
           std::cout << "DataResp uncovered Events occured!" << std::endl;
         }
         exit(0);
       }
       // mark busResp Action as processed
       busRespAction = SNOOPPrivCtrlAction::ProcessedAck;

       /******************************
        * Second, decide how to go with 
        * the the rest of the remaining   
        * (CPU, BUS) actions.
        ******************************/

       // requests to the same cache line, and same memory
       // location have to respect the controller priorities
       if (cpuReqAction != SNOOPPrivCtrlAction::NullAck && busReqAction != SNOOPPrivCtrlAction::NullAck){
         if (cpuAddrMap.idx_set  == busAddrMap.idx_set) {
           if (cpuReqBusPriority == PMSI::RequestBusEvent) {
             cpuReqAction = SNOOPPrivCtrlAction::NullAck;
             if (m_logFileGenEnable) 
               std::cout << "PrivCache " << m_coreId << " ReqBus Event priotrized over Core Event!" << std::endl;
           }
           else if (cpuReqBusPriority == PMSI::CoreReqBusEventsWorkConserv){
             m_prllActionCnt = m_prllActionCnt+1;
             if (m_prllActionCnt  > m_reqWbRatio) {
               m_prllActionCnt = 0;
               cpuReqAction = SNOOPPrivCtrlAction::NullAck;
               if (m_logFileGenEnable) 
                 std::cout << "PrivCache " << m_coreId << " WorkConservative Cpu, Bus Events. ReqBus Event priotrized over Core Event!" << std::endl;
             }
             else {
               busReqAction = SNOOPPrivCtrlAction::NullAck;
               if (m_logFileGenEnable) 
                 std::cout << "PrivCache " << m_coreId << " WorkConservative Cpu, Bus Events. Core Event priotrized over ReqBus Event!" << std::endl;
             }
           }
           else {
               if (m_logFileGenEnable) 
                 std::cout << "PrivCache " << m_coreId << " Parallel Cpu, Bus Events can be done!" << std::endl;
           }
         }
       }
       else {
         m_prllActionCnt = 0;
       }

       /*
        * Process CpuReq action
        */
       if (cpuReqAction != SNOOPPrivCtrlAction::NullAck) {

         if (cpuReqAction == SNOOPPrivCtrlAction::Hit) {
           // a.1.1.1) Remove request from CpuReq FIFO
           m_cpuFIFO->m_txFIFO.PopElement();

           // a.1.1.2) Push a new response msg into CpuRx FIFO 
           CpuResp.reqcycle = cpuReqMsg.cycle;
           CpuResp.addr  = cpuReqMsg.addr;
           CpuResp.cycle = m_cacheCycle;
           CpuResp.msgId = cpuReqMsg.msgId;

           m_cpuFIFO->m_rxFIFO.InsertElement(CpuResp);
         }
         else if (cpuReqAction == SNOOPPrivCtrlAction::issueTrans) {


           if (ReplacementEventFlag == false) {

             if (IssueFromPendingBuf == true) {
               cpuPendingMsg.IsIssued = true;
               m_cpuPendingFIFO->UpdateFrontElement(cpuPendingMsg);
             }
             else {
               m_pendingCpuReq++;
               cpuPendingMsg.cpuMsg = cpuReqMsg;
               cpuPendingMsg.IsIssued = true;
               cpuPendingMsg.cpuReqEvent = cpuReqEvent;

               if (m_logFileGenEnable) {
                 std::cout << "Core Id = " << m_coreId << " New Get message Inserted Into the Pending Buffer, Pending Cnt " << m_pendingCpuReq << std::endl;

                 std::cout << "PendingCpuBuffer: " << ", core Id = " << m_coreId << " requestId = " << cpuPendingMsg.cpuMsg.msgId << ", addr = " << cpuPendingMsg.cpuMsg.addr << " IssueFlag = " << cpuPendingMsg.IsIssued << std::endl;
               }

               // a.1.1.1) Remove request from CpuReq FIFO
               m_cpuFIFO->m_txFIFO.PopElement();

               // insert msg into pending buffer
               m_cpuPendingFIFO->InsertElement(cpuPendingMsg);
             }
           }


           // a.1.2.1) Push transaction into BusTxMsg FIFO
          uint16_t wbCoreId = (cpuReqEvent == SNOOPPrivCoreEvent::Replacement) ? m_coreId : m_sharedMemId;
          uint16_t reqCoreId= (cpuReqEvent == SNOOPPrivCoreEvent::Replacement) ? m_sharedMemId : m_coreId;
          uint64_t msgId = (cpuReqEvent == SNOOPPrivCoreEvent::Replacement) ? 0 : cpuReqMsg.msgId;
           if (!PushMsgInBusTxFIFO (msgId, reqCoreId, wbCoreId, cputrans2Issue, cpuReqMsg.addr)) {
             if (m_logFileGenEnable) {
               std::cout << "PrivCache " << m_coreId << " TxReq FIFO is Full!" << std::endl;
             }
             exit(0);
           }
           // a.1.2.2) [Note]: Cannot remove Cpu request till hit happen or data response came
         }
         // filter out other actions
         else if (cpuReqAction == SNOOPPrivCtrlAction::Stall) {
           /* a.2) Postpone coreEvent untill the 
            * previous action get processed. 
            * Currently, we assume in-order execution
            */ 
           //std::cout << "CPU " << m_coreId << " Stall Event generated" << std::endl;
           if (ReplacementEventFlag == false && IssueFromPendingBuf == false) {
             m_pendingCpuReq++;
             cpuPendingMsg.cpuMsg = cpuReqMsg;
             cpuPendingMsg.IsIssued = false;
             cpuPendingMsg.cpuReqEvent = cpuReqEvent;
             // a.1.1.1) Remove request from CpuReq FIFO
             m_cpuFIFO->m_txFIFO.PopElement();

             if (m_logFileGenEnable) {
               std::cout << "Core Id = " << m_coreId << " New Stall message Inserted Into the Pending Buffer, Pending Cnt " << m_pendingCpuReq << std::endl;
             }
             // insert msg into pending buffer
             m_cpuPendingFIFO->InsertElement(cpuPendingMsg);
           }

         }
         else if (cpuReqAction == SNOOPPrivCtrlAction::NoAck) {
           /* a.3) No action is required, just 
            * update state
            */ 
         }
         else if (cpuReqAction == SNOOPPrivCtrlAction::Fault) {
           // a.3) illegal Event at this state
           if (m_logFileGenEnable) {
             std::cout << "PrivCache " << m_coreId << " CoreEvent occur in illegal state!" << std::endl;
           }
           exit(0);
         }
         // a.4) Check For Uncovered scenario
         else {
           if (m_logFileGenEnable) {
             std::cout << "nPrivCache " << m_coreId << " uncovered Events occured!" << std::endl;
           }
           exit(0);
         }

         if (cpuReqAction != SNOOPPrivCtrlAction::Stall) {
           // Update cache line state 
           UpdatePrivateCache (Line, cpuReqMsg.addr, cpuReqCacheNextState, cpuReqMsg.data);
         }
         // Mark Cpu action as processed
         cpuReqAction = SNOOPPrivCtrlAction::ProcessedAck;
       }
   
       /*
        * Process BusReq action
        */
       if (busReqAction != SNOOPPrivCtrlAction::NullAck) {

         if (busReqCacheLine.tag != busAddrMap.tag) {
          // ignore BusReq for different Tag (i.e. different memory location)
          m_busIfFIFO->m_rxMsgFIFO.PopElement();
         }
         else {
           // a.2.1) this happen only at SM_W state
           if (busReqAction == SNOOPPrivCtrlAction::Hit) { 

             // a.2.1.1) Remove request from CpuReq FIFO
             m_cpuFIFO->m_txFIFO.PopElement();  

             // a.2.1.2) Push a new response msg into cpuRx FIFO 
             CpuResp.addr  = busReqMsg.addr;
             CpuResp.cycle = m_cacheCycle;
             CpuResp.msgId = busReqMsg.msgId;
             CpuResp.reqcycle = cpuReqMsg.cycle;
             m_cpuFIFO->m_rxFIFO.InsertElement(CpuResp);

             // a.2.1.3) Remove message from the busReq buffer
             m_busIfFIFO->m_rxMsgFIFO.PopElement();
           }
           // a.2.2) this happen only in write back states
           else if (busReqAction == SNOOPPrivCtrlAction::WritBack     ||
                    busReqAction == SNOOPPrivCtrlAction::SendMemOnly  || 
                    busReqAction == SNOOPPrivCtrlAction::SendCoreOnly ||
                    busReqAction == SNOOPPrivCtrlAction::SendCoreMem   ) {

             // a.2.2.1) Remove message from the busReq buffer
             m_busIfFIFO->m_rxMsgFIFO.PopElement();

             // a.2.2.2) Do write back
             uint16_t reqCoreId = (busReqAction ==  SNOOPPrivCtrlAction::SendCoreOnly || busReqAction == SNOOPPrivCtrlAction::SendCoreMem) ? busReqMsg.reqCoreId : m_sharedMemId;
             bool dualTrans = (busReqAction == SNOOPPrivCtrlAction::SendCoreMem);
             if (!DoWriteBack (busReqMsg.addr, reqCoreId, busReqMsg.msgId, dualTrans)) {
               if (m_logFileGenEnable) {
                 std::cout << "This is will cause stall in the PMSI state machine !!!!" << std::endl;
               }
               exit(0);
             }

           }
           // a.2.3) this can be only PutM()
           else if (busReqAction == SNOOPPrivCtrlAction::issueTrans || 
                    busReqAction == SNOOPPrivCtrlAction::issueTransSaveWbId ) {
             // a.2.3.1) Remove message from the busReq buffer
             m_busIfFIFO->m_rxMsgFIFO.PopElement();
             if (busReqtrans2Issue != PutMTrans) {
               if (m_logFileGenEnable) {
                 std::cout << "Error This should be PutM() transaction, coreId =  " << m_coreId << " transaction = " <<  busReqtrans2Issue << std::endl;
               }
               exit(0);
             }
             //uint16_t reqCoreId = (m_cach2Cache == true) ? busReqMsg.reqCoreId : m_sharedMemId;
             uint16_t reqCoreId = busReqMsg.reqCoreId;
             if (!PushMsgInBusTxFIFO (busReqMsg.msgId, reqCoreId, m_coreId, busReqtrans2Issue, busReqMsg.addr)) {
               if (m_logFileGenEnable) {
                 std::cout << "This is will cause stall in the PMSI state machine !!!!" << std::endl;
               }
               exit(0);
             }
             if (busReqAction == SNOOPPrivCtrlAction::issueTransSaveWbId) {
               // save pending write back coreId & address
               if (!PushMsgInBusTxFIFO (busReqMsg.msgId, busReqMsg.reqCoreId, m_coreId,busReqtrans2Issue, busReqMsg.addr, true)) {
                 if (m_logFileGenEnable) {
                   std::cout << "PrivCache: Pending Wb buffer is full !!!!" << std::endl;
                 }
                 exit(0);
               }
             }
           }
           else if (busReqAction == SNOOPPrivCtrlAction::SaveWbCoreId) {
               m_busIfFIFO->m_rxMsgFIFO.PopElement();
               // save pending write back coreId & address
               if (!PushMsgInBusTxFIFO (busReqMsg.msgId, busReqMsg.reqCoreId, m_coreId,busReqtrans2Issue, busReqMsg.addr, true)) {
                 if (m_logFileGenEnable) {
                   std::cout << "PrivCache: Pending Wb buffer is full !!!!" << std::endl;
                 }
                 exit(0);
               }
           }
           else if (busReqAction == SNOOPPrivCtrlAction::ReissueTrans) {
             // a.2.4.1) Remove message from the busReq buffer
             m_busIfFIFO->m_rxMsgFIFO.PopElement();
             // replace the previous Upg message with GetM
             if (!ReplcMsgInBusTxFIFO(SNOOPPrivCohTrans::UpgTrans, busReqMsg.addr,busReqtrans2Issue)) {
               if (m_logFileGenEnable) {
                 std::cout << "\t\tPrivCache UpgTrans Replacement cannot be found in the buffer!!!!" << std::endl;
               }
               exit(0);
             } 
             else {
               if (m_logFileGenEnable) {
                 std::cout << "\t\tPrivCache " << m_coreId << "UpgTrans Replacement with GetM is done" << std::endl;
               }
             }
           }
           // filter out other actions
           else if (busReqAction == SNOOPPrivCtrlAction::NoAck) {
             // b.1) remove no-action event
             m_busIfFIFO->m_rxMsgFIFO.PopElement();
           }
           else if (busReqAction == SNOOPPrivCtrlAction::Fault) {
             // b.2) genenerate error message
             if (m_logFileGenEnable) {
               std::cout << "BusReqEvent occur in illegal state!" << std::endl;
             }
             exit(0);
           }
           // a.1.2.3) Check For Uncovered scenario
           else {
             if (m_logFileGenEnable) {
               std::cout << "busReqEvent " << m_coreId << " uncovered Events occured!" << std::endl;
             }
             exit(0);
           }

           // a.2.2) update cache line state 
           UpdatePrivateCache (State, busReqMsg.addr, busReqCacheNextState , NULL);
         }
         // a.2.3) mark action as processed
         busReqAction = SNOOPPrivCtrlAction::ProcessedAck;

       }

     } // PrivateCacheCtrl::CacheCtrlMain ()

    void PrivateCacheCtrl::CycleProcess() {

       //std::cout << "Core Id = " << m_coreId << " tic... " << m_cacheCycle << std::endl;
       CacheCtrlMain();
      // Schedule the next run
      Simulator::Schedule(NanoSeconds(m_dt), &PrivateCacheCtrl::Step, Ptr<PrivateCacheCtrl > (this));
      m_cacheCycle++;
    }

    // The init function starts the controller at the beginning 
    void PrivateCacheCtrl::init() {
        // initialize private cache states
      #ifdef PMSI_PROT
        m_cache->InitalizeCacheStates(static_cast<int>(SNOOP_PMSIPrivCacheState::I));
        m_cohProtocol->SetProtocolType    (CohProtType::SNOOP_PMSI);
      #else
        m_cache->InitalizeCacheStates(static_cast<int>(SNOOP_MSIPrivCacheState::I));
        m_cohProtocol->SetProtocolType    (CohProtType::SNOOP_MSI);
      #endif
        // Initialized Cache Coherence Protocol
        m_cohProtocol->SetLogFileGenEnable(m_logFileGenEnable);
        m_cohProtocol->SetCoreId          (m_coreId          );
        m_cohProtocol->SetSharedMemId     (m_sharedMemId     );
        m_cohProtocol->SetPrivCachePtr    (m_cache           );
        m_cohProtocol->SetCpuFIFOPtr      (m_cpuFIFO         );
        m_cohProtocol->SetBusFIFOPtr      (m_busIfFIFO       );
        m_cohProtocol->SetCache2Cache     (m_cach2Cache      );
        m_cohProtocol->SetReqWbRatio      (m_reqWbRatio      );
        m_cohProtocol->SetMaxPendingReq   (1                 );
        m_cohProtocol->SetPendingCpuFIFODepth (1             );

        Simulator::Schedule(NanoSeconds(m_clkSkew), &PrivateCacheCtrl::Step, Ptr<PrivateCacheCtrl > (this));
    }


    /**
     * Runs one mobility Step for the given vehicle generator.
     * This function is called each interval dt
     */

    void PrivateCacheCtrl::Step(Ptr<PrivateCacheCtrl> privateCacheCtrl) {
        privateCacheCtrl->CycleProcess();
    }

}

/*
 * File  :      SNOOPPrivCohProtocol.cc
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 30, 2020
 */

#include "SNOOPPrivCohProtocol.h"

namespace ns3 {

    // override ns3 type
    TypeId SNOOPPrivCohProtocol::GetTypeId(void) {
        static TypeId tid = TypeId("ns3::SNOOPPrivCohProtocol")
               .SetParent<Object > ();
        return tid;
    }

    SNOOPPrivCohProtocol::SNOOPPrivCohProtocol() {
        // default
        m_pType            = CohProtType::SNOOP_PMSI;
        m_processEvent     = SNOOPPrivEventType::Core;
        m_coreId           = 1;
        m_sharedMemId      = 10;
        m_cache2Cache      = false;
        m_logFileGenEnable = false;
        m_prllActionCnt    = 0;
        m_reqWbRatio       = 0;
        m_maxPendingReq    = 0;
        m_pendingCpuReq    = 0;
        m_currEventTrans2Issue = NullTrans;
        m_cpuPendingFIFO   = CreateObject<GenericFIFO <CpuFIFO::ReqMsg >> ();
        m_replcPolicy      = ReplcPolicy::RANDOM;
        uRnd1 = CreateObject<UniformRandomVariable> ();
        uRnd1-> SetAttribute ("Min", DoubleValue (0));
        uRnd1-> SetAttribute ("Max", DoubleValue (100));
    }

    // We don't do any dynamic allocations
    SNOOPPrivCohProtocol::~SNOOPPrivCohProtocol() {
    }

    // Set Log File Enable Flag
    void SNOOPPrivCohProtocol::SetLogFileGenEnable (bool logFileGenEnable ) {
      m_logFileGenEnable = logFileGenEnable;
    }

    // Set Associated CoreId
    void SNOOPPrivCohProtocol::SetCoreId (int coreId) {
      m_coreId = coreId;
    }

    // Set Associated SharedMemId
    void SNOOPPrivCohProtocol::SetSharedMemId (int sharedMemId) {
      m_sharedMemId = sharedMemId;
    }

    // Set Ratio Between Process CoreEvents and WriteBack Events
    void SNOOPPrivCohProtocol::SetReqWbRatio (int reqWbRatio) {
      m_reqWbRatio = reqWbRatio;
    }

    // Set Coherence Protocol Type
    void SNOOPPrivCohProtocol::SetProtocolType (CohProtType ptype) {
      m_pType = ptype;
    }

    // Set Maximum number of Pending CPU Request (OOO)
    void SNOOPPrivCohProtocol::SetMaxPendingReq (int maxPendingReq) {
      m_maxPendingReq = maxPendingReq;
    }

    // Set Pending CPU FIFI depth
    void SNOOPPrivCohProtocol::SetPendingCpuFIFODepth (int size) {
      m_cpuPendingFIFO->SetFifoDepth(size);
    }


    // Get Coherence Protocol Type
    CohProtType SNOOPPrivCohProtocol::GetProtocolType () {
      return m_pType;
    } 

    // Set Pointer to Private Cache
    void SNOOPPrivCohProtocol::SetPrivCachePtr (Ptr<GenericCache> privCache) {
      m_privCache = privCache;
    }

    // Set Pointer for Cpu FIFO buffer
    void SNOOPPrivCohProtocol::SetCpuFIFOPtr (Ptr<CpuFIFO> cpuFIFO) {
      m_cpuFIFO = cpuFIFO;
    }

    // Set Pointer for Buf IF FIFO buffer
    void SNOOPPrivCohProtocol::SetBusFIFOPtr (Ptr<BusIfFIFO> busFIFO) {
      m_busIfFIFO = busFIFO;
    }

    // Set Cache-to-Cache Communication Flag
    void SNOOPPrivCohProtocol::SetCache2Cache (bool cache2Cache) {
      m_cache2Cache = cache2Cache;
    }

    // Get Current Process CPU Msg
    CpuFIFO::ReqMsg SNOOPPrivCohProtocol::GetCpuReqMsg () {
      return m_msgList.cpuReqMsg;
    }

    // Get Current Process ReqBus Msg
    BusIfFIFO::BusReqMsg SNOOPPrivCohProtocol::GetBusReqMsg () {
      return m_msgList.busReqMsg;
    }

    // Get Current Process RespBus Msg
    BusIfFIFO::BusRespMsg SNOOPPrivCohProtocol::GetBusRespMsg () {
      return m_msgList.busRespMsg;
    }

    // Get Current Process CPU Event
    SNOOPPrivCoreEvent SNOOPPrivCohProtocol::GetCpuReqEvent () {
      return m_eventList.cpuReqEvent;
    }

    // Get Current Process ReqBus Event
    SNOOPPrivReqBusEvent SNOOPPrivCohProtocol::GetBusReqEvent () {
      return m_eventList.busReqEvent;
    }

    // Get Current Process RespBus Event
    SNOOPPrivRespBusEvent SNOOPPrivCohProtocol::GetBusRespEvent () {
      return m_eventList.busRespEvent;
    }

    // Check Core Events 
    void SNOOPPrivCohProtocol::ChkCoreEvent () {
       if (!m_cpuFIFO->m_txFIFO.IsEmpty()) {
         m_msgList.cpuReqMsg = m_cpuFIFO->m_txFIFO.GetFrontElement();
         if (m_msgList.cpuReqMsg.type == CpuFIFO::REQTYPE::READ) {
           m_eventList.cpuReqEvent = SNOOPPrivCoreEvent::Load;
         }
         else {
           m_eventList.cpuReqEvent = SNOOPPrivCoreEvent::Store;
         }
       }
       else {
         m_eventList.cpuReqEvent = SNOOPPrivCoreEvent::Null;
       }
    }

    // Check Request Bus Events
    void SNOOPPrivCohProtocol::ChkReqBusEvent() {
       if (!m_busIfFIFO->m_rxMsgFIFO.IsEmpty()) {
         m_msgList.busReqMsg = m_busIfFIFO->m_rxMsgFIFO.GetFrontElement();
         int reqCoreId = m_msgList.busReqMsg.reqCoreId;
         int wbCoreId = m_msgList.busReqMsg.wbCoreId;
         switch (m_msgList.busReqMsg.cohrMsgId) {
           case SNOOPPrivCohTrans::GetSTrans:
             m_eventList.busReqEvent = (reqCoreId == m_coreId) ? SNOOPPrivReqBusEvent::OwnGetS  : 
                                                                 SNOOPPrivReqBusEvent::OtherGetS;
             break;
           case SNOOPPrivCohTrans::GetMTrans:
             m_eventList.busReqEvent = (reqCoreId == m_coreId) ? SNOOPPrivReqBusEvent::OwnGetM  : 
                                                                 SNOOPPrivReqBusEvent::OtherGetM;
             break;
           case SNOOPPrivCohTrans::UpgTrans :
             m_eventList.busReqEvent = (reqCoreId == m_coreId) ? SNOOPPrivReqBusEvent::OwnUpg  : 
                                                                 SNOOPPrivReqBusEvent::OtherUpg;
             break;
           case SNOOPPrivCohTrans::PutMTrans:
             m_eventList.busReqEvent = (wbCoreId == m_coreId ) ? SNOOPPrivReqBusEvent::OwnPutM  : 
                                                                 SNOOPPrivReqBusEvent::OtherPutM;
             break;
           case SNOOPPrivCohTrans::PutSTrans:
             m_eventList.busReqEvent = (wbCoreId == m_coreId ) ? SNOOPPrivReqBusEvent::OwnPutS  : 
                                                                 SNOOPPrivReqBusEvent::OtherPutS;
             break;
           default: // Invalid Transaction
             if (m_logFileGenEnable) {
               std::cout << " SNOOPPrivCohProtocol: Invalid Transaction detected on the Bus" << std::endl;
             }
             exit(0);
           }
       }
       else {
         m_eventList.busReqEvent = SNOOPPrivReqBusEvent::Null;
       }
    }

    // Check Response Bus Events
    void SNOOPPrivCohProtocol::ChkRespBusEvent () {
       if (!m_busIfFIFO->m_rxRespFIFO.IsEmpty()) {
         m_msgList.busRespMsg = m_busIfFIFO->m_rxRespFIFO.GetFrontElement();
         if (m_cache2Cache == false) {
           m_eventList.busRespEvent =  (m_msgList.busRespMsg.reqCoreId  == m_coreId && 
                                        m_msgList.busRespMsg.respCoreId == m_sharedMemId) ? SNOOPPrivRespBusEvent::OwnDataResp : 
                                                                                            SNOOPPrivRespBusEvent::OtherDataResp;
         }
         else {
           m_eventList.busRespEvent = (m_msgList.busRespMsg.reqCoreId == m_coreId) ? SNOOPPrivRespBusEvent::OwnDataResp : 
                                                                                     SNOOPPrivRespBusEvent::OtherDataResp;
         }
       }
       else {
         m_eventList.busRespEvent = SNOOPPrivRespBusEvent::Null;
       }
    }

    // Chk and Update Coherence
    // Protocol Event List
    void SNOOPPrivCohProtocol::ChkCohEvents () {
       ChkCoreEvent ();
       ChkReqBusEvent();
       ChkRespBusEvent();
    }

    // Generate Coherence Protocol
    // Action lists
    void SNOOPPrivCohProtocol::UpdateCohActionList (){

      // Check available Coherence Events
      ChkCohEvents ();

      // Get Cache Line state coresponding to each event
      GetEventsCacheInfo ();

      // Check Which Event Get Executed first
      CohEventsSerialize ();

      //std::cout << "\t\tSNOOPPriVCohrProtocol: CoreId = " << m_coreId << PrintPrivEventType(m_processEvent) << std::endl;

      // Update State Info
      if (m_processEvent == SNOOPPrivEventType::Core) {
        ProcessCoreEvent ();
      }
      else if (m_processEvent == SNOOPPrivEventType::ReqBus || 
               m_processEvent == SNOOPPrivEventType::RespBus) {
        ProcessBusEvents ();
      }

      PrintEventInfo ();

    }

    void SNOOPPrivCohProtocol::ProcessCoreEvent () {
      uint32_t replcClIdx;
      int      emptyIdx;
      GenericCacheFrmt CacheLine;

      // if cache line exist in the private cache
      if (m_eventCacheInfoList.cpuReqCacheLineInfo.IsExist == false) {
        // if no empty space in the cache, do replacement
        if (m_eventCacheInfoList.cpuReqCacheLineInfo.IsSetFull == true) {
          m_eventList.cpuReqEvent = SNOOPPrivCoreEvent::Replacement;  
          replcClIdx = m_privCache->GetReplacementLine(m_eventCacheInfoList.cpuReqCacheLineInfo.setOfst , m_replcPolicy);
          CacheLine  = m_privCache->ReadCacheLine(replcClIdx);
          m_eventCacheInfoList.cpuReqCacheLineInfo.cl_idx = replcClIdx;
          m_eventCacheInfoList.cpuReqCacheLineInfo.state  = CacheLine.state;

          m_msgList.cpuReqMsg.addr  = m_privCache->CpuPhyAddr(replcClIdx);
          m_msgList.cpuReqMsg.msgId = 0; // MsgId for replacement
        }
        else {
          // if there is empty location in the cache
          emptyIdx = m_privCache->GetEmptyCacheLine(m_msgList.cpuReqMsg.addr);
          if (emptyIdx == -1 ) {
            std::cout << "SNOOPPrivCohProtocol: Fault non-empty condition !!!" << std::endl;
            exit(0);
          }
          CacheLine = m_privCache->ReadCacheLine(emptyIdx);   
          m_eventCacheInfoList.cpuReqCacheLineInfo.cl_idx = emptyIdx;
          m_eventCacheInfoList.cpuReqCacheLineInfo.state  = CacheLine.state;   
        }
      }
      m_currEventCurrState = m_eventCacheInfoList.cpuReqCacheLineInfo.state;
      m_currEventNextState = m_eventCacheInfoList.cpuReqCacheLineInfo.state;
      CohProtocolFSMProcessing ();
    }

    void SNOOPPrivCohProtocol::ProcessBusEvents (){
      if (m_processEvent == SNOOPPrivEventType::ReqBus) {
        m_currEventCurrState = m_eventCacheInfoList.busReqCacheLineInfo.state;
        m_currEventNextState = m_eventCacheInfoList.busReqCacheLineInfo.state;
      }
      else {
        m_currEventCurrState = m_eventCacheInfoList.busRespCacheLineInfo.state;
        m_currEventNextState = m_eventCacheInfoList.busRespCacheLineInfo.state;
      }

      // check if request to an existant cache line
      if ((m_eventCacheInfoList.busReqCacheLineInfo.IsExist  == false && m_processEvent == SNOOPPrivEventType::ReqBus) ||
          (m_eventCacheInfoList.busRespCacheLineInfo.IsExist == false && m_processEvent == SNOOPPrivEventType::RespBus)) {
        m_currEventTrans2Issue = SNOOPPrivCohTrans::NullTrans;
        m_ctrlAction = SNOOPPrivCtrlAction::NoAck;
      }
      else {
        CohProtocolFSMProcessing ();
      }
    }

    // Cohr FSM Function Call
    void SNOOPPrivCohProtocol::CohProtocolFSMProcessing () {
      // Check Snooping Protocol Type
      switch (m_pType) {
        case CohProtType::SNOOP_PMSI: 
          CohProtocolFSMBinding(new PMSI);
          break;
        case CohProtType::SNOOP_MSI:
          CohProtocolFSMBinding(new MSI);
          break;
        default:
          std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
          exit(0);
        }
    }

    // dynamic binding
    void SNOOPPrivCohProtocol::CohProtocolFSMBinding (IFCohProtocol *obj){
      return obj->SNOOPPrivEventProcessing 
                    (m_processEvent, 
                     m_eventList, 
                     m_currEventNextState, 
                     m_currEventTrans2Issue, 
                     m_ctrlAction, 
                     m_cache2Cache    
                    );
    }

    void SNOOPPrivCohProtocol::InitializeCacheStates () {
       int initState;
      // Check Snooping Protocol Type
      switch (m_pType) {
        case CohProtType::SNOOP_PMSI: 
          initState = static_cast<int>(SNOOP_PMSIPrivCacheState::I);
          break;
        case CohProtType::SNOOP_MSI:
          initState = static_cast<int>(SNOOP_MSIPrivCacheState::I);
          break;
        default:
          std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
          exit(0);
        }
        m_privCache->InitalizeCacheStates(initState);
    }

    void SNOOPPrivCohProtocol::GetEventsCacheInfo () {

      if (m_eventList.cpuReqEvent != SNOOPPrivCoreEvent::Null) {
        m_eventCacheInfoList.cpuReqCacheLineInfo  = m_privCache->GetCacheLineInfo(m_msgList.cpuReqMsg.addr);
      }

      if (m_eventList.busReqEvent != SNOOPPrivReqBusEvent::Null) {
        m_eventCacheInfoList.busReqCacheLineInfo  = m_privCache->GetCacheLineInfo(m_msgList.busReqMsg.addr);
      }

      if (m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null) {
        m_eventCacheInfoList.busRespCacheLineInfo  = m_privCache->GetCacheLineInfo(m_msgList.busRespMsg.addr);
      }
    }


    // dynamic binding
    SNOOPPrivEventPriority SNOOPPrivCohProtocol::PrivEventPriorityBinding (IFCohProtocol *obj){
      return obj->PrivCacheEventPriority 
                    (m_eventList, 
                     m_eventCacheInfoList 
                    );
    }

    void SNOOPPrivCohProtocol::CohEventsSerialize () {

      // Protocol's dependance eventPriority
      SNOOPPrivEventPriority eventPriority;
      switch (m_pType) {
        case CohProtType::SNOOP_PMSI: 
          eventPriority = PrivEventPriorityBinding(new PMSI); 
          break;
        case CohProtType::SNOOP_MSI:
          eventPriority = PrivEventPriorityBinding(new MSI); 
          break;
        default:
          std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
          exit(0);
      }

      // set default value for m_processEvent
      m_processEvent = SNOOPPrivEventType::Null;

      // uniform distribution for Request/Response bus
      if (m_eventList.busReqEvent  != SNOOPPrivReqBusEvent::Null && 
          m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null){
        m_processEvent = (uRnd1->GetValue() <= 50) ? SNOOPPrivEventType::ReqBus : SNOOPPrivEventType::RespBus;
      }
      else if (m_eventList.busReqEvent  != SNOOPPrivReqBusEvent::Null) {
        m_processEvent = SNOOPPrivEventType::ReqBus;
      }
      else if (m_eventList.busRespEvent  != SNOOPPrivRespBusEvent::Null) {
        m_processEvent = SNOOPPrivEventType::RespBus;
      }

      // Check if certain events needs to be priotrized
      if (eventPriority == SNOOPPrivEventPriority::ReqBus) {
        m_processEvent = SNOOPPrivEventType::ReqBus;
      }
      else if (eventPriority == SNOOPPrivEventPriority::RespBus) {
        m_processEvent = SNOOPPrivEventType::RespBus;
      }
      else if (eventPriority == SNOOPPrivEventPriority::WorkConserv) {
        if (m_eventList.cpuReqEvent != SNOOPPrivCoreEvent::Null && m_pendingCpuReq < m_maxPendingReq){
          if (m_eventList.busReqEvent != SNOOPPrivReqBusEvent::Null || 
              m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null) {
            m_prllActionCnt++;
            if (m_prllActionCnt  > m_reqWbRatio) {
              m_prllActionCnt = 0;
            }
            else {
              m_processEvent = SNOOPPrivEventType::Core;
            }
          }
          else {
            m_processEvent = SNOOPPrivEventType::Core;
          }
        }
      }   
    }

    void SNOOPPrivCohProtocol::PrintEventInfo () {
      if (m_processEvent == SNOOPPrivEventType::Core) {
        std::cout << "\nSNOOPPrivCohProtocol: CoreId = " << m_coreId << " has Cpu ReqAction" << " ================================================================== " << std::endl;
        std::cout << "\t\t Cpu Req Addr = " << m_msgList.cpuReqMsg.addr << " Type(0:Read) = " << m_msgList.cpuReqMsg.type << " CacheLine = " << m_eventCacheInfoList.cpuReqCacheLineInfo.cl_idx << std::endl;
        std::cout << "\t\t CacheLine Info: \n\t\t\t IsExist         =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.IsExist << " \n\t\t\t IsValid         =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.IsValid << " \n\t\t\t IsSetFull       =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.IsSetFull << std::endl;
        std::cout << "\t\t\t CurrState       = " << PrintPrivStateName(m_currEventCurrState) << std::endl;
        std::cout << "\t\t\t NextState       = " << PrintPrivStateName(m_currEventNextState) << std::endl;
        std::cout << "\t\t\t Ctrl ReqAction  = " << PrintPrivActionName(m_ctrlAction) << std::endl;
        std::cout << "\t\t\t Ctrl ReqTrans   = " << PrintTransName(m_currEventTrans2Issue) << std::endl;
      }


      if (m_processEvent == SNOOPPrivEventType::ReqBus) {
        std::cout << "\nSNOOPPrivCohProtocol: CoreId = " << m_coreId << " has Msg on the RxReq Bus" << " ================================================================== " << std::endl;
        std::cout << "\t\t BusEventName        = " << PMSI::PrivReqBusEventName(m_eventList.busReqEvent) << std::endl;
        std::cout << "\t\t ReqCoreId           = " << m_msgList.busReqMsg.reqCoreId << ", RespCoreId = " << m_msgList.busReqMsg.wbCoreId << ", ReqMsgId = " << m_msgList.busReqMsg.msgId << ", Req Addr  = " << m_msgList.busReqMsg.addr << " CacheLine = " << m_eventCacheInfoList.busReqCacheLineInfo.cl_idx << std::endl;
        std::cout << "\t\t CacheLine Info: \n\t\t\t IsExist         =  " << m_eventCacheInfoList.busReqCacheLineInfo.IsExist << " \n\t\t\t IsValid         =  " << m_eventCacheInfoList.busReqCacheLineInfo.IsValid << std::endl;
        std::cout << "\t\t\t CurrState       = " << PrintPrivStateName(m_currEventCurrState) << std::endl;
        std::cout << "\t\t\t NextState       = " << PrintPrivStateName(m_currEventNextState) << std::endl;
        std::cout << "\t\t\t Ctrl ReqAction  = " << PrintPrivActionName(m_ctrlAction) << std::endl;
        std::cout << "\t\t\t Ctrl ReqTrans   = " << PrintTransName(m_currEventTrans2Issue) << std::endl;
      }

      if (m_processEvent == SNOOPPrivEventType::RespBus) {
        std::cout << "\nSNOOPPrivCohProtocol: CoreId = " << m_coreId << " has Msg on the RxResp Bus" << " ================================================================== " << std::endl;
        std::cout << "\t\t BusEventName        = " << PMSI::PrivRespBusEventName(m_eventList.busRespEvent) << std::endl;
        std::cout << "\t\t ReqCoreId           = " << m_msgList.busRespMsg.reqCoreId << ", RespCoreId = " << m_msgList.busRespMsg.respCoreId << ", ReqMsgId = " << m_msgList.busRespMsg.msgId << ", Req Addr  = " << m_msgList.busRespMsg.addr << " CacheLine = " << m_eventCacheInfoList.busRespCacheLineInfo.cl_idx << std::endl;
        std::cout << "\t\t CacheLine Info: \n\t\t\t IsExist         =  " << m_eventCacheInfoList.busRespCacheLineInfo.IsExist << " \n\t\t\t IsValid         =  " << m_eventCacheInfoList.busRespCacheLineInfo.IsValid << std::endl;
        std::cout << "\t\t\t CurrState       = " << PrintPrivStateName(m_currEventCurrState) << std::endl;
        std::cout << "\t\t\t NextState       = " << PrintPrivStateName(m_currEventNextState) << std::endl;
        std::cout << "\t\t\t Ctrl ReqAction  = " << PrintPrivActionName(m_ctrlAction) << std::endl;
        std::cout << "\t\t\t Ctrl ReqTrans   = " << PrintTransName(m_currEventTrans2Issue) << std::endl;
      }


    }

    // dynamic binding
    std::string SNOOPPrivCohProtocol::PrintPrivStateName (int state) {
      IFCohProtocol *obj;
      switch (m_pType) {
        case CohProtType::SNOOP_PMSI: 
          obj = new(PMSI);
          return obj->PrivStateName (state);
        case CohProtType::SNOOP_MSI:
          obj = new(MSI);
          return obj->PrivStateName (state);
        default:
          std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
          exit(0);
      }
    }

     std::string SNOOPPrivCohProtocol::PrintPrivEventType (SNOOPPrivEventType event)
     {
       switch (event) {
         case SNOOPPrivEventType::Core:
           return " SNOOPPrivCohProtocol: Core Event is currently processed "; 
         case SNOOPPrivEventType::ReqBus:
           return " SNOOPPrivCohProtocol: ReqBus Event is currently processed "; 
         case SNOOPPrivEventType::RespBus:
           return " SNOOPPrivCohProtocol: RespBus Event is currently processed "; 
         case SNOOPPrivEventType::Null:
           return " SNOOPPrivCohProtocol: No Events currently processed "; 
         default:
           return " SNOOPPrivCohProtocol: Unknown Event!!!!";
       }
     }

     std::string SNOOPPrivCohProtocol::PrintPrivActionName (SNOOPPrivCtrlAction action)
     {
       switch (action) {
         case SNOOPPrivCtrlAction::Stall:
           return " Stall"; 
         case SNOOPPrivCtrlAction::Hit:
           return " Hit";
         case SNOOPPrivCtrlAction::issueTrans:
           return " issueTrans";
         case SNOOPPrivCtrlAction::ReissueTrans:
           return " ReissueTrans";
         case SNOOPPrivCtrlAction::issueTransSaveWbId:
           return " issueTransSaveWbId";
         case SNOOPPrivCtrlAction::WritBack:
           return " WritBack";
         case SNOOPPrivCtrlAction::CopyThenHit:
           return " CopyThenHit" ;
         case SNOOPPrivCtrlAction::CopyThenHitWB:
           return " CopyThenHitWB" ;
         case SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly:
           return " CopyThenHitSendCoreOnly" ;
         case SNOOPPrivCtrlAction::CopyThenHitSendMemOnly:
           return " CopyThenHitSendMemOnly" ;
         case SNOOPPrivCtrlAction::CopyThenHitSendCoreMem:
           return " CopyThenHitSendCoreMem" ;
         case SNOOPPrivCtrlAction::SaveWbCoreId:
           return " SaveWbCoreId" ;
         case SNOOPPrivCtrlAction::SendMemOnly:
           return " SendMemOnly" ;
         case SNOOPPrivCtrlAction::SendCoreOnly:
           return " SendCoreOnly" ;
         case SNOOPPrivCtrlAction::SendCoreMem:
           return " SendCoreMem" ;
         case SNOOPPrivCtrlAction::Fault:
           return " Fault" ;
         case SNOOPPrivCtrlAction::NoAck:
           return " NoAck";
         case SNOOPPrivCtrlAction::NullAck:
           return " NullAck";
         case SNOOPPrivCtrlAction::ProcessedAck:
           return " ProcessedAck";
         default: 
           return " Unknow Action";
       }
     }

     std::string SNOOPPrivCohProtocol::PrintTransName (SNOOPPrivCohTrans trans)
     {
       switch (trans) {
         case GetSTrans:
           return " GetSTrans "; 
         case GetMTrans:
           return " GetMTrans ";
         case UpgTrans:
           return " UpgTrans ";
         case PutMTrans:
           return " PutMTrans ";
         case PutSTrans:
           return " PutSTrans " ;
         case NullTrans:
           return " NullTrans ";
         default: // NullTrans:
           return " Unknow Trans";
       }
     }

    // Get ReqBus transaction to be
    // Sent.    
    SNOOPPrivCohTrans SNOOPPrivCohProtocol::GetCohTrans () { return m_currEventTrans2Issue;};




}

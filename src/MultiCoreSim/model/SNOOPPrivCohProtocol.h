/*
 * File  :      SNOOPPrivCohProtocol.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 29, 2020
 */

#ifndef _SNOOPPrivCohProtocol_H
#define _SNOOPPrivCohProtocol_H


#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

#include "CohProtocolCommon.h"

namespace ns3 { 
  // SNOOP Coherence Protocol Class
  class SNOOPPrivCohProtocol: public ns3::Object {
  private:
    CohProtType                                 m_pType;
    int                                         m_coreId;
    int                                         m_sharedMemId;
    bool                                        m_cache2Cache;
    Ptr<GenericCache>                           m_privCache;
    Ptr<CpuFIFO>                                m_cpuFIFO;
    Ptr<BusIfFIFO>                              m_busIfFIFO;
    SNOOPPrivEventList                          m_eventList;
    SNOOPPrivMsgList                            m_msgList;
    Ptr<GenericFIFO <CpuFIFO::ReqMsg >>         m_cpuPendingFIFO;
    SNOOPPrivEventsCacheInfo                    m_eventCacheInfoList;
    int                                         m_currEventCurrState;
    int                                         m_currEventNextState;
    SNOOPPrivCohTrans                           m_currEventTrans2Issue;
    SNOOPPrivEventType                          m_processEvent;
    SNOOPPrivCtrlAction                         m_ctrlAction;
    int                                         m_prllActionCnt;
    int                                         m_reqWbRatio;
    int                                         m_maxPendingReq;
    int                                         m_pendingCpuReq;
    ReplcPolicy                                 m_replcPolicy;
    Ptr<UniformRandomVariable>                  uRnd1;
    bool                                        m_logFileGenEnable;
  public:
    // Override TypeId.
    static TypeId GetTypeId(void);

    // Constructor
    SNOOPPrivCohProtocol();

    ~SNOOPPrivCohProtocol();
     
    // Set Log File Enable Flag
    void SetLogFileGenEnable (bool logFileGenEnable);

    // Set associated core Id
    void SetCoreId (int coreId);

    // Set Associated SharedMemId
    void SetSharedMemId (int sharedMemId);

    // Set Coherence Protocol Type
    void SetProtocolType (CohProtType ptype);

    // Set Maximum number of Pending CPU Request (OOO)
    void SetMaxPendingReq (int maxPendingReq);

    // Set Pending CPU FIFI depth
    void SetPendingCpuFIFODepth (int size);

    // Get Coherence Protocol Type
    CohProtType GetProtocolType ();    

    // Set Pointer to Private Cache
    void SetPrivCachePtr (Ptr<GenericCache> privCache);

    // Set Pointer for Cpu FIFO buffer
    void SetCpuFIFOPtr (Ptr<CpuFIFO> cpuFIFO);

    // Set Pointer for Buf IF FIFO buffer
    void SetBusFIFOPtr (Ptr<BusIfFIFO> busFIFO);

    // Set Cache-to-Cache Communication Flag
    void SetCache2Cache (bool cache2Cache);

    // Set Ratio Between Process CoreEvents and WriteBack Events
    void SetReqWbRatio (int reqWbRatio);

    // Get Current Process CPU Msg
    CpuFIFO::ReqMsg GetCpuReqMsg ();

    // Get Current Process ReqBus Msg
    BusIfFIFO::BusReqMsg GetBusReqMsg ();

    // Get Current Process RespBus Msg
    BusIfFIFO::BusRespMsg GetBusRespMsg ();

    // Get Current Process CPU Event
    SNOOPPrivCoreEvent GetCpuReqEvent ();

    // Get Current Process ReqBus Event
    SNOOPPrivReqBusEvent GetBusReqEvent ();

    // Get Current Process RespBus Event
    SNOOPPrivRespBusEvent GetBusRespEvent ();


    // Initialize Cache Lines State
    void InitializeCacheStates ();

    // Check Core Events 
    void ChkCoreEvent ();

    // Check Request Bus Events
    void ChkReqBusEvent();

    // Check Response Bus Events
    void ChkRespBusEvent ();

    // Chk and Update Coherence
    // Protocol Event List
    void ChkCohEvents ();

    // Chk Private Cache State 
    // Corresponding to each 
    // Event
    void GetEventsCacheInfo ();
    
    // Generate Coherence Protocol
    // Action lists
    void UpdateCohActionList ();

    // Event's Serializations
    void CohEventsSerialize ();

    // Process Core Event
    void ProcessCoreEvent ();

    // Process Req/Resp Events
    void ProcessBusEvents ();

    // Call Coherence Protocol FSM 
    void CohProtocolFSMProcessing ();

    // Get ReqBus transaction to be
    // Sent.    
    SNOOPPrivCohTrans GetCohTrans ();

    // dynamic binding
    SNOOPPrivEventPriority PrivEventPriorityBinding (IFCohProtocol *obj);
    void CohProtocolFSMBinding (IFCohProtocol *obj);
    std::string PrintPrivStateName (int state);
    void PrintEventInfo ();
    // debug print
    std::string PrintPrivEventType  (SNOOPPrivEventType event);

    std::string PrintPrivActionName (SNOOPPrivCtrlAction action);

    std::string PrintTransName (SNOOPPrivCohTrans trans);

  }; // class CohProtocol

 
}

#endif /* _SNOOPPrivCohProtocol_H */


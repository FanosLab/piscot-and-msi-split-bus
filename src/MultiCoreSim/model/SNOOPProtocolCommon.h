/*
 * File  :      SNOOPProtocolCommon.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 29, 2020
 */


#ifndef _SNOOPProtocolCommon_H
#define _SNOOPProtocolCommon_H


#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

#include "MemTemplate.h"
#include "GenericCache.h"

namespace ns3 { 

  // List of supported Coherence
  // Protocols (update this list
  // only if a new protocol is added. 
  enum CohProtType {
    SNOOP_PMSI = 0x000, 
    SNOOP_MSI  = 0x100
  };

  // Coherence Transactions to be
  // issued by Private Cache Ctrl 
  // on the Request Bus 
  enum SNOOPPrivCohTrans {
    GetSTrans = 0,
    GetMTrans,
    PutMTrans,
    PutSTrans,
    UpgTrans,
    NullTrans
  };

   // SNOOPING Event Types
   enum class SNOOPPrivEventType {
     Core    = 0,
     ReqBus,
     RespBus,
     Null
   };

  // List of core Events to monitor by
  // Private Cache Ctrl 
  enum class SNOOPPrivCoreEvent {
    Load = 0,
    Store,
    Replacement,
    Null  // if there is no request issued in this cycle
  };

  // List of Req Bus Events to be monitor
  // by private cache controller in snooping 
  // based coherence. These Events are decoded from 
  // transaction received on the Rx-Req bus
  enum class SNOOPPrivReqBusEvent {
    OwnGetS = 0,
    OwnGetM,
    OwnPutM,
    OwnPutS,
    OwnUpg,
    OtherGetS,
    OtherPutS,
    OtherGetM,
    OtherPutM, 
    OtherUpg,       
    Null
  };

  // List of the Response Bus Events
  // to be monitor by Private/Shared Cache 
  // Controllers in snooping based
  // coherence protocol.
  enum class SNOOPPrivRespBusEvent {
    OwnDataResp = 0,      
    OtherDataResp,
    Null
  };

  // Private Cache Controller
  // Action lists generated
  // by coherence protocol
   enum class SNOOPPrivCtrlAction {
       Stall = 0,                    // Stall this event, this happens when multiple requests to the same cache line
       Hit,                          // Do load/store hit 
       issueTrans,                   // Issue coherence Msg on the Bus 
       ReissueTrans,                 // Cancel previous coherence transaction and replace it with new one (PMSI- SM_w)
       issueTransSaveWbId,           // Issue transaction (i.e. PUTM) 
       WritBack,                     // Do write back of the requested cache line on the response bus.
       CopyThenHit,                  // Copy response data into cache buffer, then do hit load/store for the request core command. 
       CopyThenHitWB,                // Same as "CopyThenHit", pls the core needs to write back the data on the response bus.
       CopyThenHitSendCoreOnly,      // Same as "CopyThenHit", pls the core needs to write back the data to requestor core only.
       CopyThenHitSendMemOnly,       // Same as "CopyThenHit", pls the core needs to write back the data to shared memory only.
       CopyThenHitSendCoreMem,       // Same as "CopyThenHit", pls the core needs to write back the data to requestor core pls shared memory. 
       SaveWbCoreId,                 // Save requestor core ID.
       SendMemOnly,                  // Send Data to Shared memory
       SendCoreOnly,                 // Send Data to Requestor core 
       SendCoreMem,                  // Send Data to both Requestor core and shared memory
       Fault,                        // Coherence Protocol generated Fault action, the program should stop and debug the issue.
       NoAck,                        // No action is required except removing the message from the received FIFO and update cache coherence state.
       NullAck,                      // Null event, sleep the controller
       ProcessedAck                  // Processed action Flag asserted by controller when event get processed. 
   };

  // Snoop List of Events
  struct SNOOPPrivEventList {
    SNOOPPrivCoreEvent    cpuReqEvent;
    SNOOPPrivReqBusEvent  busReqEvent;
    SNOOPPrivRespBusEvent busRespEvent;
  };

  // Cache States corresponding to each
  // Event
  struct SNOOPPrivEventsCacheInfo {
    GenericCache::CacheLineInfo cpuReqCacheLineInfo;
    GenericCache::CacheLineInfo busReqCacheLineInfo;
    GenericCache::CacheLineInfo busRespCacheLineInfo;
  };

  // Snoop List of Events
  struct SNOOPPrivStateList {
    int  cpuEventState;
    int  busReqEventState;
    int  busRespEventState;
  };

  // Execution priority of snooping Events
  enum class SNOOPPrivEventPriority {
    ReqBus,
    RespBus,
    WorkConserv,
  };

  // Snoop Bus List of Bus & CPU
  // Messages
  struct SNOOPPrivMsgList {
    CpuFIFO  ::ReqMsg      cpuReqMsg = {};
    BusIfFIFO::BusReqMsg   busReqMsg = {};
    BusIfFIFO::BusRespMsg  busRespMsg= {};
  };

  // List of request bus events 
  // to be monitor by shared memory 
  // controller in snooping based
  // coherence protocol.
  enum class SNOOPSharedReqBusEvent {
    GetS = 0,
    GetM,
    Upg,
    OwnerPutM,
    OTherPutM,
    PutS,
    Null  
  };

  // List of the Response Bus Events
  // to be monitor by Private/Shared Cache 
  // Controllers in snooping based
  // coherence protocol.
  enum class SNOOPSharedRespBusEvent {
    OWnDataResp = 0,      
    OTherDataResp,
    NUll
  };

  enum class SNOOPSharedOwnerState {
    SharedMem = 0,
    OtherCore,
    Preserve
  };

   enum class SNOOPSharedEventType {
     ReqBus = 0,
     RespBus
   };

  // Snoop List of Events
  struct SNOOPSharedEventList {
    SNOOPSharedReqBusEvent  busReqEvent;
    SNOOPSharedRespBusEvent busRespEvent;
  };

  // Cache States corresponding to each
  // Event
  struct SNOOPSharedEventsCacheInfo {
    GenericCache::CacheLineInfo busReqCacheLineInfo;
    GenericCache::CacheLineInfo busRespCacheLineInfo;
  };

  // Snoop List of Events
  struct SNOOPSharedStateList {
    int  busReqEventState;
    int  busRespEventState;
  };

  // Snoop Bus List of Bus & CPU
  // Messages
  struct SNOOPSharedMsgList {
    BusIfFIFO::BusReqMsg   busReqMsg = {};
    BusIfFIFO::BusRespMsg  busRespMsg= {};
  };


  // Shared Cache Controller
  // Action lists generated
  // by coherence protocol
  enum class SNOOPSharedCtrlAction {
    Stall = 0,
    UpdateState,
    SendData,
    StoreData,
    SaveReqCoreId,
    SendPendingReq,
    Fault,
    NullAck,
  };


}

#endif /* _SNOOPProtocolCommon_H */

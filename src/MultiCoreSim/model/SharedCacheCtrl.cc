/*
 * File  :      SharedCacheCtrl.cc
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 20, 2020
 */

#include "SharedCacheCtrl.h"

namespace ns3 {

    // override ns3 type
    TypeId SharedCacheCtrl::GetTypeId(void) {
        static TypeId tid = TypeId("ns3::SharedCacheCtrl")
               .SetParent<Object > ();
        return tid;
    }

    SharedCacheCtrl::SharedCacheCtrl(uint32_t cachLines, 
                     Ptr<BusIfFIFO> assoicateBusIfFIFO) {
        // default
        m_cacheType    = 0;
        m_cacheSize    = 64*256;
        m_cacheBlkSize = 64;
        m_nways        = 1;
        m_nsets        = 256;
        m_coreId       = 1;
        m_dt           = (1.0/1000000);
        m_clkSkew      = 0;
        m_cacheCycle   = 1;
        m_cach2Cache   = false;
        m_nPrivCores   = 4;
        m_logFileGenEnable = false;
        m_cache        = CreateObject<GenericCache> (cachLines);
        m_busIfFIFO    = assoicateBusIfFIFO;
        m_ownerCoreId  = new uint16_t[cachLines];
    }

    // We don't do any dynamic allocations
    SharedCacheCtrl::~SharedCacheCtrl() {
    }

    void SharedCacheCtrl::SetCacheSize (uint32_t cacheSize) {
      m_cacheSize = cacheSize;
      m_cache->SetCacheSize(cacheSize);
    }

    uint32_t SharedCacheCtrl::GetCacheSize () {
      return m_cacheSize;
    }

    void SharedCacheCtrl::SetCacheBlkSize (uint32_t cacheBlkSize) {
      m_cacheBlkSize = cacheBlkSize;
      m_cache->SetCacheBlkSize(cacheBlkSize);
    }

    uint32_t SharedCacheCtrl::GetCacheBlkSize () {
      return m_cacheBlkSize;
    }

    void SharedCacheCtrl::SetCacheNways (uint32_t nways) {
      m_nways = nways;
      m_cache->SetCacheNways(nways);
    }

    uint32_t SharedCacheCtrl::GetCacheNways () {
      return m_nways;
    }

    void SharedCacheCtrl::SetCacheNsets (uint32_t nsets) {
      m_nsets = nsets;
      m_cache->SetCacheNsets(nsets);
    }

    uint32_t SharedCacheCtrl::GetCacheNsets () {
      return m_nsets;
    }

    void SharedCacheCtrl::SetCacheType (uint16_t cacheType) {
      m_cacheType = cacheType;
      m_cache->SetCacheType(cacheType);
    }

    uint16_t SharedCacheCtrl::GetCacheType () {
      return m_cacheType;
    }

    void SharedCacheCtrl::SetCoreId (int coreId) {
      m_coreId = coreId;
    }

    int SharedCacheCtrl::GetCoreId () {
      return m_coreId;
    }

    void SharedCacheCtrl::SetDt (double dt) {
      m_dt = dt;
    }

    int SharedCacheCtrl::GetDt () {
      return m_dt;
    }

    void SharedCacheCtrl::SetClkSkew (double clkSkew) {
       m_clkSkew = clkSkew;
    }

    void SharedCacheCtrl::SetCache2Cache (bool cach2Cache) {
       m_cach2Cache = cach2Cache;
    }

    void SharedCacheCtrl::SetBMsPath  (std::string bmsPath) {
      m_bmsPath = bmsPath;
    }

    void SharedCacheCtrl::SetNumPrivCore (int nPrivCores) {
      m_nPrivCores = nPrivCores;
    }

    void SharedCacheCtrl::SetLogFileGenEnable (bool logFileGenEnable ) {
      m_logFileGenEnable = logFileGenEnable;
    }

     // Check BusIfFIFO content and return the corresponding 
     // ReqBusEvent of next in queue message 
     SNOOPSharedReqBusEvent SharedCacheCtrl::ChkBusRxReqEvent  (Ptr<BusIfFIFO> associatedBusIfFIFO,
                                                                BusIfFIFO::BusReqMsg &  busReqMsg) {  
       if (!associatedBusIfFIFO->m_rxMsgFIFO.IsEmpty()) {

         SNOOPSharedReqBusEvent reqBusEvent;
         busReqMsg = associatedBusIfFIFO->m_rxMsgFIFO.GetFrontElement();

         GenericCacheFrmt busReqCacheLine;
         uint32_t  busReqCacheWayIdx;
         FetchLine(busReqMsg.addr, busReqCacheLine, busReqCacheWayIdx);

         uint16_t wbCoreId   = busReqMsg.wbCoreId;
         uint16_t ownerCoreId = m_ownerCoreId[busReqCacheWayIdx];

         switch (busReqMsg.cohrMsgId) {
           case SNOOPPrivCohTrans::GetSTrans:
             reqBusEvent = SNOOPSharedReqBusEvent::GetS;
             break;
           case SNOOPPrivCohTrans::GetMTrans:
             reqBusEvent = SNOOPSharedReqBusEvent::GetM;
             break;
           case SNOOPPrivCohTrans::UpgTrans :
             reqBusEvent = SNOOPSharedReqBusEvent::Upg; 
             break;
           case SNOOPPrivCohTrans::PutMTrans:
             //std::cout << " OwnerId = " << ownerCoreId << " requestWbCoreId " << wbCoreId << std::endl;
             reqBusEvent = (wbCoreId == ownerCoreId) ? 
                            SNOOPSharedReqBusEvent::OwnerPutM : 
                            SNOOPSharedReqBusEvent::OTherPutM ;
             break;
           case SNOOPPrivCohTrans::PutSTrans:
             reqBusEvent = SNOOPSharedReqBusEvent::PutS;
             break;
           default: // Invalid Transaction
             if (m_logFileGenEnable) {
               std::cout << "SharedMem: [Error] invalid Transaction detected on the Bus" << std::endl;
             }
             exit(0);
           }
         return reqBusEvent;
       }
       else {// CoreNullEvent
         return SNOOPSharedReqBusEvent::Null;
       }
     }

     // Check BusIfFIFO content and return the corresponding 
     // RespBusEvent of next in queue message 
     SNOOPSharedRespBusEvent SharedCacheCtrl::ChkBusRxRespEvent (Ptr<BusIfFIFO> associatedBusIfFIFO,
                                                                  BusIfFIFO::BusRespMsg & busRespMsg){

       if (!associatedBusIfFIFO->m_rxRespFIFO.IsEmpty()) {
         busRespMsg = associatedBusIfFIFO->m_rxRespFIFO.GetFrontElement();
         uint16_t respCoreId   = busRespMsg.respCoreId;
         return (respCoreId == m_coreId) ? SNOOPSharedRespBusEvent::OWnDataResp :
                                           SNOOPSharedRespBusEvent::OTherDataResp;
       }
       else {
         return SNOOPSharedRespBusEvent::NUll;
       }
     }

     // update private cache state or complete line
     void SharedCacheCtrl::UpdateSharedCache   (CacheField field, 
                                                uint64_t addr, 
                                                int state, 
                                                uint8_t * data) {

       GenericCacheFrmt cacheLine;
       uint32_t LineWayIdx;
       if (FetchLine(addr, cacheLine, LineWayIdx)) {
         if (field == State) {
           m_cache->SetCacheLineState (LineWayIdx, state);
         }
         else if (field == Line) {
           cacheLine.state  = state;
           cacheLine.tag    = m_cache->CpuAddrMap (addr).tag;
           for (int i = 0; i < 8; i++)
             cacheLine.data[i] = data[i];
           m_cache->WriteCacheLine(cacheLine, LineWayIdx);
         }
       }
       else {
         if (m_logFileGenEnable) {
           std::cout << "SharedMem: [Warning] Cannot find the block in shared cache!" << std::endl;
         }
         exit(0);
       }
     }

     bool SharedCacheCtrl::FetchLine (uint64_t addr, GenericCacheFrmt  & cacheLine, uint32_t & LineWayIdx) {
       GenericCacheMapFrmt addrMap   = m_cache->CpuAddrMap (addr);
       uint32_t setIdx = addrMap.idx_set;
       uint32_t nWays  = m_cache -> GetCacheNways();
       uint32_t setOfst = setIdx * nWays;

       for (uint32_t wayIdx = setOfst; wayIdx < setOfst+nWays;wayIdx++) {
         cacheLine    = m_cache->ReadCacheLine(wayIdx);
         LineWayIdx   = wayIdx;
         //std::cout << "wayIdx = " << wayIdx << " valid = " << cacheLine.valid << " tagstore "  << cacheLine.tag << " reqtag " << addrMap.tag << std::endl;
         if (cacheLine.valid == true && cacheLine.tag == addrMap.tag) {
           return true;
         }
       }
       return false;
     }

     // execute write back or sent command
     bool SharedCacheCtrl::DoWriteBack (uint64_t addr, uint16_t wbCoreId, uint64_t msgId, double timestamp, bool PushFrontFlag = false) {
       if (!m_busIfFIFO->m_txRespFIFO.IsFull()) {
         uint32_t wayIdx;
         GenericCacheFrmt wbLine;
         if (FetchLine(addr, wbLine, wayIdx)) {
           BusIfFIFO::BusRespMsg  wbMsg;
           wbMsg.reqCoreId    = wbCoreId;
           wbMsg.respCoreId   = m_coreId;
           wbMsg.addr         = addr;
           wbMsg.msgId        = msgId;
           wbMsg.timestamp    = timestamp;
           for (int i = 0; i < 8; i++)
             wbMsg.data[i] = wbLine.data[i];

           // (1) Implementation 1: push message into BusTxMsg FIFO
           m_busIfFIFO->m_txRespFIFO.InsertElement(wbMsg);

             //std::cout << "SharedMem: wb to core Id = " << wbMsg.reqCoreId << ", addr = " << wbMsg.addr <<  std::endl;

           // (2) Implementation 1: check where to push message inside buffer
           // First come First serve implementation go here
           /*
           bool InsertionDone = false;
           int pendingQueueSize = m_busIfFIFO->m_txRespFIFO.GetQueueSize();
           BusIfFIFO::BusRespMsg pendingWbMsg ;
           std::cout << "SharedMem: Pending Wb Msg CoreId = " <<  wbMsg.reqCoreId << ", TimeStamp =" << wbMsg.timestamp << std::endl;
           std::cout << "SharedMem: pendingResp Queue size = " << pendingQueueSize << std::endl;
           for (int i = 0; i < pendingQueueSize ;i++) {
             pendingWbMsg = m_busIfFIFO->m_txRespFIFO.GetFrontElement();
             std::cout << "SharedMem: pendingResp core Id = " << pendingWbMsg.reqCoreId << ", addr = " << pendingWbMsg.addr <<  ", timestamp = " << pendingWbMsg.timestamp << std::endl;
             // Remove message from the busResp buffer
             m_busIfFIFO->m_txRespFIFO.PopElement();
             if (pendingWbMsg.timestamp >  wbMsg.timestamp && InsertionDone == false) {
               InsertionDone = true;
               m_busIfFIFO->m_txRespFIFO.InsertElement(wbMsg);
               m_busIfFIFO->m_txRespFIFO.InsertElement(pendingWbMsg);
               std::cout << "SharedMem: Pending WB Msg for core " << wbMsg.reqCoreId << " inserted before CoreId " << pendingWbMsg.reqCoreId << std::endl;
             }
             else {
               // b.3.1) Dequeue the data again into pending buffer
               m_busIfFIFO->m_txRespFIFO.InsertElement(pendingWbMsg);
             }
           }

           if (InsertionDone == false) {
             m_busIfFIFO->m_txRespFIFO.InsertElement(wbMsg);
           }
           std::cout << "SharedMem: pendingResp Queue size after Insertion= " << m_busIfFIFO->m_txRespFIFO.GetQueueSize() << std::endl;
           */

           return true;
         }
         else {
           if (m_logFileGenEnable) {
             std::cout << "SharedMem: [Warning] Cannot find the block in shared cache!" << std::endl;
           }
           return false;
         }
       }
       else {
         if (m_logFileGenEnable) {
           std::cout << "SharedMem: [Warning] Cannot insert the Msg into BusTxResp FIFO, FIFO is Full!" << std::endl;
         }
         return false;
       }
     }

     // insert new Transaction into BusTxMsg "pending" WB FIFO 
     bool SharedCacheCtrl::PushMsgInBusTxFIFO  (uint64_t       msgId, 
                                                uint16_t       reqCoreId, 
                                                uint16_t       wbCoreId, 
                                                uint64_t       addr) {
       if (!m_busIfFIFO->m_txMsgFIFO.IsFull()) {
         BusIfFIFO::BusReqMsg tempBusReqMsg;
         tempBusReqMsg.msgId        = msgId;
         tempBusReqMsg.reqCoreId    = reqCoreId;
         tempBusReqMsg.wbCoreId     = wbCoreId;
         tempBusReqMsg.addr         = addr;
         tempBusReqMsg.timestamp    = m_cacheCycle*m_dt;
         // push message into BusTxMsg FIFO
         m_busIfFIFO->m_txMsgFIFO.InsertElement(tempBusReqMsg);
         return true;
       }
       else {
         if (m_logFileGenEnable) {
           std::cout << "SharedMem: [Warning] Cannot insert the Msg in BusTxMsg FIFO, FIFO is Full!" << std::endl;
         }
         return false;
       }
     }

     // process pending buffer
     void SharedCacheCtrl::SendPendingReqData  (GenericCacheMapFrmt recvTrans ) {
       // check if there is a pending write-back to this line in the pending buffer
       if (!m_busIfFIFO->m_txMsgFIFO.IsEmpty()) {
          BusIfFIFO::BusReqMsg pendingWbMsg ;
          GenericCacheMapFrmt  pendingWbAddrMap;
          int pendingQueueSize = m_busIfFIFO->m_txMsgFIFO.GetQueueSize();
          //std::cout << "SharedMem: pendingWb Queue size = " << pendingQueueSize << std::endl;
          for (int i = 0; i < pendingQueueSize ;i++) {
            pendingWbMsg = m_busIfFIFO->m_txMsgFIFO.GetFrontElement();
            pendingWbAddrMap = m_cache->CpuAddrMap (pendingWbMsg.addr);
            if (m_logFileGenEnable) {
              std::cout << "SharedMem: pendingWb Entery = " << i << ", core Id = " << pendingWbMsg.reqCoreId << ", addr = " << pendingWbMsg.addr << std::endl;
            }
            // Remove message from the busReq buffer
            m_busIfFIFO->m_txMsgFIFO.PopElement();
            if (recvTrans.idx_set == pendingWbAddrMap.idx_set &&
                recvTrans.tag == pendingWbAddrMap.tag) {

              // b.3.2)send data to requestors
              if (m_logFileGenEnable) {
                std::cout << "SharedMem: PendingWb Entery = " << i << ", Core Id = " << pendingWbMsg.reqCoreId <<  " get inserted into SharedMem RespBuf" << std::endl;
              }

              if (!DoWriteBack (pendingWbMsg.addr,pendingWbMsg.reqCoreId,pendingWbMsg.msgId,pendingWbMsg.timestamp,true)) {
                if (m_logFileGenEnable){
                  std::cout << "SharedMem: This is will cause stall in the PMSI state machine !!!!" << std::endl;
                }
                exit(0);
              }
            }
            else {
              // b.3.1) Dequeue the data again into pending buffer
              m_busIfFIFO->m_txMsgFIFO.InsertElement(pendingWbMsg);
            }
          }
        } 
     }

     // This function does most of the functionality.
     void SharedCacheCtrl::CacheCtrlMain () {
       // Generate Cache Controller requested messages
       // and its corresponding Events if exists
       SNOOPSharedReqBusEvent  busReqEvent;
       SNOOPSharedRespBusEvent busRespEvent;

       BusIfFIFO::BusReqMsg   busReqMsg = {};
       BusIfFIFO::BusRespMsg  busRespMsg= {};

       busReqEvent  = ChkBusRxReqEvent  (m_busIfFIFO, busReqMsg );
       busRespEvent = ChkBusRxRespEvent (m_busIfFIFO, busRespMsg);      

       // Next, Generate PMSI next action/state for each type of Events
       PMSI::PMSISharedAction busReqAction;  
       PMSI::PMSISharedAction busRespAction;

       int                         busReqCacheNextState,
                                   busReqCacheCurrState,
                                   busRespCacheNextState,
                                   busRespCacheCurrState;

       PMSI::PMSIOwnerState        busReqCacheOwnerNextState,
                                   busReqCacheOwnerCurrState,
                                   busRespCacheOwnerNextState,
                                   busRespCacheOwnerCurrState;

       GenericCacheFrmt                             busReqCacheLine,
                                                    busrespCacheLine;

       uint32_t  busReqCacheWayIdx,
                 busRespCacheWayIdx;

       // get current state first
       if (!FetchLine(busReqMsg.addr, busReqCacheLine, busReqCacheWayIdx)) {
         if (busReqEvent != SNOOPSharedReqBusEvent::Null) {
           if (m_logFileGenEnable){
             std::cout << "SharedMem: [Error] Request Cache Line not exist!" << std::endl;
           }
           exit(0);
         }
       }

       if (!FetchLine(busRespMsg.addr, busrespCacheLine, busRespCacheWayIdx)) {
         if (busRespEvent != SNOOPSharedRespBusEvent::NUll) {
           if (m_logFileGenEnable){
             std::cout << "SharedMem: [Error] Response Cache Line not exist!" << std::endl;
           }
           exit(0);
         }
       }

       busReqCacheCurrState  = m_cache->GetCacheLineState(busReqCacheWayIdx );
       busRespCacheCurrState = m_cache->GetCacheLineState(busRespCacheWayIdx);
 
       // get owner core state
       busReqCacheOwnerCurrState  = (m_ownerCoreId[busReqCacheWayIdx] == m_coreId) ? 
                                     PMSI::SharedMem : PMSI::OtherCore;

       busRespCacheOwnerCurrState = (m_ownerCoreId[busRespCacheWayIdx] == m_coreId) ? 
                                     PMSI::SharedMem : PMSI::OtherCore;

       // set next state to current state before PMSI call, this is due
       // the bidirection design of the SharedCacheEventProcessing fucntion
       busReqCacheNextState  = busReqCacheCurrState;
       busRespCacheNextState = busRespCacheCurrState;

       busReqCacheOwnerNextState  = busReqCacheOwnerCurrState;
       busRespCacheOwnerNextState = busRespCacheOwnerCurrState;

       // Call PMSI protocol to get next action/state
       SNOOPSharedEventType eventType = SNOOPSharedEventType::ReqBus;
       MSI::SharedCacheEventProcessing (eventType,    m_cach2Cache,         busReqEvent, 
                                         busRespEvent, busReqCacheNextState, busReqCacheOwnerNextState, 
                                         busReqAction);

       eventType = SNOOPSharedEventType::RespBus;
       MSI::SharedCacheEventProcessing (eventType,    m_cach2Cache,          busReqEvent, 
                                         busRespEvent, busRespCacheNextState, busRespCacheOwnerNextState, 
                                         busRespAction);

      GenericCacheMapFrmt respAddrMap   = m_cache->CpuAddrMap (busRespMsg.addr);
      GenericCacheMapFrmt busAddrMap   = m_cache->CpuAddrMap (busReqMsg.addr);
/*
      // OOO handler
       int ReqQueueSize = m_busIfFIFO->m_rxMsgFIFO.GetQueueSize();
       bool NewFetchDone = false;
       eventType = SNOOPSharedEventType::ReqBus;
       std::cout << "SharedMem RxReq Size " << ReqQueueSize << std::endl;

       BusIfFIFO::BusReqMsg   BusReqMsgTemp = {};
       for (int i = 0; i < ReqQueueSize; i++ ) {

         busReqEvent  = ChkBusRxReqEvent  (m_busIfFIFO, busReqMsg );
         m_busIfFIFO->m_rxMsgFIFO.PopElement();

         if (NewFetchDone == false) {
            BusReqMsgTemp = busReqMsg;
           // get current state first
           if (!FetchLine(busReqMsg.addr, busReqCacheLine, busReqCacheWayIdx)) {
             if (busReqEvent != SNOOPSharedReqBusEvent::Null) {
               if (m_logFileGenEnable){
                 std::cout << "SharedMem: [Error] Request Cache Line not exist!" << std::endl;
               }
               exit(0);
             }
           }

           std::cout << "SharedMem: Req entry " << i << " coreId " << busReqMsg.reqCoreId << " addr " << busReqMsg.addr << std::endl;

           busReqCacheCurrState  = m_cache->GetCacheLineState(busReqCacheWayIdx );
           // get owner core state
           busReqCacheOwnerCurrState  = (m_ownerCoreId[busReqCacheWayIdx] == m_coreId) ? 
                                         PMSI::SharedMem : PMSI::OtherCore;

           busReqCacheNextState  = busReqCacheCurrState;
           busReqCacheOwnerNextState  = busReqCacheOwnerCurrState;

           // Call PMSI protocol to get next action/state
           MSI::SharedCacheEventProcessing (eventType,    m_cach2Cache,         busReqEvent, 
                                            busRespEvent, busReqCacheNextState, busReqCacheOwnerNextState, 
                                            busReqAction);

           if (busReqAction != PMSI::SharedStall) {
             NewFetchDone = true;
           }
           else {
             m_busIfFIFO->m_rxMsgFIFO.InsertElement(busReqMsg);
           }
         }
         else {
           m_busIfFIFO->m_rxMsgFIFO.InsertElement(busReqMsg);
         }

       }
       busReqMsg    = BusReqMsgTemp;
       busAddrMap   = m_cache->CpuAddrMap (busReqMsg.addr);
*/

      if (busReqAction == PMSI::SharedStall) {

        busReqAction = PMSI::SharedNullAck;

      }

      if (m_logFileGenEnable){
        if (busReqAction != PMSI::SharedNullAck) {
          std::cout << "\nSharedMem: " << m_coreId << " has Msg in the RxReq Bus" << std::endl;
          std::cout << "\t\t BusEventName        = " << PMSI::SharedReqBusEventName(busReqEvent) << std::endl;
          std::cout << "\t\t ReqCoreId           = " << busReqMsg.reqCoreId << " RespCoreId = " << busReqMsg.wbCoreId 
                    << " Req Addr  = " << busReqMsg.addr << " CacheLine = " << busAddrMap.idx_set << std::endl;
          std::cout << "\t\t CacheLine CurrState = " << MSI::SharedStateName(busReqCacheCurrState) << std::endl;
          std::cout << "\t\t CacheLine NextState = " << MSI::SharedStateName(busReqCacheNextState) << std::endl;
          std::cout << "\t\t Ctrl ReqAction      = " << PMSI::SharedActionName(busReqAction) << std::endl;
        }

        if (busRespAction != PMSI::SharedNullAck) {
          std::cout << "\nSharedMem: " << m_coreId << " has Msg in the RxResp Bus" << std::endl;
          std::cout << "\t\t BusEventName        = " << PMSI::SharedRespBusEventName(busRespEvent) << std::endl;
          std::cout << "\t\t ReqCoreId           = " << busRespMsg.reqCoreId << " RespCoreId = " << busRespMsg.respCoreId 
                    << " Resp Addr  = " << busRespMsg.addr << " CacheLine = " << respAddrMap.idx_set << std::endl;
          std::cout << "\t\t CacheLine CurrState = " << MSI::SharedStateName(busRespCacheCurrState) << std::endl;
          std::cout << "\t\t CacheLine NextState = " << MSI::SharedStateName(busRespCacheNextState) << std::endl;
          std::cout << "\t\t Ctrl ReqAction      = " << PMSI::SharedActionName(busRespAction) << std::endl;
        }
      } 

       /******************************
        * First Step is to process any 
        * pending response message.
        ******************************/
        if (busRespAction != PMSI::SharedNullAck) {
          if (busRespAction == PMSI::StoreData || busRespAction == PMSI::StoreDataOnly) {
            // a.1) Remove message from the busResp buffer
            m_busIfFIFO->m_rxRespFIFO.PopElement();

            // a.2) Copy response data into LLC cache 
            UpdateSharedCache (Line, busRespMsg.addr, busRespCacheNextState, busRespMsg.data);

            // a.3) check if there is a pending write-back to this line in the pending buffer
            if (busRespAction != PMSI::StoreDataOnly) {
              SendPendingReqData(respAddrMap);
            }
          }
          else if (busRespAction == PMSI::SharedNoAck) {
            // intervension memory (i.e cache-to-cache)
            m_busIfFIFO->m_rxRespFIFO.PopElement();
          }
          else { // busRespAction == SharedFault
            if (m_logFileGenEnable){
              std::cout << "SharedCache: DataResp occur in illegal state!" << std::endl; 
            }   
            exit(0);     
          }

          // mark busResp Action as processed
          busRespAction = PMSI::SharedProcessedAck;

          // a.3) skip processing the other bus action
          busReqAction = PMSI::SharedNullAck;
        }

       /******************************
        * Second, is to process any 
        * pending message in BusReq FIFO
        ******************************/
        if (busReqAction != PMSI::SharedNullAck) {
          // initial filtering
          if (busReqAction == PMSI::SharedFault) {
            if (m_logFileGenEnable){
              std::cout << "SharedCache: DataResp occur in illegal state!" << std::endl;    
            }
            exit(0);     
          }
          else if (busReqAction == PMSI::SharedNoAck) {
            // b.1) remove no-action event
            m_busIfFIFO->m_rxMsgFIFO.PopElement();
          }
          else if (busReqAction == PMSI::SharedStall) {
           /* b.2) Postpone busReq untill the 
            * previous action get processed. 
            */ 
           //std::cout << "Info:SharedMem Stall Event generated" << std::endl;
          }
          else if (busReqAction == PMSI::SendData) {
            // b.3.1) Remove message from the busReq buffer
            m_busIfFIFO->m_rxMsgFIFO.PopElement();
            // b.3.2)send data to requestors
            if (!DoWriteBack (busReqMsg.addr,busReqMsg.reqCoreId,busReqMsg.msgId, m_cacheCycle*m_dt)) {
              if (m_logFileGenEnable){
                std::cout << "SharedMem: This is will cause stall in the PMSI state machine !!!!" << std::endl;
              }
              exit(0);
            }
            // b.3.3) check core owner
          }
          else if (busReqAction == PMSI::SaveReqCoreId) {
            m_busIfFIFO->m_rxMsgFIFO.PopElement();
            // save pending write back
            if (!PushMsgInBusTxFIFO (busReqMsg.msgId, busReqMsg.reqCoreId, m_coreId, busReqMsg.addr )) {
              if (m_logFileGenEnable){
                std::cout << "SharedMem: Pending buffer is full !!!!" << std::endl;
              }
              exit(0);
            }
          }
          else if (busReqAction == PMSI::SendPendingData) {
            SendPendingReqData(busAddrMap);
          }
          else { // unknown action
            if (m_logFileGenEnable){
              std::cout << "SharedMem: BusReq undefine action occur!" << std::endl;    
            }
            exit(0);     
          }

          // b.4) update state into LLC cache 
          UpdateSharedCache (State, busReqMsg.addr, busReqCacheNextState, NULL);

          // b.5) update owner core
          if (busReqCacheOwnerNextState == PMSI::SharedMem) {
            m_ownerCoreId[busReqCacheWayIdx] = m_coreId;
          }
          else if (busReqCacheOwnerNextState == PMSI::OtherCore) {
            m_ownerCoreId[busReqCacheWayIdx] = busReqMsg.reqCoreId;
          }

          // b.6) mark event as processed
          busReqAction = PMSI::SharedProcessedAck;
        }

     }


    void SharedCacheCtrl::CacheInitialize () {
      if (m_logFileGenEnable){
        std::cout << "\nSharedMem: [Info] Cache State, Owner Initialization" << std::endl; 
      }
       // initialize shared cache states
      //m_cache->InitalizeCacheStates(static_cast<int>(SNOOP_PMSISharedCacheState::IorS));
      m_cache->InitalizeCacheStates(static_cast<int>(SNOOP_MSISharedCacheState::IorS));
      // initialize core owner

      for (uint32_t i = 0; i < m_cacheSize/m_cacheBlkSize; i++) {
        m_ownerCoreId[i] = m_coreId;
      }

      // load memory content from benchmark files
      GenericCacheFrmt cacheLine;
      std::stringstream ss;
      std::string fline;
      std::ifstream m_bmTrace;
      size_t pos;
      std::string s;
      uint64_t addr;
      uint32_t setIdx;
      uint32_t nWays;
      uint32_t setOfst;
      bool     filled;
      for (int i = 0; i < m_nPrivCores; i++) {
        ss << m_bmsPath <<"/trace_C" << i << ".trc.shared";
        if (m_logFileGenEnable){
          std::cout << "\nSharedMem: [Info] Load benchmark file\n\t" << ss.str() << std::endl; 
        }
        m_bmTrace.open(ss.str());
        if(m_bmTrace.is_open()) {
          while (getline(m_bmTrace,fline)){
            pos = fline.find(" ");
            s = fline.substr(0, pos); 
            // convert hex string address to decimal 
            addr = (uint64_t) strtol(s.c_str(), NULL, 16);

            GenericCacheMapFrmt addrMap   = m_cache->CpuAddrMap (addr);
            setIdx = addrMap.idx_set;
            nWays  = m_cache -> GetCacheNways();
            setOfst = setIdx * nWays;
            filled = false;
            uint32_t wayIdx;
            //std::cout << "Load Shared Mem addr = " << addr << " setIdx = " << setIdx << " nways = " << nWays <<" setOfst = " <<  setOfst << std::endl;
            for (wayIdx = setOfst; wayIdx < setOfst+nWays;wayIdx++) {
              cacheLine    = m_cache->ReadCacheLine(wayIdx);
              if (cacheLine.valid == true && cacheLine.tag == addrMap.tag) {
                //std::cout << "SharedMem: [Info] Shared Cache Line exist\n"; 
                filled = true;
                //std::cout << "Load Shared Mem addr = " << addr << " tag = " << addrMap.tag << " setIdx = " << setIdx << " nways = " << nWays <<" wayIdx = " <<  wayIdx << std::endl;
                break;
              }
              else if (cacheLine.valid == false) {
                //std::cout << "Load Shared Mem addr = " << addr << " tag = " << addrMap.tag << " setIdx = " << setIdx << " nways = " << nWays <<" wayIdx = " <<  wayIdx << std::endl;
                cacheLine.valid = true;
                cacheLine.tag   = addrMap.tag;
                filled = true;
                m_cache->WriteCacheLine(cacheLine,wayIdx);
                break;
              }
            }
            if (filled == false) {
              if (m_logFileGenEnable){
                std::cout << "Load Shared Mem addr = " << addr << " tag = " << addrMap.tag << " setOfst = " << setOfst << " nways = " << nWays <<" wayIdx = " <<  wayIdx << std::endl;
                std::cout << "SharedMem: [Error] Shared Line cannot placed in cache" << std::endl;
              }
              exit(0);
            }
          }
          m_bmTrace.close();
          ss.str(std::string());
        }
        else {
          if (m_logFileGenEnable){
            std::cout<<"SharedMem: [Error] Benchmark file name "<< m_bmsPath << " is not exist!" << std::endl;
          }
          exit(0); // The program is terminated here
        } 
      } // for (int i = 0; i < m_nPrivCores; i++)
    }


    void SharedCacheCtrl::CycleProcess() {
       CacheCtrlMain();
      // Schedule the next run
      Simulator::Schedule(NanoSeconds(m_dt), &SharedCacheCtrl::Step, Ptr<SharedCacheCtrl > (this));
      m_cacheCycle++;
    }

    // The init function starts the controller at the beginning 
    void SharedCacheCtrl::init() {
        CacheInitialize ();
        Simulator::Schedule(NanoSeconds(m_clkSkew), &SharedCacheCtrl::Step, Ptr<SharedCacheCtrl > (this));
    }

    /**
     * Runs one mobility Step for the given vehicle generator.
     * This function is called each interval dt
     */

    void SharedCacheCtrl::Step(Ptr<SharedCacheCtrl> sharedCacheCtrl) {
        sharedCacheCtrl->CycleProcess();
    }

}

/*
 * File  :      SharedCacheCtrl.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 20, 2020
 */

#ifndef _SharedCacheCtrl_H
#define _SharedCacheCtrl_H

#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"
#include "MemTemplate.h"
#include "GenericCache.h"
#include "PMSI.h"
#include "MSI.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

namespace ns3 { 
  /**
   * brief SharedCacheCtrl Implements Cache coherence protocol on the 
   * shared memory side, it is the main interface between LLC and the 
   * bus arbiter, the cache controller communicates with bus arbiter 
   * through FIFO bi-directional channel. 
  */
  
  class SharedCacheCtrl : public ns3::Object {
  private:
     enum CacheField {
       State = 0,
       Tag,
       Data,
       Line
     };

     // shared cache parameters
     uint16_t   m_cacheType;
     uint32_t   m_cacheSize;
     uint32_t   m_cacheBlkSize;
     uint32_t   m_nways;
     uint32_t   m_nsets;
     int        m_coreId;
     double     m_dt; 
     double     m_clkSkew; 
     uint64_t   m_cacheCycle;
     bool       m_cach2Cache;
     int        m_nPrivCores;
     bool       m_logFileGenEnable;

     std::string m_bmsPath;

     uint16_t * m_ownerCoreId;

     // A pointer to Bus Interface FIFO
     Ptr<BusIfFIFO> m_busIfFIFO;

     // A pointer to Shared cache
     Ptr<GenericCache> m_cache;

     // Check BusIfFIFO content and return the corresponding 
     // ReqBusEvent of next in queue message 
     SNOOPSharedReqBusEvent  ChkBusRxReqEvent  (Ptr<BusIfFIFO> associatedBusIfFIFO,
                                                 BusIfFIFO::BusReqMsg &  busReqMsg); 

     // Check BusIfFIFO content and return the corresponding 
     // RespBusEvent of next in queue message 
     SNOOPSharedRespBusEvent ChkBusRxRespEvent (Ptr<BusIfFIFO> associatedBusIfFIFO,
                                                 BusIfFIFO::BusRespMsg & busRespMsg); 

     void UpdateSharedCache                     (CacheField field, 
                                                 uint64_t addr, 
                                                 int state, 
                                                 uint8_t * data);

     bool FetchLine (uint64_t addr, GenericCacheFrmt & cacheLine, uint32_t & LineWayIdx);

     bool DoWriteBack (uint64_t cl_idx, uint16_t wbCoreId, uint64_t msgId, double timestamp , bool PushFrontFlag);

     // insert new Transaction into BusTxMsg "pending" WB FIFO 
     bool PushMsgInBusTxFIFO  (uint64_t       msgId, 
                                                uint16_t       reqCoreId, 
                                                uint16_t       wbCoreId, 
                                                uint64_t       addr);

     void SendPendingReqData  (GenericCacheMapFrmt recvTrans );

     // Called by static method to process step
     // to insert new request or remove response
     // from assoicatedBuffers.
     void CycleProcess  ();
     void CacheCtrlMain ();
     void CacheInitialize();

  public:
    // Override TypeId.
    static TypeId GetTypeId(void);

    SharedCacheCtrl (uint32_t       cachLines, 
                     Ptr<BusIfFIFO> assoicateBusIfFIFO);


    ~SharedCacheCtrl();
     
    void SetCacheSize (uint32_t cacheSize);

    uint32_t GetCacheSize ();

    void SetCacheBlkSize (uint32_t cacheBlkSize);

    uint32_t GetCacheBlkSize ();

    void SetCacheNways (uint32_t nways);

    uint32_t GetCacheNways ();

    void SetCacheNsets (uint32_t nsets);

    uint32_t GetCacheNsets ();

    void SetCacheType (uint16_t cacheType);

    uint16_t GetCacheType ();

    void SetCoreId (int coreId);

    int GetCoreId ();

    void SetDt (double dt);

    int GetDt ();

    void SetClkSkew (double clkSkew);

    void SetCache2Cache (bool cach2Cache);

    void SetBMsPath  (std::string bmsPath);

    void SetNumPrivCore (int nPrivCores);

    void SetLogFileGenEnable (bool logFileGenEnable);

    void init();

    /**
     * Run PrivateCacheCtrl every clock cycle to
     * update cache line states and generate coherence 
     * messages. This function does the scheduling
     */
     static void Step(Ptr<SharedCacheCtrl> sharedCacheCtrl);

  };
}

#endif /* _SharedCacheCtrl_H */

